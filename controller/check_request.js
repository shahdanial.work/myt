//version 3
const helper = require('./../helper')
const fs = require('fs')

var check_request = function(req, res, current_url, data) {

   if (req.url.slice(-1) === '/') {
    req.url = helper.get_canonical(req.url);//req.url.slice(0, -1);
   }


   if (req.url == '/robots.txt') {

      /* ROBOTS.TXT */
      var robotfile = fs.readFileSync('./robots.txt', 'utf8')

      res.writeHead(200, {
         'Content-Type': 'text/plain\; charset=us-ascii'
      })
      res.write(robotfile)
      res.end()

   } else if(req.url == '/favicon.ico'){

      /* FAVICON.ICO */
      var ico = fs.readFileSync('./favicon.ico')

      res.writeHead(200, {
         'Content-Type': 'image/x-icon'
      })
      res.write(ico)
      res.end()

   } else if (!current_url.search && req.url == current_url.pathname) { // req.url (path?) == current_url.pathname (path)

      var my_path = current_url.pathname.split('/');
      data['my_path'] = helper.clean_array(my_path)
      var jumlah_path = parseFloat(data['my_path'].length);

      if (parseFloat(jumlah_path) < parseFloat(2)) {

         // ini single post sebab 1 path
         // if(req['url'].substr(-1) != '/'){

            data['path'] = helper.path_to_query(helper.get_canonical(req.url))

            const send = require('./../' + data['dir'] + '/singlepost/singlepost.js')

            send.getPostData(req, res, current_url, data)

      } else {

         if(data['domain'] = 'hub.malayatimes.com'){

            if(parseFloat(jumlah_path) == 2){
               // there's already data['my_path'], you can use it to server 'figure' and 'awie' something like that
               data['path'] = helper.path_to_query(helper.get_canonical(req.url))
               const send = require('./../' + data['dir'] + '/singlepost/singlepost.js')
               send.getPostData(req, res, current_url, data)

            } else {
               res.writeHead(404, {
                  'Content-Type': 'text/plain\; charset=utf-8'
               })
               res.write('page not found')
               res.end()
            }

         } else {

            //ini bukan single post sebab bukan 1 path (404 sementara, nnt 2 or more path leh main sini)
            res.writeHead(404, {
               'Content-Type': 'text/plain'
            });
            res.write('Page ini tidak wujud');
            res.end();

         }

      }

   } else {

      var redirect_to = helper.get_canonical(req.url);

      if(data['domain'] == 'hub.malayatimes.com' && redirect_to == '/cari'){

         const send = require('./../hub/cari/1');
         send.kkk(req, res, current_url, data);

      } else {
         res.writeHead(301, {
            'Location': redirect_to,
            'Content-Type': 'text/plain'
         });
         res.end();
      }
   }

}

module.exports.check_request = check_request;
