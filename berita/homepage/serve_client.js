var serve_client = function(req, res, current_url, data){

    res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'});
    res.write('<!DOCTYPE html>\n')
    res.write('<html class="main-blog">')
    res.write(data['head_open'])
    res.write('<title>Berita Terkini Malaysia</title><meta content="Berita Terkini Malaysia 2018. Web berita dan info serta isu politik, jenayah semasa paling terkini juga terbaru di Malaysia" name="description"/><meta content="https://berita.malayatimes.com/" property="og:url"/>')
    res.write('<style type="text/css">'+data['css']+'</style>')
    res.write(data['head_close'])
    res.write('<body id="main-blog">') // because it's a main website. this is main thing including the header !
    res.write(data['menu_nav'])
    res.write(data['header'])
    res.write('<div class="hfeed section" id="blog-content"><div class="widget Blog" data-version="1" id="Blog1">')
    //////////// DYNAMIC START /////////////

    var article_id = 0;

        res.write('<div class="wrapper rows">')
            /* SECTION POPULAR */
            // res.write(data['popular_box'])
            /* POPULAR START */
            if(data['popular_feed']){
                var popular = data['popular_feed'];
                res.write('<aside class="popular-articles column_1"> <h3>Popular</h3> ')
                 for (let i = 0; i < popular.length; i++) {
                     var artikel = popular[i];
                     res.write('<div class="popular-article first"><div class="inner-wrapper"><a class="img" href="//'+artikel.domain+'/'+artikel.path+'" style="background: url(//media.malayatimes.com/foto/'+artikel.first_img+') no-repeat;"><img alt="'+artikel.title.replace(/"/g,'')+' " src="//media.malayatimes.com/gambar/'+artikel.first_img+'"></a><div class="caption"><h2><a class="title" href="//'+artikel.domain+'/'+artikel.path+'">'+artikel.title+'</a></h2>')
                        if(artikel.tag_title && artikel.tag_path){
                            res.write('<a class="tag" href="//hub.malayatimes.com/'+artikel.tag_path+'">'+artikel.tag_title+'</a>')
                        }
                    //
                     res.write('</div></div></div> ')
                 }
                 if(popular.length > 1){
                    res.write(' <button class="slide_left">Back</button> <button class="slide_right">Next</button> ')
                    res.write('<div class="slidecount">')
                    for (let i = 0; i < popular.length; i++) {
                        if(i == 0){
                         res.write('<span id="count'+i+'" class="active"></span>')
                        } else {
                         res.write('<span id="count'+i+'"></span>')
                        }
                    }
                    res.write('</div>')
                 }
                res.write('</aside>')
            } else {
                res.write(data['popular_box'])
            }
            /* POPULAR END */

            article_id = 10;
            column = 0;
            /* SECTION LATEST */
            var latest_post = data['latest_post'];

            if(latest_post[0]){
                res.write('<div class="recent-articles column_2 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-11"><a class="img" href="//'+latest_post[0].category+'/'+latest_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[0].first_img+') no-repeat"><img alt=" '+latest_post[0].title+' " src="//media.malayatimes.com/photo/'+latest_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[0].category+'/'+latest_post[0].path+'">'+latest_post[0].title+'</a></h3>')

                var masa_utc = new Date(latest_post[0]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[0].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[0].tags[0].tag_path+'">'+latest_post[0].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> ')
                res.write('</div></div>')
            }

            if(latest_post[1]){
                res.write('<div class="recent-articles column_3 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-12"><a class="img" href="//'+latest_post[1].category+'/'+latest_post[1].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[1].first_img+') no-repeat"><img alt=" '+latest_post[1].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[1].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[1].category+'/'+latest_post[1].path+'">'+latest_post[1].title+'</a></h3>')

                var masa_utc = new Date(latest_post[1]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[1].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[1].tags[0].tag_path+'">'+latest_post[1].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> ')
                res.write('</div></div>')
            }
            /* /div LATEST */

            res.write('</div><!-- end class="wrapper rows"-->') // dalamnya popular dan recent atas

            /* SECTION LATEST BAWAH */
            if(latest_post[2]){
                res.write('<div class="wrapper rows">')

                res.write('<div class="recent-articles column_4 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-13"><a class="img" href="//'+latest_post[2].category+'/'+latest_post[2].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[2].first_img+') no-repeat"><img alt=" '+latest_post[2].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[2].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[2].category+'/'+latest_post[2].path+'">'+latest_post[2].title+'</a></h3>')

                var masa_utc = new Date(latest_post[2]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[2].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[2].tags[0].tag_path+'">'+latest_post[2].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> ')
                res.write('</div></div>')

                /*3 to 8 (2 group posts box)*/
                if(latest_post[3]){
                res.write('<div class="recent-articles column_5 odd">')


                    var even_odd = 'odd';
                    var column = 5;
                    var ganda = 0;
                    var article_id = 13;
                    for (let i = 3; i < latest_post.length; i++) {
                        const post_latest = latest_post[i];
                        article_id +=1;
                        ganda += 1;

                        ///////////////////////
                        if(i < 9){
                            res.write('<article class="hentry recent-article" id="article-'+article_id+'"> <a class="img" href="//'+post_latest.category+'/'+post_latest.path+'"> <img alt="'+post_latest.title.replace(/"/g,'')+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> <h2 class="entry-title"><a class="title" href="//'+post_latest.category+'/'+post_latest.path+'">'+post_latest.title+'</a></h2>')

                            var masa_utc = new Date(post_latest['masa']).toISOString()
                            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                            if(post_latest.tags[0]){
                                res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                            }
                            res.write(' </article>')
                        }
                        ///////////////////////

                        if(ganda == 3 && i != 8){
                            column += 1;
                            ganda = 0;
                            if(even_odd == 'even'){
                                even_odd = 'odd';
                            } else {
                                even_odd = 'even';
                            }
                            res.write('</div><div class="recent-articles column_'+column+' '+even_odd+'">')
                        }

                    }

                res.write('</div><!-- end class="recent-articles column_5 odd"-->')
                }


                // post 9
                if(latest_post[9]){
                    res.write('<div class="recent-articles column_6 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-15"><a class="img" href="//'+latest_post[9].category+'/'+latest_post[9].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[9].first_img+') no-repeat"><img alt=" '+latest_post[9].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[9].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[9].category+'/'+latest_post[9].path+'">'+latest_post[9].title+'</a></h3>')

                    var masa_utc = new Date(latest_post[9]['masa']).toISOString()
                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(latest_post[9].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[9].tags[0].tag_path+'">'+latest_post[9].tags[0].tag_title+'</a>')
                    }
                    res.write('</div></article> ')
                    res.write('</div></div>')
                }


                res.write('</div><!-- end class="wrapper rows"-->')
            }
            /* /div LATEST BAWAH */



            /* SECTION LATEST LAST */


            if(latest_post[10]){
                res.write('<div class="wrapper rows">')

                res.write('<div class="recent-articles column_7 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-16"><a class="img" href="//'+latest_post[10].category+'/'+latest_post[10].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[10].first_img+') no-repeat"><img alt=" '+latest_post[10].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[10].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[10].category+'/'+latest_post[10].path+'">'+latest_post[10].title+'</a></h3>')

                var masa_utc = new Date(latest_post[10]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[10].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[10].tags[0].tag_path+'">'+latest_post[10].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> ')
                res.write('</div></div>')


                //11 to 16
                if(latest_post[11]){
                    res.write('<div class="recent-articles column_8 odd">')


                        var even_odd = 'odd';
                        var column = 8;
                        var ganda = 0;
                        var article_id = 16;
                        for (let i = 11; i < latest_post.length; i++) {
                            const post_latest = latest_post[i];
                            article_id += 1;
                            ganda += 1;

                            ///////////////////////
                            if(i < 17){
                                res.write('<article class="hentry recent-article" id="article-'+article_id+'">')
                                res.write(' <a class="img" href="//'+post_latest.category+'/'+post_latest.path+'"> <img alt="'+post_latest.title.replace(/"/g,'')+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> ')
                                res.write('<h2 class="entry-title"><a class="title" href="//'+post_latest.category+'/'+post_latest.path+'">'+post_latest.title+'</a></h2>')

                                var masa_utc = new Date(post_latest['masa']).toISOString()
                               res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                                if(post_latest.tags[0]){
                                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                                }
                                res.write(' </article>')
                            }

                            if(ganda == 3 && i != 16){
                                column += 1;
                                ganda = 0;
                                if(even_odd == 'even'){
                                    even_odd = 'odd';
                                } else {
                                    even_odd = 'even';
                                }
                                res.write('</div><div class="recent-articles column_'+column+' '+even_odd+'">')
                            }
                            ///////////////////////

                        }

                    res.write('</div><!-- end class="recent-articles column_7 odd"-->')
                    }



                // post 17
                if(latest_post[17]){
                    res.write('<div class="recent-articles column_9 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-20"><a class="img" href="//'+latest_post[17].category+'/'+latest_post[17].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[17].first_img+') no-repeat"><img alt=" '+latest_post[17].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[17].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[17].category+'/'+latest_post[17].path+'">'+latest_post[17].title+'</a></h3>')

                    var masa_utc = new Date(latest_post[17]['masa']).toISOString()
                   res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(latest_post[17].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[17].tags[0].tag_path+'">'+latest_post[17].tags[0].tag_title+'</a>')
                    }
                    res.write('</div></article> ')
                    res.write('</div></div>')
                }


                res.write('</div><!-- end class="wrapper rows"-->')
            }

            /* /div LATEST LAST */


            res.write('<aside id="load-more" class="js-auto-load-content"><span class="loader"><span class="pusing"></span></span></aside>')


    //////////// DYNAMIC END ///////////////
    res.write('</div></div><!--class="widget Blog" data-version="1" id="Blog1"-->')
    res.write(data['footer'])
    res.write('</body></html>')
    res.end();

} // end create server


module.exports.serve_client = serve_client;
