var send = require('./format_data')
const db = require('../../db')
const mysql = require('mysql')
const helper = require('../../helper')

var get_popular = function(req,res,current_url, data, id_tak_mau){

    var popular_feed = function(err1, rows1){
        if(err1){
            console.log('error popular_single sub sql \n', err1);
            throw err1;
        } else {
            if(rows1){
                data['popular_feed'] = rows1;
                for (let i = 0; i < data['popular_feed'].length; i++) {
                    if(data['popular_feed'][i]['id']){
                        id_tak_mau = id_tak_mau+', '+data['popular_feed'][i]['id'];
                    }
                }
            }
        }
        ///////// SEND TO VIEW ///////////////
        send.format_data(req,res,current_url, data);
    }
    db.statement("SELECT reputation_manager.from_id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path as path, category_manager.domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.from_tbl = 'post_tbl' AND reputation_manager.from_id NOT IN ("+id_tak_mau+") GROUP BY reputation_manager.from_id ORDER BY score DESC LIMIT 0,10", false, popular_feed);



    ////////// UPDATE REPUTATION AND PAGE VIEW ///////////

    if(data){ //wtf start
        if(data['post']){

            if(data['post']['id'] && data['post']['path'] && data['ip_adress'] && data['post']['author_id']){

                // let's record reputation by view...
                db.statement('UPDATE post_tbl SET pageview = pageview + 1 WHERE id = ?', [mysql.escape(data['post']['id'])], false);

                var dariJam = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(10000); // waktu2 1 jam sebelum
                var dariHari = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(1000000); // waktu2 1 hari sebelum

                ///////////////////////////// checkHour ///////////////////////////
                var checkCount = function(err1, rejaut_1) { //what ever category from post_tbl..
                    if(err1){
                        console.log('error check_ip mySql', err1);
                        throw err1;
                    } else {
                        if(rejaut_1){
                            if(rejaut_1[0]){
                                var jumlah = rejaut_1[0];
                                if( jumlah.byJam < 1 && jumlah.byHari < 10 && jumlah.byBulan < 225 ){
                                    /* MELEPASI ALGOTHRM */
                                    ////////////////////// SEMUA LEPAS START ///////////////
                                    // insert new reputation manager..
                                    db.statement(
                                        //query
                                        'INSERT INTO reputation_manager (ip, time, author_id, from_tbl, from_id, reputation, type) VALUES (?, ?, ?, ?, ?, ?, ?)'
                                        ,
                                        //string
                                        [
                                            data['ip_adress'],
                                            helper.datetime().replace(/ /g, ''),
                                            data['post']['author_id'],
                                            'post_tbl',
                                            mysql.escape(data['post']['id']),
                                            '1',
                                            'view'
                                        ]
                                        ,
                                        false
                                    );
                                    ////////////////////// SEMUA LEPAS END ///////////////
                                }
                            }
                        }
                    }
                }
                db.statement(
                /* STATEMENT */
                'SELECT (SELECT COUNT(id) FROM reputation_manager WHERE time > ? AND from_tbl = ? AND from_id = ? AND ip = ?) as byJam, (SELECT COUNT(id) FROM reputation_manager WHERE time > ? AND author_id = ? AND ip = ?) as byHari, (SELECT COUNT(id) FROM reputation_manager WHERE author_id = ? AND ip = ?) as byBulan',
                /* STRING */
                [
                    //By Jam
                    dariJam, 'post_tbl', data['post']['id'], data['ip_adress'],

                    // By Hari
                    dariHari, data['post']['author_id'], data['ip_adress'],

                    //By Bulan
                    data['post']['author_id'], data['ip_adress']
                ],
                /* CALLBACK */
                checkCount);
                ///////////////////////////// checkHour ///////////////////////////

            }
        }
    } // wtf end



} // end module

module.exports.get_popular = get_popular;
