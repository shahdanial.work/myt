var build_tag = function(tag_path, tag_title, tag_type) {
   var tag_path = clean_array(tag_path.split(','))
   var tag_title = clean_array(tag_title.split(','))
   var tag_type = clean_array(tag_type.split(','))
   if (tag_path && tag_title && tag_path.length === tag_title.length && tag_path.length === tag_type.length) {
      var arr = new Array()
      for (let i = 0; i < tag_path.length; i++) {
         arr[i] = new Array()
         arr[i]['tag_path'] = tag_path[i];
         arr[i]['tag_title'] = tag_title[i];
         arr[i]['tag_type'] = tag_type[i];
      }
      return arr;
   } else {
      // jumlah tag_title dan tag_path tak sepadan. So kita kosongkan je lah tag.
      return new Array();
   }
}
module.exports.build_tag = build_tag;

var build_keyword = function(obj){

   var mykey = obj.mykey || '';
   var myvalue = obj.myvalue || '';
   var key = mykey.split(',');
   var value = myvalue.split(',');


   var arr = new Array()
   arr['gender'] = new Array()
   arr['interest'] = new Array()

   if(key.length == value.length){
      for (var i = 0; i < key.length; i++) {
         if( key[i] == 'gender' && !arr['gender'].includes(value[i]) && value[i].trim().length>0) arr['gender'].push(value[i]);
         if( key[i] == 'interest'  && !arr['interest'].includes(value[i]) && value[i].trim().length>0) arr['interest'].push(value[i]);
      }
   }

   if(obj['searched_keyword']){
      if(obj['searched_keyword'].length > 0 ){
         for (var i = 0; i < obj.searched_keyword.length; i++) {
            if(!arr['interest'].includes(obj['searched_keyword'][i])) arr['interest'].push(obj['searched_keyword'][i]);
         }
      }
   }


   if(arr['gender'] || arr['interest']){
      var res = '';
      if(arr['gender'].length > 0){
         res = "googletag.pubads().setTargeting('gender',[";
         for (var i = 0; i < arr['gender'].length; i++) {
            res += "'"+ arr['gender'][i] +"'";
            if(i != parseFloat(arr['gender'].length) - parseFloat(1)){
               res += ",";
            }
         }
         res +="]),";
      }
      if(arr['interest'].length > 0){
         res += "googletag.pubads().setTargeting('interest',[";
         for (var i = 0; i < arr['interest'].length; i++) {
            res += "'"+ arr['interest'][i] +"'";
            if(i != parseFloat(arr['interest'].length) - 1){
               res += ",";
            }
         }
         res +="]),";
      }

      return res;

   }

   return '';

}
module.exports.build_keyword = build_keyword;

var build_keyword_v2 = function(rows){
   var res = "googletag.pubads().setTargeting('interest',[";
   for (var i = 0; i < rows.length; i++) {
      res += "'"+ rows[i].myvalue +"'";
      if(i != parseFloat(rows.length) - 1){
         res += ",";
      }
   }
   res +="]),";

   return res
}
module.exports.build_keyword_v2 = build_keyword_v2

/*
clean_array(your array string or array directly)
['ada', 'something', '', '']
TO
['ada', 'something']
*/
var clean_array = function(actual) {
   var newArray = new Array();
   for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
         newArray.push(actual[i]);
      }
   }
   return newArray;
}
module.exports.clean_array = clean_array;

// check if value in array or not
var isInArray = function(value, array) {
  return array.indexOf(value) > -1;
}
module.exports.isInArray = isInArray;

/*

1. you can convert, full url, with http , without http, or path only.
2. it will remove all chrachter after "?" and "?" itself
3. it will remove all chrachter after "#" and "#" itself
4. everything return will convert toLowerCase();
*/
var get_canonical = function(str) {

   str = str.replace(/http:|https:/g, '').replace('//', '');

   if (str.indexOf('?') > -1) {
      str = str.split('?');
      str = str[0];
   }
   if (str.indexOf('#') > -1) {
      str = str.split('#');
      str = str[0];
   }

   if (str.slice(-1) === '/') { // remove last trailing slash
      str = str.slice(0, -1);
   }

   return str.toLowerCase().replace(/\/$/, "");

}
module.exports.get_canonical = get_canonical;

/*
from your PATH /last-path-only/?query=0#hash
to 'last-path-only'
TRUE path_to_query('/last-path-only/?query=0#hash')
WRONG path_to_query('ANOTHER-PATH/last-path-only/?query=0#hash')
*/
var path_to_query = function(str) {

   if (str.indexOf('?') > -1) {
      str = str.split('?');
      str = str[0];
   }
   if (str.indexOf('#') > -1) {
      str = str.split('#');
      str = str[0];
   }
   str = str.replace('/', '')

   return str.toLowerCase();

}
module.exports.path_to_query = path_to_query;


var number_k = function(number){

   var decPlaces = 1;

   // ratus
   if(parseFloat(number) < 999){
      return number;
   }
      var orig = number;
      var dec = decPlaces;
      // 2 decimal places => 100, 3 => 1000, etc
      decPlaces = Math.pow(10, decPlaces);

      // Enumerate number abbreviations
      var abbrev = ["K", "M", "B", "T"];

      // Go through the array backwards, so we do the largest first
      for (var i = abbrev.length - 1; i >= 0; i--) {

         // Convert array index to "1000", "1000000", etc
         var size = Math.pow(10, (i + 1) * 3);

         // If the number is bigger or equal do the abbreviation
         if (size <= number) {
            // Here, we multiply by decPlaces, round, and then divide by decPlaces.
            // This gives us nice rounding to a particular decimal place.
            var number = Math.round(number * decPlaces / size) / decPlaces;

            // Handle special case where we round up to the next abbreviation
            if ((number == 1000) && (i < abbrev.length - 1)) {
               number = 1;
               i++;
            }

            // Add the letter for the abbreviation
            number += abbrev[i];

            // We are done... stop
            break;
         }
      }
      return number;
   }
module.exports.number_k = number_k;


//2018:07:22 01:59:40

var datetime = function() {
   var date = new Date();

   var hour = date.getHours();
   hour = (hour < 10 ? "0" : "") + hour;

   var min = date.getMinutes();
   min = (min < 10 ? "0" : "") + min;

   var sec = date.getSeconds();
   sec = (sec < 10 ? "0" : "") + sec;

   var year = date.getFullYear();

   var month = date.getMonth() + 1;
   month = (month < 10 ? "0" : "") + month;

   var day = date.getDate();
   day = (day < 10 ? "0" : "") + day;

   return year + " " + month + " " + day + " " + hour + " " + min + " " + sec;
}

module.exports.datetime = datetime;



var tambahanOnline = function(){

   var d = new Date();
   var jamSkang = d.getHours();
   var minitSkang = d.getMinutes();

   var percentMinit = (minitSkang / 59 * 100);// * 10;

   // 8pg - 13.59ptg PENAMBAHAN 1-10
   if(jamSkang >= 8 && jamSkang < 14){

      var bermula = 100;
      for (var i = 8; i < 14+1; i++) {
         if(i == jamSkang){
             var tambah_tolak = bermula + percentMinit;
             break;
         } else {
            bermula = bermula + 10;
         }
      }

   } else
   // 14ptg - 18.59mlm PENOLAKAN 1-10
   if(jamSkang >= 14 && jamSkang < 19){

      var bermula = 170;
      for (var i = 14; i < 19+1; i++) {
         if(i == jamSkang){
             var tambah_tolak = bermula - percentMinit;
             break;
         } else {
            bermula = bermula - 10;
         }
      }

   } else
   // 19mlm 23.59tgh-mlm PENAMBAHAN 1-10
   if(jamSkang >= 19 && jamSkang < 24){

      var bermula = 120;
      for (var i = 20; i < 24+1; i++) {
         if(i == jamSkang || i == 24){
             var tambah_tolak = bermula + percentMinit;
             break;
         } else {
            bermula = bermula + 10;
         }
      }

   } else
   // 0tgh-mlm - 7.59pg PENOLAKAN 1-10
   if(jamSkang >= 0 && jamSkang < 8){

      var bermula = 170;
      for (var i = 0; i < 8+1; i++) {
         if(i == jamSkang){
             var tambah_tolak = bermula - percentMinit;
             break;
         } else {
            bermula = bermula - 10;
         }
      }

   }

   return parseFloat(tambah_tolak.toFixed(0));

}
module.exports.tambahanOnline = tambahanOnline;


var compare_domain = function(isi, arr){
   var myreturn = false;
   if(isi && arr){
        if(typeof arr == 'string'){
             if(arr.indexOf('*') > -1){// example: *.mydomain.com..

                  var myreturn = check_allow_sub_domain(get_domain(isi), arr);

             } else  if(get_domain(isi) == arr.toLowerCase()){
                  var myreturn = true;
             }
        } else if(typeof arr == 'object') {
             for (var i = 0; i < arr.length; i++) {
                  if(arr[i].indexOf('*') > -1 && check_allow_sub_domain(get_domain(isi), arr[i])){
                       var myreturn = true;
                  }else if(get_domain(isi) == arr[i].toLowerCase()){
                       var myreturn = true;
                  }
             }
        }
   } else {
        // console.log('isi or arr empty: compare_domain()');
        var myreturn = false;
   }
   return myreturn;
}
module.exports.compare_domain = compare_domain;

var check_allow_sub_domain = function(str, allowed){
   var str = str.toLowerCase().split('.');
   var allowed = allowed.toLowerCase().split('.');

   var myreturn = false;

   if(allowed.length == 3){ /* CONDITION 1*/ // *.domain.com
        // console.log('allowed 3');
        //console.log(allowed);
        //console.log(str);
        if(str.length == 2){ // domain.com
             if(allowed[1] == str[0] && allowed[2] == str[1]){
                  var myreturn = true;
                  // console.log('A');
             }
        }
        if(str.length == 3){ // sub.domain.com = lulus... domain.com.country = gagal..
             if(allowed[1] == str[1] && allowed[2] == str[2]){
                  var myreturn = true;
                  // console.log('B');
             }
        }
        //4 takkan lepas. sub.domain.com.country vs *.domain.com(3aallowed).
   }

   if(allowed.length == 4){ /* CONDITION 1*/ // *.domain.com.country
        // console.log('allowed 4');
        //2 xkan lepas .. domain.com vs *sub.domain.com.country
        if(str.length == 3){ // (domain.com.country = lepas..) (sub.domain.com = x lepas..)
             if(allowed[1] == str[0] && allowed[2] == str[1] && allowed[3] == str[2]){
                  var myreturn = true;
                  // console.log('C');
             }
        }
        if(str.length == 3){ // (sub.domain.com.country = lepas..)
             if(allowed[1] == str[1] && allowed[2] == str[2] && allowed[3] == str[3]){
                  var myreturn = true;
                  // console.log('D');
             }
        }
   }

   return myreturn;
}
module.exports.check_allow_sub_domain = check_allow_sub_domain;

var get_domain = function(str){
   var str = str.replace(/http:|https:/g,'').replace('//','');
   if(str.indexOf('/') > -1){
        var str = str.split('/');
        var str = str[0];
   }
   if(str.indexOf('?') > -1){
        var str = str.split('?');
        var str = str[0];
   }
   if(str.indexOf('#') > -1){
        var str = str.split('#');
        var str = str[0];
   }
   return str.toLowerCase();
}
module.exports.get_domain = get_domain;