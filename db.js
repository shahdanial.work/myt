const mysql = require('mysql');
const fs = require('fs');
///////////////////////////////////
function handleDisconnect(cback) {

   fs.readFile('../db.json', 'utf8', function(err_1, data) {
      if(err_1) throw err_1;
      // console.log(typeof data, data)
      var obj = JSON.parse(data);
      var connection = mysql.createConnection(obj); // Recreate the connection, since
                                                      // the old one cannot be reused.

      connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
          console.log('error when connecting to db:', err);
          setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
          // console.log('kami dapati error connect db. dibawah mesejnya', err)
        }
        // else {
        //   console.log('we succed db connect over handleDisconnect()');
        // }                                    // to avoid a hot loop, and to allow our node script to
      });                                     // process asynchronous requests in the meantime.
                                              // If you're also serving http, display a 503 error.
      connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
          console.log('PROTOCOL_CONNECTION_LOST');
          handleDisconnect();                         // lost due to either server restart, or a
        } else {
          console.log('there\'s error to connect db', err); // connnection idle timeout (the wait_timeout
          throw err;                                  // server variable configures this)
        }
      });
      if (err_1){
        if(cback){
          cback(false,false);
          // connection.end();
        }
        throw err_1;
      } else {
        // here we call the "callback" with the rows returned as the second argument
        if(cback){
          cback(null, connection);
          // connection.end();
        }
      }

   });

}

/////////////////////////// NPM: MYSQLJS/MYSQL ///////////////////////////

/* ASYNC , MUST USE CALLBACK */
var statement = function (query_arg, string, done) {

  var cbbb = function(err_123, con){

     if(err_123) throw err_123;

     if(string){
      var sql = mysql.format(query_arg, string);
     } else {
      var sql = query_arg;
     }



      // if(done){ if(done.name === 'query_callback') console.log(done.name,"\n******************\n"+sql+"\n******************\n") }

     con.query(sql, function (error, rows, fields) {
      if (error){
         if(done){
           done(false,false);
         }
         throw error;
      } else {
         // here we call the "callback" with the rows returned as the second argument
         if(done){
           done(null, rows);
         }
      }
     });
     con.end();

  }
  handleDisconnect(cbbb);

}

module.exports.statement = statement;

