const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const http = require('http');
const { URL } = require('url');
const protocol = 'http://';
const express = require('express');
const fs = require('fs')
const db = require('./db');
const controller = require('./controller/check_request');
const hostname = '127.0.0.1'; // == localhost
const helper = require('./helper');


if (!cluster.isMaster) {

/*--------------- bermula server-server domain --------------*/

//////// www.malayatimes.com //////
const www4000 = express();
www4000.get('/', function(req, res) {

  console.log('homepage')
   const current_url = new URL(protocol + hostname + req.url);
   var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
   var data = new Array()
   data['ip_adress'] = visitor_ip;
   data['head_open'] = fs.readFileSync('./head_open.html', 'utf8');
   data['css'] = fs.readFileSync('./homepage.css', 'utf8');
   data['head_close'] = fs.readFileSync('./head_close.html', 'utf8');
   data['menu_nav'] = fs.readFileSync('./menu_nav.html', 'utf8');
   data['header'] = fs.readFileSync('./www_index_header.html', 'utf8');
   data['popular_box'] = fs.readFileSync('./popular_box.html', 'utf8');

   const send = require('./www/home-page/homepage_data');
   send.homepage_data(req, res, current_url, data);

});
www4000.get('/robots.txt', function(req, res) {

   res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
   res.write(fs.readFileSync('./robots.txt', 'utf-8'))
   res.end()

});
www4000.get('/:userName', function(req, res){

   const current_url = new URL(protocol + hostname + req.url);
   var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

   var data = new Array()
   data['ip_adress'] = visitor_ip;
   data['username'] = req.params.userName;

   data['head_open'] = fs.readFileSync('./head_open.html', 'utf8');
   data['css'] = fs.readFileSync('./homepage.css', 'utf8');
   data['head_close'] = fs.readFileSync('./head_close.html', 'utf8');
   data['menu_nav'] = fs.readFileSync('./menu_nav.html', 'utf8');
   data['header'] = fs.readFileSync('./www_index_header.html', 'utf8');

   const send = require('./www/homepage_user/check_user');
   send.check_user(req, res, current_url, data);

})
www4000.listen(4000);



/******************** SUB DOMAIN AREA ***********************/
var data = new Array()
data['head_open'] = fs.readFileSync('./head_open.html', 'utf8');
data['head_close'] = fs.readFileSync('./head_close.html', 'utf8');
data['menu_nav'] = fs.readFileSync('./element/nav.html', 'utf8');


//////// artis.malayatimes.com //////
const artis4001 = express();
artis4001.get('/', function(req, res) {

   const current_url = new URL(protocol + hostname + req.url);
   data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
   data['domain'] = 'artis.malayatimes.com';

   data['meta_title'] = 'Gosip Artis Malaysia';
   data['meta_description'] = 'Gosip Artis Malaysia ok? Blog hiburan dan gosip kisah cerita video gambar artis Malaysia yang terkni terbaru panas dan lagi hot juga semasa';
   data['css'] = fs.readFileSync('./homepage.css', 'utf8');
   data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

   const send = require('./blogView/homepage/homepage_data');
   send.homepage_data(req, res, current_url, data);

});
artis4001.get('/:postUrl', function(req, res){

   const current_url = new URL(protocol + hostname + req.url);
   data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
   data['domain'] = 'artis.malayatimes.com';

   data['dir'] = 'blogView';
   data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
   data['current_url'] = current_url;
   controller.check_request(req, res, current_url, data);

});
artis4001.listen(4001);

// ----------------------------- ikram practice berita -----------------
const berita40004 = express();
berita40004.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'berita.malayatimes.com';

  data['meta_title'] = 'Berita Terkini Malaysia';
  data['meta_description'] = 'Berita Terkini Malaysia 2018. Web berita dan info serta isu politik, jenayah; semasa paling terkini juga terbaru di Malaysia';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});
berita40004.get('/:postUrl', function(req, res){
   const current_url = new URL(protocol + hostname + req.url);
   data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
   data['domain'] = 'berita.malayatimes.com';

   data['dir'] = 'blogView';
   data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
   data['current_url'] = current_url;
   controller.check_request(req, res, current_url, data);

});
berita40004.listen(4004);

// ----------------------------- ikram practice- viral -----------------------

const viral4006 = express();
viral4006.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'viral.malayatimes.com';

  data['meta_title'] = 'Viral Malaysia';
  data['meta_description'] = 'Blog viral Malaysia adalah sebuah Laman web serta yang blog popular isu gambar video status artikel dan kisah viral terkini juga semasa serta terbaru di Malaysia';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});

viral4006.get('/:postUrl', function(req, res){

   const current_url = new URL(protocol + hostname + req.url);
   data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
   data['domain'] = 'viral.malayatimes.com';

   data['dir'] = 'blogView';
   data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
   data['current_url'] = current_url;

   controller.check_request(req, res, current_url, data);

});
viral4006.listen(4006);


// ----------------------------- ikram practice sukan ------------------------

const sukan4007 = express();
sukan4007.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'sukan.malayatimes.com';

  data['meta_title'] = 'sukan Malaysia';
  data['meta_description'] = 'Blog sukan Malaysia adalah sebuah Laman web serta yang blog popular isu gambar video status artikel dan kisah sukan terkini juga semasa serta terbaru di Malaysia';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});
sukan4007.get('/:postUrl', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'sukan.malayatimes.com';

  data['dir'] = 'blogView';
  data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
  data['current_url'] = current_url;

  controller.check_request(req, res, current_url, data);

});
sukan4007.listen(4007);

// ----------------------------- ikram practice politik ------------------------
const politik4008 = express();
politik4008.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'politik.malayatimes.com';

  data['meta_title'] = 'Berita Terkini Politik Malaysia';
  data['meta_description'] = 'Berita terkini isu semasa politik Malaysia terbaru. Web berita terkini berkenaan isu semasa politik Malaysia yang terbaru termasuk Pakatan Harapan, Barisan Nasional dan Bebas';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});
politik4008.get('/:postUrl', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'politik.malayatimes.com';

  data['dir'] = 'blogView';
  data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
  data['current_url'] = current_url;

  controller.check_request(req, res, current_url, data);

});
politik4008.listen(4008);

// ----------------------------- ikram practice blog.malayatimes.com ------------------------

  const blog4009 = express();
  blog4009.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'blog.malayatimes.com';

  data['meta_title'] = 'Official Malaya Times Blog';
  data['meta_description'] = 'Official Malaya Times Blog adalah laman pengumuman dan tips untuk Malaya Times users';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});
blog4009.get('/:postUrl', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'blog.malayatimes.com';

  data['dir'] = 'blogView';
  data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
  data['current_url'] = current_url;

controller.check_request(req, res, current_url, data);

});
blog4009.listen(4009);

//------------------------------------- Pendidikan --------------------------
const pendidikan5001 = express();
pendidikan5001.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'pendidikan.malayatimes.com';

  data['meta_title'] = 'Info Pendidikan Malaysia';
  data['meta_description'] = 'Info Pendidikan Malaysia ok? Blog info semakan dan rujukan pendidikan pelajar dan guru bagi peperiksaan, soalan, keputussan dan keputusan Malaysia.';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');

  const send = require('./blogView/homepage/homepage_data');
  send.homepage_data(req, res, current_url, data);

});

pendidikan5001.get('/:postUrl', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'pendidikan.malayatimes.com';

  data['dir'] = 'blogView';
  data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
  data['current_url'] = current_url;
  controller.check_request(req, res, current_url, data);

});

pendidikan5001.listen(5001);

// --------------------------- kesihatan -----------------------------

const kesihatan5002 = express();
kesihatan5002.get('/', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'kesihatan.malayatimes.com';

  data['meta_title'] = 'Info kesihatan';
  data['meta_description'] = 'Info kesihatan Malaysia ok? Blog info rujukan dan tips kesihatan bagi rawatan dan pencegahan cara kampung dan moden.';
  data['css'] = fs.readFileSync('./homepage.css', 'utf8');
  data['header'] = fs.readFileSync('./element/header_index.html', 'utf8');
  const send = require('./blogView/homepage/homepage_data');

  send.homepage_data(req, res, current_url, data);

});

kesihatan5002.get('/:postUrl', function(req, res){

  const current_url = new URL(protocol + hostname + req.url);
  data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
  data['domain'] = 'kesihatan.malayatimes.com';

  data['dir'] = 'blogView';
  data['header'] = fs.readFileSync('./element/header_single.html', 'utf8');
  data['current_url'] = current_url;
  controller.check_request(req, res, current_url, data);

});
kesihatan5002.listen(5002);

// ------------------------------ HUBB -----------------------------------
     /* hub.malayatimes.com */
     http.createServer(function (req, res) {

       const current_url = new URL(protocol + hostname + req.url);
       var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

       var data = new Array()
       data['ip_address'] = visitor_ip;
       data['domain'] = 'hub.malayatimes.com';

       if (req.url === '' || req.url === '/') {
         /* HOME */
         data['meta_title'] = 'Hub dan Wiki';
         data['meta_description'] = 'Hub profile figura, organisasi serta wiki bagi acara, peristiwa dan produk popular.';

         const send = require('./hub/homepage/1')
         send.check(req, res, current_url, data)

       } else {
         /* SINGLE */
         data['dir'] = 'hub';

         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data)

       }

     }).listen(4002); // end create server
// const hub4002 = express();
// hub4002.get('/', function(req, res){

//   const current_url = new URL(protocol + hostname + req.url);
//   var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

//   var data = new Array();
//   data['ip_address'] = visitor_ip;
//   data['domain'] = 'hub.malayatimes.com';

//   data['meta_title'] = 'Hub dan Wiki';
//   data['meta_description'] = 'Hub profile figura, organisasi serta wiki bagi acara, peristiwa dan produk popular.';

//   const send = require('./hub/homepage/1');
//   send.check(req, res, current_url, data);

// });

// hub4002.get('/cari', function (req, res) {
//   const current_url = new URL(protocol + hostname + req.url);
//   var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

//   var data = new Array()
//   data['ip_address'] = visitor_ip;
//   data['domain'] = 'hub.malayatimes.com';

//   data['dir'] = 'hub';
//   data['current_url'] = current_url;

//   controller.check_request(req, res, current_url, data);

// });

// hub4002.get('/:typeWiki/:pathWiki', function(req, res){

//    const current_url = new URL(protocol + hostname + req.url);
//    var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

//    data['dir'] = 'hub';
//    data['current_url'] = current_url;

//   controller.check_request(req, res, current_url, data);

// });
// hub4002.listen(4002);

//---------------------- SERVICE AREA ----------------------------

const service4005 = express();
service4005.get('/update-myr', function(req, res){

  var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

  if (visitor_ip.indexOf('127.0.0.1') > -1) {

    const fx = require("yahoo-exchange")

    fx.getExchangeDataArray('USDMYR', function(data) {
       if (data) {
          if (data[0]) {
             if (!isNaN(data['0'])){

                var jumlah = parseFloat(Math.round(data['0'] * 100) / 100).toFixed(2);



                // update db..
                db.statement(
                   //query..
                   'UPDATE site_data SET value = ? WHERE what = ?',
                   // str..
                   [
                      jumlah,
                      'usd_to_myr'
                   ],
                   //callback
                   false
                );

                // res.writeHead(200, {
                //    'Content-Type': 'text/plain\; charset=utf-8'
                // });
                // res.write(jumlah);
                // res.end();

             } else {
                console.log('Yahoo money exchange return NaN data')
             }
          }
       }
    });

  }

});
service4005.listen(4005);
















const api5000 = express();
api5000.get('/', function(req, res){

  res.writeHead(404, {'Content-Type': 'text/plain'});
  res.write('page under construction');
  res.end();

});
api5000.post('/robots.txt', function(req, res){

  res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
  res.write(fs.readFileSync('./api-robots.txt', 'utf-8'))
  res.end()

});
api5000.post('/:folderInsideApiDir/:jsFilename', function(req, res) {

  var my_url = req['url'].split('?');
  my_url[0] = my_url[0].replace(/\./g,'');
  res['my_param'] = my_url[1] || false;

  if ( my_url[0] && fs.existsSync('./api-dir'+my_url[0]+'.js') ) {

     const send = require('./api-dir'+my_url[0]);
     send.filter_api(req, res);

  } else {

     res.writeHead(404, {
        'Content-Type': 'text/plain'
     });
     res.write('404 hahaha');
     res.end();
  }

});
  api5000.get('/res-param/:jsFilename', function (req, res) {

    var my_url = req['url'].split('?');
    my_url[0] = my_url[0].replace(/\./g, '');
    res['my_param'] = my_url[1] || false;

    if (my_url[0] && fs.existsSync('./api-dir' + my_url[0] + '.js')) {

      const send = require('./api-dir' + my_url[0]);
      send.filter_api(req, res);

    } else {

      res.writeHead(404, {
        'Content-Type': 'text/plain'
      });
      res.write('404 hahaha');
      res.end();
    }

  });
api5000.listen(5000);









/*--------------- berakhir server-server domain --------------*/

} else {
   // console.log(`Master ${process.pid} is running`);
   for (let i = 0; i < numCPUs; i++) {
     cluster.fork();
   }
   // cluster.on('exit', (worker, code, signal) => {
   //   console.log(`worker ${worker.process.pid} died`);
   // });
}
