const cluster = require('cluster');
const numCPUs = require('os').cpus().length;

const http = require('http')
const {
   URL
} = require('url');

const protocol = 'http://';

const fs = require('fs')

const db = require('./db')

const controller = require('./controller/check_request');

const hostname = '127.0.0.1'; //'www.malayatimes.com';

const helper = require('./helper')

// var NodeSession = require('node-session')
// var session = new NodeSession({
//    'secret':'J@v&k6=F0q_x*=cO0oI!ilMnWmNwDGQa',//[...Array(32)].map(i=>(~~(Math.random()*36)).toString(36)).join(''),
//    'domain':'.malayatimes.com',
//    'lifetime': 300000,
//    'cookie': 'ux_session'
// })




if (cluster.isMaster) {
  // console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  // cluster.on('exit', (worker, code, signal) => {
  //   console.log(`worker ${worker.process.pid} died`);
  // });
} else {

   // MY APPS START ///////////////////////////////////


   /* WWW.malayatimes.com */
   http.createServer(function(req, res) {
      //const url_info = require('url').parse(req.url, true);
      const current_url = new URL(protocol + hostname + req.url);
      var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

      var callback = function(res, req, err){if(err) throw err}

         if (req.url === '' || req.url === '/') {

            var data = new Array()
            data['ip_adress'] = visitor_ip;
            //data['url_info'] = url_info;

            data['head_open'] = fs.readFileSync('./head_open.html', 'utf8')
            data['css'] = fs.readFileSync('./homepage.css', 'utf8')
            data['head_close'] = fs.readFileSync('./head_close.html', 'utf8')
            data['menu_nav'] = fs.readFileSync('./menu_nav.html', 'utf8')
            data['header'] = fs.readFileSync('./www_index_header.html', 'utf8')
            data['popular_box'] = fs.readFileSync('./popular_box.html', 'utf8')

            // if(data['ip_adress'] == '203.82.79.96'){

               const send = require('./www/home-page/homepage_data')
               send.homepage_data(req, res, current_url, data)

            // } else {
            //
            //    const send = require('./www/homepage/homepage_data')
            //    send.homepage_data(req, res, current_url, data)
            //
            // }

         } else if(req.url == '/robots.txt'){
            res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
            res.write(fs.readFileSync('./robots.txt', 'utf-8'))
            res.end()
         } else {

            if ( req.url.slice(-1) === '/' || current_url.search ) {
               req.url = helper.get_canonical(req.url);//req.url.slice(0, -1);
               var redirect_to = helper.get_canonical(req.url);
               res.writeHead(301, {
                  'Location': redirect_to,
                  'Content-Type': 'text/plain'
               });
               res.end();
            } else {

                  var data = new Array()
                  data['ip_adress'] = visitor_ip;
                  //data['url_info'] = url_info;

                  data['head_open'] = fs.readFileSync('./head_open.html', 'utf8')
                  data['css'] = fs.readFileSync('./homepage.css', 'utf8')
                  data['head_close'] = fs.readFileSync('./head_close.html', 'utf8')
                  data['menu_nav'] = fs.readFileSync('./menu_nav.html', 'utf8')
                  data['header'] = fs.readFileSync('./www_index_header.html', 'utf8')

                  const send = require('./www/homepage_user/check_user')
                  send.check_user(req, res, current_url, data)
          }

     }

     // console.log(data['ip_adress'])

   }).listen(4000) // end create server




   /******************** SUB DOMAIN AREA ***********************/
   var data = new Array()
   data['head_open'] = fs.readFileSync('./head_open.html', 'utf8')
   data['head_close'] = fs.readFileSync('./head_close.html', 'utf8')
   data['menu_nav'] = fs.readFileSync('./element/nav.html', 'utf8')



   /* ARTIS.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'artis.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Gosip Artis Malaysia';
         data['meta_description'] = 'Gosip Artis Malaysia ok? Blog hiburan dan gosip kisah cerita video gambar artis Malaysia yang terkni terbaru panas dan lagi hot juga semasa';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4001); // end create server

   /* berita.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'berita.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Berita Terkini Malaysia';
         data['meta_description'] = 'Berita Terkini Malaysia 2018. Web berita dan info serta isu politik, jenayah semasa paling terkini juga terbaru di Malaysia';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4004); // end create server

   /* viral.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'viral.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Viral Malaysia';
         data['meta_description'] = 'Blog viral Malaysia adalah sebuah Laman web serta yang blog popular isu gambar video status artikel dan kisah viral terkini juga semasa serta terbaru di Malaysia';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4006); // end create server


   /* sukan.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'sukan.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'sukan Malaysia';
         data['meta_description'] = 'Blog sukan Malaysia adalah sebuah Laman web serta yang blog popular isu gambar video status artikel dan kisah sukan terkini juga semasa serta terbaru di Malaysia';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4007); // end create server


   /* politik.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'politik.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Berita Terkini Politik Malaysia';
         data['meta_description'] = 'Berita terkini isu semasa politik Malaysia terbaru. Web berita terkini berkenaan isu semasa politik Malaysia yang terbaru termasuk Pakatan Harapan, Barisan Nasional dan Bebas';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4008); // end create server

   /* blog.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'blog.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Official Malaya Times Blog';
         data['meta_description'] = 'Official Malaya Times Blog adalah laman pengumuman dan tips untuk Malaya Times users';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {

         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(4009); // end create server


   /* api.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;


      if (req.url === '' || req.url === '/') {
         /* home */

         res.writeHead(404, {
            'Content-Type': 'text/plain'
         });
         res.write('page under construction');
         res.end();
      } else if(req.url == '/robots.txt'){
         res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
         res.write(fs.readFileSync('./api-robots.txt', 'utf-8'))
         res.end()

      } else {

         var my_url = req['url'].split('?');
         my_url[0] = my_url[0].replace(/\./g,'')
         res['my_param'] = my_url[1] || false;

         // console.log(my_url);

         if ( my_url[0] && fs.existsSync('./api-dir'+my_url[0]+'.js') ) {

            const send = require('./api-dir'+my_url[0])
            send.filter_api(req, res)

         } else {

            res.writeHead(404, {
               'Content-Type': 'text/plain'
            });
            res.write('404');
            res.end();

         }

      }

   }).listen(5000); // end create server


   /* pendidikan.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'pendidikan.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Info Pendidikan Malaysia';
         data['meta_description'] = 'Info Pendidikan Malaysia ok? Blog info semakan dan rujukan pendidikan pelajar dan guru bagi peperiksaan, soalan, keputussan dan keputusan Malaysia.';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(5001); // end create server


   /* kesihatan.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      data['ip_adress'] = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;
      data['domain'] = 'kesihatan.malayatimes.com';


      if (req.url === '' || req.url === '/') {
         /* home */
         data['meta_title'] = 'Info kesihatan';
         data['meta_description'] = 'Info kesihatan Malaysia ok? Blog info rujukan dan tips kesihatan bagi rawatan dan pencegahan cara kampung dan moden.';
         data['css'] = fs.readFileSync('./homepage.css', 'utf8')
         data['header'] = fs.readFileSync('./element/header_index.html', 'utf8')
         const send = require('./blogView/homepage/homepage_data')

         send.homepage_data(req, res, current_url, data)

      } else {
         /* single */
         data['dir'] = 'blogView';
         data['header'] = fs.readFileSync('./element/header_single.html', 'utf8')
         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data);

      }

   }).listen(5002); // end create server







   /* hub.malayatimes.com */
   http.createServer(function(req, res) {

      const current_url = new URL(protocol + hostname + req.url);
      var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

      var data = new Array()
      data['ip_address'] = visitor_ip;
      data['domain'] = 'hub.malayatimes.com';

      if (req.url === '' || req.url === '/') {
         /* HOME */
         data['meta_title'] = 'Hub dan Wiki';
         data['meta_description'] = 'Hub profile figura, organisasi serta wiki bagi acara, peristiwa dan produk popular.';

         const send = require('./hub/homepage/1')
         send.check(req, res, current_url, data)

      } else {
         /* SINGLE */
         data['dir'] = 'hub';

         data['current_url'] = current_url;
         controller.check_request(req, res, current_url, data)

      }

   }).listen(4002); // end create server



   // SERVICE AREA ////////////////////////////////////////////

   http.createServer(function(req, res) {
      var visitor_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || false;

      if (visitor_ip.indexOf('127.0.0.1') > -1) {

         /* 1. UPDATE CURRENCY EXCHANGE USD TO MYR */
         if (req.url === '/update-myr') {

            const fx = require("yahoo-exchange")

            fx.getExchangeDataArray('USDMYR', function(data) {
               if (data) {
                  if (data[0]) {
                     if (!isNaN(data['0'])){

                        var jumlah = parseFloat(Math.round(data['0'] * 100) / 100).toFixed(2);



                        // update db..
                        db.statement(
                           //query..
                           'UPDATE site_data SET value = ? WHERE what = ?',
                           // str..
                           [
                              jumlah,
                              'usd_to_myr'
                           ],
                           //callback
                           false
                        );

                        // res.writeHead(200, {
                        //    'Content-Type': 'text/plain\; charset=utf-8'
                        // });
                        // res.write(jumlah);
                        // res.end();

                     } else {
                        console.log('Yahoo money exchange return NaN data')
                     }
                  }
               }
            });

         }


      }


   }).listen(4005) // end create server


   // MY APPS END   ///////////////////////////////////
}
