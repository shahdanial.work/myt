const db = require('../../db');
const helper = require('../../helper');
const send = require('./get_another_data');
const mysql = require('mysql');

//console.log('##################################################')

var getPostData = function(req,res,current_url, data){

    /* 1. CHECK path, if path available in MySQL post_tbl WHERE category is viral */

    var singlePostData = function(err1, rows) { //what ever category from post_tbl..
        if(err1){
            console.log('error SinglePostData Viral sql', err1);
            throw err1;
        } else {

            data['post'] = rows[0];
            send.get_another_data(req,res,current_url, data)

        }
    }
    var query = 'SELECT post_tbl.*, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = "post_tbl" AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = "post_tbl" AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path AND tag_tbl.status = "publish" WHERE post_tbl.path = ? AND category_manager.domain = ? GROUP BY post_tbl.id ORDER BY post_tbl.id ASC LIMIT 0,1';

    var string = [
        data['path'],
        data['domain']
    ];

    db.statement(query, string, singlePostData);

}
module.exports.getPostData = getPostData;
