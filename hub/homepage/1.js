var check = function(req, res, current_url, data){


   res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'});
   res.write('<!DOCTYPE html>\n')
   res.write('<html class="main-blog">')
   res.write('<head>')
   res.write('<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/><meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>')
   res.write('<title>'+data['meta_title']+'</title><meta property="og:description" name="twitter:description" content="'+data['meta_description']+'" name="description"/><meta rel="canonical" content="https://hub.malayatimes.com" property="og:url"/>')
   res.write('<meta name="twitter:site" content="@malayatimes"><meta property="og:title" name="twitter:title" content="Malaya Times"><meta property="og:description" name="twitter:description" content="'+data['meta_description']+'"><meta property="og:type" content="website"><meta name="twitter:card" content="summary_large_image"><link href="https://media.malayatimes.com/malaya-times.jpg" rel="image_src"/><meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/malaya-times.jpg"><meta property="fb:app_id" content="1829602560385819">')

   res.write('<script src="https://media.malayatimes.com/js/es5.js"></script>')

   res.write('<link rel="stylesheet" type="text/css" href="https://media.malayatimes.com/css/hub-home.css"/>')

   res.write('</head>')

   res.write('<body id="main-blog">')

   res.write('<nav id="nav">')
      res.write(`
         <div class="nav-wrapper section" id="pagelist">
         <ul class="nav">
         <li class="menu-item"><a href="https://hub.malayatimes.com/figura">Figura</a></li>
         <li class="menu-item"><a href="https://hub.malayatimes.com/Organisasi">Organisasi</a></li>
         <li class="menu-item"><a href="https://hub.malayatimes.com/produk">Produk</a></li>
         <li class="menu-item"><a href="https://hub.malayatimes.com/peristiwa">Peristiwa</a></li>
         <li class="menu-item"><a href="https://hub.malayatimes.com/acara">Acara</a></li>
         <li class="menu-item"><a href="https://hub.malayatimes.com/informasi">Informasi</a></li>
         </ul><button id="mobile-menu-toggle">&#9776;</button>
         <aside class="aside">
         <ul>
         <li class="button"><a href="https://media.malayatimes.com/add-wiki">Tulis wiki</a></li>
         <!--<li class="search"><a href="login">Login</a></li>-->
         </ul>
         </aside>
         </div>`)
   res.write('</nav>')

   res.write('<header id="header">')
      res.write('<h1 id="site-title"><a href="https://hub.malayatimes.com" rel="home">'+ data['meta_title'] +'</a></h1>')
      res.write('<span id="site-description">'+data['meta_description']+'</span>')
   res.write('</header>')

   res.write('<main id="main">')
   res.write('<div class="main">')
   res.write('<form id="form" action="/cari">')
      res.write('<img src="https://2.bp.blogspot.com/-ot_onnsRc04/W2QmHNZOPmI/AAAAAAAAC5M/eA8spoR6p8AYmTLyLaH3CQMmKhxG4bBXwCLcBGAs/s1600/hub-malayatimes.png">')
      res.write('<div id="search-box">')
      res.write('<input name="keyword" id="search-input" type="text" placeholder="Taip carian.." required="required"><button type="submit" id="search-button" class="cari"></button><div id="search-suggest"></div>')
      res.write('</div>')
   res.write('</form>')
   res.write('</div>')
   res.write('</main>')

   res.write('<footer>')
   //res.write('<div id="footer-content">Advertise</div>')
   res.write('<aside id="not-important" class="not-important">')
   res.write('<script type="text/javascript" src="https://media.malayatimes.com/js/hub/home.js"></script>')
   res.write('</aside>')
   res.write('</footer>')
   res.write('</body></html>')

   res.end()
}
module.exports.check = check;