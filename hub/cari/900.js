const helper = require('../../helper')
const fs = require('fs')
const db = require('../../db')

var setup_iklan = function(req, res, current_url, data){
   var item = data.item || []

   var postData = []

   for (var i = 0; i < item.length; i++) {
      if( item[i].from_tbl == 'tag' || item[i].from_tbl == 'post' ){
         var bucket = {}
         bucket.from_tbl = item[i].from_tbl
         bucket.from_id = item[i].id
         postData.push(bucket)
      }
   }

   if(postData.length < 1){

      var next = require('./999')
      next.serve(req, res, current_url, data)

   } else {

      var last_array = parseFloat(postData.length) - parseFloat(1)
      var myquery = 'SELECT DISTINCT myvalue FROM keyword_manager WHERE (';
      var mystring = []
      for (var i = 0; i < postData.length; i++) {
         myquery += '(from_tbl = ? AND from_id = ?)'
         if(i != last_array && postData.length > 1) myquery += ' OR '
         mystring.push(postData[i].from_tbl+'_tbl')
         mystring.push(parseFloat(postData[i].from_id))
      }
      myquery += ") AND (mykey = 'interest')"
      // console.log(myquery, mystring);

      var callback = function(err,rows){

         if(err) throw err;

         if(rows){

            var arr_keyword = helper.build_keyword_v2(rows)

            var a = '<script async src="https://www.googletagservices.com/tag/js/gpt.js"></script><script>';
            var b = fs.readFileSync('./adsense/1-minify', 'utf8')
            var c = fs.readFileSync('./adsense/2-minify', 'utf8')
            var d = ';var totalResult = '+parseFloat(data.item.length)+'</script>';

            data['adsense_js'] = a + b + arr_keyword + c + d;

         }
         var next = require('./999')
         next.serve(req, res, current_url, data)

      }
      db.statement(myquery, mystring, callback)

   }
}
module.exports.setup_iklan = setup_iklan