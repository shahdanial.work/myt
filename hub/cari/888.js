const db = require('../../db')
const mysql = require('mysql')
var mari_record = function(data){
   if(data.param.keyword && data.ip_address){

      if(data.param.keyword.length > 1){
         var keyword = data.param.keyword.replace(/[^0-9a-z ]/gi, '').toLowerCase()

         var check_same = function(err1, rows) {
            if(err1){
                 throw err1;
            } else {
               if(!rows[0]){
                  if(keyword.length>0) db.statement('INSERT INTO searched_tbl (keyword, ip) VALUES (?, ?)',  [keyword, data.ip_address], false)
               }
            }
         }
         db.statement('SELECT id FROM searched_tbl WHERE keyword = ? AND ip = ?',  [keyword, data.ip_address], check_same)
      }

   }
}
module.exports.mari_record = mari_record