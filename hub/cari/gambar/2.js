// PROCEED DB DATA. (ALL)
const db = require('../../../db')
const helper = require('../../../helper')
const mysql = require('mysql')

var db_get = function(req, res, current_url, data){

   var json = false
   if(data['param']){
      if(data['param']['type']){
         if(data['param']['type'] === 'json') json = true
      }
   }

    //callback
    var query_callback = function(err1, rows) {
      if(err1) throw err1;
      data['img'] = rows || [];
      if(json){
         const next = require('./json')
         next.serve_json(req, res, current_url, data)
      } else {
         data['item'] = []
         const next = require('./../999')
         next.serve(req, res, current_url, data)
      }
    }

    if(!data.param.limit) data.param.limit = 19
    if(data.param.limit > 19) data.param.limit = 19

    // query
    var query =
      `SELECT
      file_tbl.id as id,
      file_tbl.caption as title,
      null as alt_title,
      null as type,
      'media.malayatimes.com' as domain,
      file_tbl.filename as link,
      file_tbl.filename as thumb,
      null as description,
      null as alt_desc,
      'image' as from_tbl
      FROM file_tbl
      WHERE MATCH(file_tbl.caption) AGAINST(?  IN BOOLEAN MODE)

      ORDER BY id
      DESC

      LIMIT ?,?`;

    // string
    var string = [
      data['param']['keyword'],
      parseFloat(data.param.offset),
      parseFloat(data.param.limit)
   ];

    db.statement(query, string, query_callback);
}
module.exports.db_get = db_get