
var serve_json = function(req, res, current_url, data){
var obj = []

   if(data.img){

      for (var i = 0; i < data.img.length; i++) {

         var boxs = {}

         var item = data.img[i]
         var title = item.title.replace(/\"/g,"'")
         if(title.length < 1 && item.alt_title) title = item.alt_title.replace(/\"/g,"'")

            // res.write('<a class="image" href="https://media.malayatimes.com/image/'+item.thumb.replace(/\./g,'--')+'" class="img"><img src="https://media.malayatimes.com/image/'+item.thumb+'" alt=" '+title+' "></a>')

            boxs.title = title
            boxs.link = 'https://media.malayatimes.com/image/'+item.thumb.replace(/\./g,'--')
            boxs.src = 'https://media.malayatimes.com/image/'+item.thumb
            boxs.inner = '<img src="https://media.malayatimes.com/image/'+item.thumb+'" alt=" '+title+' ">'
            obj.push(boxs)

      }
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write(JSON.stringify(obj, null, 0))
      res.end()
   } else {
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write('{"boxs":[]}')
      // res.write(obj)
      res.end()
   }
}
module.exports.serve_json = serve_json