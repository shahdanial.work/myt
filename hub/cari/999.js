const fs = require('fs')
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();
const helper = require('../../helper')

const jsdom = require("jsdom")
const {
   JSDOM
} = jsdom;

var serve = function(req, res, current_url, data) {
   res.writeHead(200, {
      'Content-Type': 'text/html\; charset=utf-8'
   });
   res.write('<!DOCTYPE html>\n')
   res.write('<html class="main-blog">')
   res.write('<head>')
   res.write('<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/><meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>')
   res.write('<title>Search result: ' + data.param.keyword + '</title><meta property="og:description" name="twitter:description" content="check out wiki and articles for keyword: ' + data.param.keyword + '" name="description"/><meta rel="canonical" content="https://hub.malayatimes.com" property="og:url"/>')
   res.write('<meta name="twitter:site" content="@malayatimes"><meta property="og:title" name="twitter:title" content="Malaya Times"><meta property="og:description" name="twitter:description" content="check out wiki and articles for keyword: ' + data.param.keyword + '"><meta property="og:type" content="website"><meta name="twitter:card" content="summary_large_image"><link href="https://media.malayatimes.com/malaya-times.jpg" rel="image_src"/><meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/malaya-times.jpg"><meta property="fb:app_id" content="1829602560385819">')

   res.write('<script src="https://media.malayatimes.com/js/es5.js"></script>')

   if(data['adsense_js']) res.write(data['adsense_js'])

   res.write('<link rel="stylesheet" type="text/css" href="https://media.malayatimes.com/css/hub-cari.css"/>')

   res.write('</head>')

   res.write('<body id="main-blog">')


   res.write('<aside id="top"><div class="width">')
   res.write('<form id="form" action="/cari">')

   res.write('<a href="https://hub.malayatimes.com" id="logo"><img src="https://3.bp.blogspot.com/-oP3dOJzTGVg/W2TCpuSanHI/AAAAAAAAC5Y/cvM0nSx77oUW6KE1Omum7KT_5JPGhC3OACLcBGAs/s1600/m-white.png"></a>')

   res.write('<div id="search-box">')
   res.write('<input name="keyword" id="search-input" type="text" value="' + data.param.keyword + '" placeholder="mulakan carian.." required="required"><button type="submit" id="search-button" class="cari"></button>')
   res.write('</div>')

   res.write('<div id="search-suggest"></div>')

   res.write('<input type="hidden" id="nak_filter" name="filtering">')

   res.write('</form>')
   res.write(`<aside class="aside">
   <ul>
   <li class="button"><a href="https://media.malayatimes.com/add-wiki">Tulis wiki</a></li>
   <!--<li class="search"><a href="login">Login</a></li>-->
   </ul>
   </aside>`)
   res.write('</div></aside>')

   res.write('<div class="option" id="sticky"><div class="width">')

   res.write(`
      <div class="section" id="pagelist">
      <ul class="option-list">
      <li class="menu-item"><input class="filter_search" id="nak_all" name="nak_all" type="checkbox"`)
   if (data.param.filtering == 'nak_all') res.write(' checked="true"')

   res.write(`><label for="nak_all">Semua</label></li>
      <li class="menu-item"><input class="filter_search" id="nak_wiki" name="nak_wiki" type="checkbox"`)
   if (data.param.filtering == 'nak_wiki') res.write(' checked="true"')

   res.write(`><label for="nak_wiki">Wiki</label></li>
      <li class="menu-item"><input class="filter_search" id="nak_post" name="nak_post" type="checkbox"`)
   if (data.param.filtering == 'nak_post') res.write(' checked="true"')

   res.write(`><label for="nak_post">Artikel</label></li><li class="menu-item"><input class="filter_search" id="nak_gambar" name="nak_gambar" type="checkbox"`)
   if (data.param.filtering == 'nak_gambar') res.write(' checked="true"')

   res.write(`><label for="nak_gambar">Gambar</label></li>
      </ul><button id="mobile-menu-toggle">&#9776;</button>
      </div>`)
   res.write('</div></div>')

   var input = data.param

   res.write('<div id="main"><div class="width ' + data.param.filtering + '">')
   res.write('<main id="results" class="inline inline_left">')
   //////////////////// R.E.S.U.L.T ///////////////////

   data.img = data.img || []
   var img_ada = false

   /* IMAGE */
   if (data.img) {
      img_ada = true
      button = false
      // var c = 0
      if (input.filtering != 'nak_gambar' && data.img.length > 0) res.write('<form class="result" id="image-result" action="/cari">')
      // if(input.filtering != 'nak_gambar')res.write('<div class="row">')
      for (var i = 0; i < data.img.length; i++) {

         var item = data.img[i]
         var title = item.title.replace(/\"/g, "'")
         if (title.length < 1 && item.alt_title) title = item.alt_title.replace(/\"/g, "'")

         if (i < 6 && input.filtering != 'nak_gambar') {
            /* nak all*/
            // c++

            res.write('<a class="image" href="https://media.malayatimes.com/image/' + item.thumb.replace(/\./g, '--') + '" class="img"><img src="https://media.malayatimes.com/image/' + item.thumb + '" alt=" ' + title + ' "></a>')

            // if(c == 3 && data.img.length > 3){
            //    c = 0
            //    res.write('</div><div class="rows">')
            // }
         } else if (input.filtering != 'nak_gambar') {
            button = true
         }
         if (input.filtering == 'nak_gambar' && i < 19) {
            /* nak gambar*/

            res.write('<a class="image" href="https://media.malayatimes.com/image/' + item.thumb.replace(/\./g, '--') + '" class="img"><img src="https://media.malayatimes.com/image/' + item.thumb + '" alt=" ' + title + ' "></a>')
         }

      }
      //close tag dan add extra input for form.. (that's mean this page is need autoload)
      if (input.filtering != 'nak_gambar' && data.img.length > 0) {
         // res.write('</div>')
         if (button) res.write('<button type="submit" id="tp">MORE IMAGES</button>')
         res.write('<input type="hidden" name="keyword" value="' + data.param.keyword + '">')
         res.write('<input type="hidden" name="filtering" value="nak_gambar">')
         res.write('</form>')
      }
   }

   data['item'] = data['item'] || []
   var item_ada = false
   /* WIKI/CATEGORY/POST */
   if (data['item'].length > 0) {
      item_ada = true
      var items = data.item
      for (var i = 0; i < items.length; i++) {
         if (i < 10) {
            var item = items[i]
            var title = item.title.replace(/\"/g, "'")
            if (title.length < 1 && item.alt_title) title = item.alt_title.replace(/\"/g, "'")

            res.write('<div class="result ' + item.from_tbl + '" data-type="' + item.from_tbl + '"><div class="inner">')

            if (item.from_tbl == 'tag') var link = 'http://' + item.domain + '/' + item.type + '/' + item.link
            if (item.from_tbl == 'category') var link = 'http://' + item.domain
            if (item.from_tbl == 'post') var link = 'http://' + item.domain + '/' + item.link


            res.write('<h2 class="item-title"><a href="' + link + '">' + title + '</a></h2>')
            res.write('<div class="box_wrapper">')
            var thumber = 120
            if (item.thumb) {
               thumber = 100
               res.write('<a class="img thumb" href="' + link + '"><img alt=" ' + title + ' " src="https://media.malayatimes.com/picture/' + item.thumb + '"></a>')
            }
            var desc = item.description
            if (desc.length < 1 && item.alt_desc) {
               if (item.alt_desc.length > 0) {
                  var dom = new JSDOM('<div id="jjjjjjj">' + item.alt_desc + '</div>')
                  var first_para = dom.window.document.getElementsByTagName('p')[0]
                  if (first_para) desc = first_para.textContent.substring(0, thumber)
               }
            }
            if (desc.length < 1) desc = 'no description snippets'
            res.write('<p class="item-description">')
            res.write(desc + ' ...')
            res.write('</p>')
            res.write('</div>')

            res.write('</div></div>')
         }

      }

   }

   if (!img_ada && !item_ada) {
      res.write('<div class="no-result"><div class="msg">Ops, related dengan carian <i>' + data.param.keyword + '</i>, sila tulis <a class="button" href="https://media.malayatimes.com/add-post">Artikel</a> atau <a class="button" href="https://media.malayatimes.com/add-wiki">Wiki</a> untuk <i>' + data.param.keyword + '</i></div></div>')
   }



   //////////////////// R.E.S.U.L.T ///////////////////
   res.write('</main>')
   if (input.filtering != 'nak_gambar') {
      res.write('<aside id="sidebar" class="inline inline_right">')
      res.write('<div class="widget">')
      res.write('<div class="premium sticker">')
         res.write('<div id="div-gpt-ad-1524121116261-0" class="premium"><script>if(is_visible(document.getElementById("sidebar"))){console.log("sidebar visible");googletag.cmd.push(function() { googletag.display("div-gpt-ad-1524121116261-0") })}</script></div>')
         //res.write('<img src="https://media.malayatimes.com/assets/image/iklan-square.png">')
      res.write('</div>')
      res.write('</div>')
      res.write('</aside>')
   }
   res.write('</div></div>')

    res.write('<aside id="loading-spiner"><div class="center"><span class="loader"><span class="pusing"></span></span><noscript>Allow Javascript to load more result by click the green `secure` button in address bar..</noscript></div></aside>')

   res.write('<footer>')
   //res.write('<div id="footer-content">Advertise</div>')
   res.write('<aside id="noti-important" class="not-important">')
   res.write('<script type="text/javascript" src="https://media.malayatimes.com/js/hub/cari.js"></script>')
   res.write('</aside>')
   res.write('</footer>')
   res.write('</body></html>')

   res.end()

}
module.exports.serve = serve
