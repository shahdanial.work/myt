// PROCEED DB DATA. (ALL)
const db = require('../../../db')
const helper = require('../../../helper')
const mysql = require('mysql')

var db_get = function(req, res, current_url, data){

   var json = false
   if(data['param']){
      if(data['param']['type']){
         if(data['param']['type'] === 'json') json = true
      }
   }

    //callback
    var query_callback = function(err1, rows) {
      if(err1) throw err1;
      if(rows) data['item'] = rows
      else data['item'] = [];
      if(json){
         const next = require('./json')
         next.serve_json(req, res, current_url, data)
      } else {
         const next = require('./3')
         next.get_img(req, res, current_url, data)
      }
    }

    if(!data.param.limit) data.param.limit = 11
    if( data.param.limit > 11)  data.param.limit = 11
    // query
    var query =
      `SELECT
      category_tbl.id as id,
      category_tbl.meta_title as title,
      category_tbl.title as alt_title,
      null as type,
      category_tbl.domain as domain,
      null as link,
      null as thumb,
      category_tbl.meta_description as description,
      null as alt_desc,
      'category' as from_tbl
      FROM category_tbl
      WHERE MATCH(title) AGAINST(?  IN BOOLEAN MODE)


      UNION DISTINCT


      SELECT
      tag_tbl.id as id,
      tag_tbl.meta_title as title,
      tag_tbl.title as alt_title,
      tag_tbl.type as type,
      'hub.malayatimes.com' as domain,
      tag_tbl.tag_path as link,
      tag_tbl.first_img as thumb,
      tag_tbl.meta_description as description,
      tag_tbl.content as alt_desc,
      'tag' as from_tbl
      FROM tag_tbl
      WHERE MATCH(title) AGAINST(?  IN BOOLEAN MODE) AND tag_tbl.status = 'publish'


      UNION DISTINCT


      SELECT
      post_tbl.id as id,
      post_tbl.meta_title as title,
      post_tbl.title as alt_title,
      null as type,
      category_manager.domain as domain,
      post_tbl.path as link,
      post_tbl.first_img as thumb,
      post_tbl.meta_description as description,
      post_tbl.content as alt_desc,
      'post' as from_tbl
      FROM post_tbl
      LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id
      WHERE MATCH(title) AGAINST(?  IN BOOLEAN MODE)  AND post_tbl.status = 'publish'

      ORDER BY id
      DESC

      LIMIT ?,?`;

    // string
    var string = [
      data['param']['keyword'],
      data['param']['keyword'],
      data['param']['keyword'],
      parseFloat(data.param.offset),
      parseFloat(data.param.limit)
   ];

    db.statement(query, string, query_callback)
}
module.exports.db_get = db_get