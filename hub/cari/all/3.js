// PROCEED DB DATA. (ALL)
const db = require('../../../db')
const helper = require('../../../helper')
const mysql = require('mysql')

var get_img = function(req, res, current_url, data){

    //callback
    var query_callback = function(err1, rows) {
      if(err1) throw err1;
      if(rows) data['img'] = rows
      const next = require('./../900')
      next.setup_iklan(req, res, current_url, data)
    }

    // query
    var query =
      `SELECT
      file_tbl.id as id,
      file_tbl.caption as title,
      null as alt_title,
      null as type,
      'media.malayatimes.com' as domain,
      file_tbl.filename as link,
      file_tbl.filename as thumb,
      null as description,
      null as alt_desc,
      'image' as from_tbl
      FROM file_tbl
      WHERE MATCH(file_tbl.caption) AGAINST(?  IN BOOLEAN MODE)
      ORDER BY file_tbl.id
      DESC
      LIMIT 0,7`;

    // string
    var string = [
      data['param']['keyword']
   ];

    db.statement(query, string, query_callback)
}
module.exports.get_img = get_img