//SET UP INPUT


var kkk = function(req, res, current_url, data){

   var url = require('url');
   var url_parts = url.parse(req.url, true);
   var query = url_parts.query;

   var ok = false;
   if(query){
      if(query.keyword){
         if(query.keyword.replace(/\s\s+/g, ' ').trim().length>0) ok = true, query.keyword = query.keyword.replace(/\s\s+/g, ' ').trim()
      }
   }

   data['param'] = query;
   if(!data.param.offset) data.param.offset = 0
   if(!data.param.filtering) data.param.filtering = 'nak_all';
   if(data.param.filtering == 'nak_all') var next = require('./all/2')
   if(data.param.filtering == 'nak_wiki') var next = require('./wiki/2')
   if(data.param.filtering == 'nak_post') var next = require('./post/2')
   if(data.param.filtering == 'nak_gambar') var next = require('./gambar/2')

   if(ok){

      next.db_get(req, res, current_url, data)
   } else {
      res.writeHead(301, {
         'Location': '/',
         'Content-Type': 'text/plain'
      });
      res.end()
   }

   // record data.
   var record = require('./888.js')
   record.mari_record(data)

}

module.exports.kkk = kkk;