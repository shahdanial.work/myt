// PROCEED DB DATA. (ALL)
const db = require('../../../db')
const helper = require('../../../helper')
const mysql = require('mysql')

var db_get = function(req, res, current_url, data){

   var json = false
   if(data['param']){
      if(data['param']['type']){
         if(data['param']['type'] === 'json') json = true
      }
   }

    //callback
    var query_callback = function(err1, rows) {
      if(err1) throw err1;
      data['item'] = rows || [];
      if(json){
         const next = require('./json')
         next.serve_json(req, res, current_url, data)
      } else {
         const next = require('./../900')
         next.setup_iklan(req, res, current_url, data)
      }
    }

    if(!data.param.limit) data.param.limit = 11
    if(data.param.limit > 11) data.param.limit = 11

    // query
    var query =
      `SELECT
      post_tbl.id,
      post_tbl.meta_title as title,
      post_tbl.title as alt_title,
      null as type,
      category_manager.domain as domain,
      post_tbl.path as link,
      post_tbl.first_img as thumb,
      post_tbl.meta_description as description,
      post_tbl.content as alt_desc,
      'post' as from_tbl
      FROM post_tbl
      LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id
      WHERE MATCH(title) AGAINST(?  IN BOOLEAN MODE)  AND post_tbl.status = 'publish'

      LIMIT 0,11`;

    // string
    var string = [
      data['param']['keyword'],
      parseFloat(data.param.offset),
      parseFloat(data.param.limit)
   ];

    db.statement(query, string, query_callback);
}
module.exports.db_get = db_get