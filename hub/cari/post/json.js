
var serve_json = function(req, res, current_url, data){

   data['item'] = data['item'] || []
   var item_ada = false
   /* WIKI/CATEGORY/POST */
   if(data['item'].length > 0){

         const jsdom = require("jsdom")
         const {
            JSDOM
         } = jsdom;

         var items = data.item
         var obj = []

         for (var i = 0; i < items.length; i++) {
            var boxs = {}
            var box = '';

            var item = items[i]
            var title = item.title.replace(/\"/g,"'")
            if(title.length < 1 && item.alt_title) title = item.alt_title.replace(/\"/g,"'")

            box +='<div class="inner">'

                  if(item.from_tbl == 'tag') var link = 'https://'+ item.domain +'/'+ item.type +'/'+ item.link
                  if(item.from_tbl == 'category') var link = 'https://'+ item.domain
                  if(item.from_tbl == 'post') var link = 'https://'+ item.domain +'/'+ item.link


                  box+='<h2 class="item-title"><a href="'+link+'">'+title+'</a></h2>'
                  box+='<div class="box_wrapper">'
                     var thumber = 120
                     if(item.thumb){
                        thumber = 100
                        box+='<a class="img thumb" href="'+link+'"><img alt=" '+title+' " src="https://media.malayatimes.com/picture/'+item.thumb+'"></a>'
                     }
                     var desc = item.description
                     if(desc.length < 1 && item.alt_desc){
                        if(item.alt_desc.length > 0){
                           var dom = new JSDOM('<div id="jjjjjjj">' + item.alt_desc + '</div>')
                           var first_para = dom.window.document.getElementsByTagName('p')[0]
                           if(first_para) desc = first_para.textContent.substring(0, thumber)
                        }
                     }
                     if(desc.length < 1) desc = 'no description snippets'
                     box+='<p class="item-description">'
                     box+=desc+' ...'
                     box+='</p>'
                  box+='</div>'

            box+='</div>'

             boxs.type = item.from_tbl
             boxs.title = title
             boxs.link = link
             boxs.thumb = 'https://media.malayatimes.com/picture/'+item.thumb
             boxs.desc = desc+' ...'
             boxs.inner = box

             obj.push(boxs)
         }
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write(JSON.stringify(obj, null, 0))
      // res.write(obj)
      res.end()
   } else {
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write('{"boxs":[]}')
      // res.write(obj)
      res.end()
   }
}
module.exports.serve_json = serve_json