// PROCEED DB DATA. (ALL)
const db = require('../../../db')
const helper = require('../../../helper')
const mysql = require('mysql')

var db_get = function(req, res, current_url, data){

   var json = false
   if(data['param']){
      if(data['param']['type']){
         if(data['param']['type'] === 'json') json = true
      }
   }

    //callback
    var query_callback = function(err1, rows) {
      if(err1) throw err1;
      data['item'] = rows || [];
      if(json){
         const next = require('./json')
         next.serve_json(req, res, current_url, data)
      } else {
         const next = require('./../900')
         next.setup_iklan(req, res, current_url, data)
      }
    }

    if(!data.param.limit) data.param.limit = 11
    if(data.param.limit > 11) data.param.limit = 11

    // query
    var query =
      `SELECT
      tag_tbl.id as id,
      tag_tbl.meta_title as title,
      tag_tbl.title as alt_title,
      tag_tbl.type as type,
      'hub.malayatimes.com' as domain,
      tag_tbl.tag_path as link,
      tag_tbl.first_img as thumb,
      tag_tbl.meta_description as description,
      tag_tbl.content as alt_desc,
      'tag' as from_tbl
      FROM tag_tbl
      WHERE MATCH(title) AGAINST(?  IN BOOLEAN MODE) AND tag_tbl.status = 'publish'

      LIMIT ?,?`;

    // string
    var string = [
      data['param']['keyword'],
      parseFloat(data.param.offset),
      parseFloat(data.param.limit)
   ];

    db.statement(query, string, query_callback);
}
module.exports.db_get = db_get