const helper = require('../../helper')
const db = require('../../db')
const mysql = require('mysql')

var get_another_data = function (req, res, current_url, data) {

    /* NANTI KALO ADA NAK AMEK APA2 DATA GUNAKAN FILE NI */

    if (data) {
        if (data['post']) {

            var latest_post_tag = function(err, rows) { //what ever category from post_tbl..
                if(err){
                    throw err;
                } else {
                    // berjaya
                    data['latest_post'] = rows; // this is array. maybe contains multiple of post data.
                    // var send = require('./serve-client.js')
                    // send.view(req, res, current_url, data)
                    var send = require('./format_data.js')
                    send.format_data(req, res, current_url, data)
                }

            }
            var query = "SELECT post_tbl.*, category_manager.domain FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id WHERE tag_manager.tag_path = ? GROUP BY post_tbl.id ORDER BY post_tbl.id ASC LIMIT 0,10";
            var string = [
                data['post']['tag_path']
            ];
            db.statement(query, string, latest_post_tag)

        }
    }


} // end module..

module.exports.get_another_data = get_another_data;