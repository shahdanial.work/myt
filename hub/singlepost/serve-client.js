const Entities = require('html-entities').XmlEntities;
const entities = new Entities();
const helper = require('../../helper')


var view = function(req, res, current_url, data){

   if(!data['post']){
      // 404
   } else {

      var post =  data['post']
      var url_share = 'https://'+data.domain +'/'+ post.type +'/'+ encodeURIComponent(post.tag_path)

      //////////////////////////////

      res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'})
      res.write('<!DOCTYPE html>')

      /* HEAD AREA */
      res.write('<head>')
      res.write('<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/><meta content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>')

      /////////// SEO meta ////////////
      var seo_title = 'Tiada title';
      if (post.meta_title) {
         seo_title = post.meta_title;
      } else if (post.title) {
         seo_title = post.title;
      }
      res.write('<title>' + entities.encode(seo_title) + '</title>')

      if (post.meta_description) res.write('<meta content="' + entities.encode(post.meta_description) + '" name="description"/>')

      if (post.google_thumb) {
         var meta_image_src = 'foto/' + post.google_thumb;
      } else if (post.viral_thumb) {
         var meta_image_src = 'foto/' + post.viral_thumb;
      } else if (post.first_img) {
         var meta_image_src = 'foto/' + post.first_img;
      } else {
         var meta_image_src = 'malaya-times.jpg';
      }
      res.write('<link href="https://media.malayatimes.com/' + meta_image_src + '" rel="image_src"/>')


      /////////// OGP.ME (Twitter, wFacebook and Google+ use it) ////////////

      res.write('<meta property="og:type" content="article">')

      res.write('<meta name="twitter:card" content="summary_large_image">')

      if (post.viral_thumb) {
         var thumbnail_viral = 'foto/' + post.viral_thumb;
      } else if (post.google_thumb) {
         var thumbnail_viral = 'foto/' + post.google_thumb;
      } else if (post.first_img) {
         var thumbnail_viral = 'foto/' + post.first_img;
      } else {
         var thumbnail_viral = 'malaya-times.jpg'
      }
      res.write('<meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/' + thumbnail_viral + '">')


      res.write('<meta name="twitter:site" content="@malayatimes">')

      if (post.viral_title) {
         var viral_title = post.viral_title
      } else {
         var viral_title = post.title;
      }
      res.write('<meta property="og:title" name="twitter:title" content="' + entities.encode(viral_title) + '">')

      if (post.viral_description) {
         var viral_description = post.viral_description;
      } else if (post.meta_description) {
         var viral_description = post.meta_description;
      } else if (post.title) {
         var viral_description = post.title + ' trending di Malayatimes. Baca lanjut ' + post.title + ' yang sedang viral kini.';
      } else {
         var viral_description = false;
      }
      if (viral_description) res.write('<meta property="og:description" name="twitter:description" content="' + entities.encode(viral_description) + '">')

      res.write('<meta rel="canonical" content="'+url_share+'" property="og:url">  ')

      res.write('<meta property="fb:app_id" content="1829602560385819">')

      if(data.keyword) res.write(data.keyword)

      res.write('<link rel="stylesheet" type="text/css" href="https://media.malayatimes.com/css/hub-cari.css">')
      res.write('</head>')

      res.write('<body id="wiki-page" class="cols_2">')


      /* TOP AREA */
      res.write('<aside id="top"><div class="width"><form id="form" action="https://hub.malayatimes.com/cari"> <a href="https://hub.malayatimes.com" id="logo"><img src="https://3.bp.blogspot.com/-oP3dOJzTGVg/W2TCpuSanHI/AAAAAAAAC5Y/cvM0nSx77oUW6KE1Omum7KT_5JPGhC3OACLcBGAs/s1600/m-white.png"></a><div id="search-box"> <input name="keyword" id="search-input" type="text" value="" autocomplete="off" placeholder="mulakan carian.." required="required"><button type="submit" id="search-button" class="cari"></button> </div><div id="search-suggest"></div><input type="hidden" id="nak_filter" name="filtering"></form><div class="aside"><ul><li class="button"><a href="https://media.malayatimes.com/add-wiki">Tulis wiki</a></li></ul></div></div></aside>')


      /* NAV AREA */
      res.write('<div class="option"> <div class="width"> <div class="section"> <ul class="option-list"> <li class="menu-item"><a href="//www.malayatimes.com" title="Berita Semasa Malaysia">Utama</a></li> <li class="menu-item"><a href="//politik.malayatimes.com" title="Berita Terkini Politik Malaysia">Politik</a></li> <li class="menu-item"><a href="//artis.malayatimes.com" title="Gosip Artis Malaysia">Artis</a></li> <li class="menu-item berita"><a href="//berita.malayatimes.com" title="Berita Terkini Malaysia">Berita</a></li> <li class="menu-item"><a href="//viral.malayatimes.com" title="Viral Malaysia">Viral</a></li> <li class="menu-item"><a href="//sukan.malayatimes.com" title="Berita Sukan Malaysia">Sukan</a></li> <li class="menu-item"><a href="//pendidikan.malayatimes.com" title="Info pendidikan Malaysia">Pendidikan</a></li> </ul> <button id="mobile-menu-toggle">☰</button> </div> </div> </div>')

      // adsense bwh header..
      if(data.showAds){
         if(data.showAds.adseseHeader) res.write(data.showAds.adseseHeader);
      }

      /* OPEN WRAPPER AREA */
      res.write('<div id="main"><div class="width">')

      /* LEFT AREA */
      res.write('<main class="main inline inline_left">')
         res.write('<article id="post" class="post hentry">')

         // title
         res.write('<h1 id="title" class="entry-title">'+entities.encode(post.title)+'</h1>')

         /* META POST */
         if (post['namapena'] || post['masa']) {
            res.write('<p class="entry-meta headline-meta" id="headline_meta">')
            if (post['namapena']) {
               res.write('<span class="vcard author"><span class="fn" title="' + post['namapena'] + '"><a href="//www.malayatimes.com/' + post['namapena'] + '" rel="author" title="author profile">' + post['namapena'] + '</a></span></span>')
            }

            if (post['masa']) {
               //var masa_rpc = post['masa'].replace(' ', 'T');
               var masa_utc = new Date(post['masa']).toISOString()
               res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')
            }

            res.write('</p>')
         }





         /* SOCIAL SHARE BUTTON */
         res.write('<aside id="share" class="share_buttons social_share">')
         res.write('<p class="share-count-container"><em class="shares-count">'+helper.number_k(post.pageview)+'</em><span class="label">shares</span></p>')
         res.write('<p class="social-buttons">')

         res.write('<span class="label_button">Share this news with your friends </span>')

         res.write('<a rel="nofollow" target="_blank" id="share-facebook" class="button-share facebook " href="//www.facebook.com/dialog/share?app_id=1829602560385819&display=page&href=' + url_share + '"><img alt="Share berita ' + post['title'] + ' di Facebook " src="//2.bp.blogspot.com/-gMk-7bgjdPE/Vvg2rzB6_LI/AAAAAAAAAp0/LcIOou21K1cG-h1jBer9sWRteljCrrbow/s1600/fb-icon.png"></a>')
         res.write('<a rel="nofollow" target="_blank" id="share-google" class="button-share google " href="//plus.google.com/share?url=' + url_share + '&text=' + encodeURIComponent(post['title']) + '"/><img alt="Share berita ' + post['title'] + ' di Google+ " src="//2.bp.blogspot.com/-EF-q1k1uR00/Vvg3S95Uz1I/AAAAAAAAAp8/7_R7ptzkaYQvZvTkVcCHfFHovETJskGaQ/s1600/G%252Bicon.png"></a>')
         res.write('<a rel="nofollow" target="_blank" id="share-twitter" class="button-share tweet " href="//twitter.com/intent/tweet?text=' + encodeURIComponent(post['title']) + '&url=' + url_share + '&via=malayatimes"><img alt="Tweet berita ' + post['title'] + ' di Twitter " src="//3.bp.blogspot.com/-ECcM-77Rj_g/Vvg3kAVesZI/AAAAAAAAAqA/QmI2oby6HSoBj8pll5MGAnQdvHdCXWq5A/s1600/twitter.icon.png"></a>')
         res.write('<a id="share-whatsapp" class="button-share whatsapp"  href="//api.whatsapp.com/send?text=' + encodeURIComponent(post['title'] + ' >> ' + url_share) + '" rel="nofollow" target="_blank"><img alt="Kongsi berita ' + post['title'] + ' di melalui Whatsapp " src="//1.bp.blogspot.com/-OUhkisqZ4kE/Vvg306mhUMI/AAAAAAAAAqE/_RYLuC-sLOUBsjtFmyqYqdeXDL2AU6TUw/s1600/whatsapp-icon.png"></a>')

         res.write('</p>')
         res.write('</aside>')





         /* ENTRY CONTENT */
         res.write('<div class="entry-content" id="entry-content">')
         var content = post['content'].replace(/\"para\"/g, '\"paragraph\"')
         res.write(content)
         res.write('</div>') // end #entry content

         res.write('</article>')

         if(data.latest_post){
            res.write('<section id="related"><div class="related_wrapper">')
            res.write('<h3>RELATED</h3>')
            res.write('<span class="button slide_left"></span>')
            for (var i = 0; i < data.latest_post.length; i++) {
               var latest = data.latest_post[i]
               var left = parseFloat(i) * parseFloat(305)
               res.write('<div class="related_post" id="related-'+i+'" style="left:'+left+'px;"><div class="related_inner"><a href="https://'+latest.domain+'/'+latest.path+'" class="related-thumb"><img src="https://media.malayatimes.com/photo/'+latest.first_img+'"></a><a href="'+latest.domain+'/'+latest.path+'" class="related-title">'+latest.title+'</a></div></div>')

            }
            var ll = parseFloat(data.latest_post.length) * parseFloat(305)
            res.write('<div class="related_post big even see_more_related" id="related-'+data.latest_post.length+'" style="left: '+ll+'px;"><div class="related_inner"><form action="https://hub.malayatimes.com/cari"><input name="keyword" type="hidden" value="'+post['title']+'" autocomplete="off" placeholder="mulakan carian.."><button type="submit">SEE MORE</button></form></div></div>')
            res.write('<span class="button slide_right"></span>')
            res.write('</div></section>')
         }

      res.write('</main>')



      /* RIGHT AREA */
      res.write('<aside id="sidebar" class="inline inline_right">')
      //adsssssss
      res.write('<div class="widget">')
         res.write('<div id="div-gpt-ad-1524121116261-0" class="premium sticker"><script>googletag.cmd.push(function() { googletag.display("div-gpt-ad-1524121116261-0") })</script></div>')
      res.write('</div>')


      //visitor online..
      res.write('<div class="widget visitor_online">')
      res.write('<h4 class="widget-heading">Live Visitor</h4>')
      res.write(' <div class="widget-content">')
      res.write('<div id="visitors"><p><span id="live-visitor-count">'+helper.number_k((post.jumlahOnline + helper.tambahanOnline()))+'</span> ONLINE</p><div class="lintangan"></div></div>')
      res.write('</div>')
      res.write('</div>')
      res.write('</aside>')

      /* CLOSE WRAPPER AREA */
      res.write('</div></div>')



      /* FOOTER AREA */
      res.write('<footer><div class="footer-content"><div class="option"><div class="width"><div class="section">   <ul class="option-list"> <li class="menu-item"><a href="https://blog.malayatimes.com/dasar-privasi">Dasar privasi</a></li></ul></div></div></div></div><aside id="noti-important" class="not-important"><script type="text/javascript" src="https://media.malayatimes.com/js/hub/single.js"></script></aside></footer>')



      res.end('</body></html>')
      res.end()

   }

}
module.exports.view =  view;