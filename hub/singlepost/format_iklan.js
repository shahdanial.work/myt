const fs = require('fs')
const helper = require('../../helper')
const db = require('../../db');


var format_iklan = function(req,res,current_url, data){

   if(data){

      /****** 1 *******/
      var slow_function = function(){

         var ok = false

         if(req.headers.cookie){
            var cookie = require('cookie');
            var str = req.headers.cookie;
            var cookies = cookie.parse(str);

            if(cookies.searched){
               ok = true
               var searched =  cookies.searched.split('_,_')
            }

         }


         if(!ok){
            return false;
         } else {

            return new Promise(resolve => {

               var query =
               "SELECT DISTINCT keyword_manager.myvalue FROM post_tbl LEFT JOIN keyword_manager ON keyword_manager.from_tbl = 'post_tbl' AND keyword_manager.from_id = post_tbl.id WHERE (";
               var string = [];
               for (var i = 0; i < searched.length; i++) {
                  if(i != 0) query += " OR "
                  query +=
                  "( MATCH(post_tbl.meta_title) AGAINST(?  IN BOOLEAN MODE) OR MATCH(post_tbl.title) AGAINST(?  IN BOOLEAN MODE) )"
                   string.push(searched[i], searched[i])
               }
               query += ") AND post_tbl.status = 'publish' AND keyword_manager.mykey = 'interest'";


               var callback = function(err, rows){
                  if(err) throw err;
                  var searched_keyword = []
                  if(rows){
                     for (var i = 0; i < rows.length; i++) {
                        if(rows[i].myvalue) searched_keyword.push(rows[i].myvalue);
                     }
                  }
                  // console.log(searched_keyword)
                  resolve(searched_keyword)
               }
               db.statement(query, string, callback)

            })

         }

      }

      /****** 2 *******/
      async function tunggu() {
        const a = await slow_function()
        // you can do stuff/logic before return
        return a;
      }

      /****** 3 *******/
      tunggu(10).then((searched_kwd) => {

         var arr_keyword = helper.build_keyword(
            {
               'mykey': data['post']['mykey'],
               'myvalue': data['post']['myvalue'],
               'searched_keyword': searched_kwd
            }
         )


         var a = '<script src="https://media.malayatimes.com/js/es5.js"></script><script async src="https://www.googletagservices.com/tag/js/gpt.js"></script><script>';
         var b = fs.readFileSync('./adsense/1-minify', 'utf8')
         var c = fs.readFileSync('./adsense/2-minify', 'utf8')
         var d = '</script>';

         data['keyword'] = a + b + arr_keyword + c + d;


         var send = require('./serve-client')
         send.view(req,res,current_url, data)

      })

   }

}
module.exports.format_iklan = format_iklan;
