const db = require('../../db');
const helper = require('../../helper');
const mysql = require('mysql');

var getPostData = function(req,res,current_url, data){

   // console.log(data)

    /* 1. CHECK path, if path available in MySQL post_tbl WHERE category is artis */

    var singlePostData = function(err1, rows) { //what ever category from post_tbl..
      var data_ada = false
        if(err1){
            throw err1;
        } else {

            data['post'] = rows[0];
            if(data){
                if(data['post']){
                    data_ada = true
                    // send to view
                    const send = require('./get_another_data')
                    send.get_another_data(req,res,current_url, data)
                    //up reputation
                    const send_1 = require('./kira_reputation')
                    send_1.kira_reputation(data)
                }
            }


        }

        if(!data_ada){
           // 404
           res.writeHead(404, {
             'Content-Type': 'text/plain'
           })
           res.write('Page ini tidak wujud')
           res.end()
        }
    }

   var myArray = [201, 203, 204, 205, 206, 207, 208, 209];
   var rand = myArray[(Math.random() * myArray.length) | 0]

   var dariJam = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(10000); // waktu2 1 jam sebelum
   // var dariHari = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(1000000); // waktu2 1 hari
   // var dariBulan = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(100000000); // waktu2 1 bulan hari
   // console.log(helper.datetime())

    var query =
    `SELECT tag_tbl.*,
    users.namapena,
    users.id_user,
    group_concat(keyword_manager.mykey) as mykey,
    group_concat(keyword_manager.myvalue) as myvalue,

    (SELECT COALESCE(Count(id), 0) + `+rand+`
                  FROM   reputation_manager
                  WHERE  time > `+dariJam+`) AS jumlahOnline

    FROM tag_tbl

    LEFT JOIN users
    ON users.id_user = tag_tbl.author_id

    LEFT JOIN keyword_manager ON keyword_manager.from_tbl = 'tag_tbl'
    AND keyword_manager.from_id =  tag_tbl.id

    WHERE type = ? AND tag_path = ?
    `;
    var string = [
                  data['my_path'][0],
                  data['my_path'][1]
                 ];
    db.statement(query, string, singlePostData)

}
module.exports.getPostData = getPostData
