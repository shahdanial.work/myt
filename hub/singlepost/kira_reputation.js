const helper = require('../../helper')
const db = require('../../db')
const mysql = require('mysql')

var kira_reputation = function (data){

   var dariJam = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(10000); // waktu2 1 jam sebelum
   var dariHari = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(1000000); // waktu2 1 hari
   var dariBulan = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(100000000); // waktu2 1 bulan hari
   // console.log(helper.datetime())

   var mesti = false;
   if(data){ if(data['post']){ if(!isNaN(parseFloat(data.post.id)) && !isNaN(parseFloat(data.post.id_user)) ) mesti = true; }}

   if(mesti){

      var callback =  function(err1, rows) {
         if(err1){
             throw err1;
         } else {

                     if(data['post']['id'] && data['post']['tag_path'] && data['ip_address'] && data['post']['id_user']){

                         // update tag post pageview.
                         db.statement('UPDATE tag_tbl SET pageview = pageview + 1 WHERE id = ?', [mysql.escape(data['post']['id'])], false);

                         var lepas_algo = rows[0].byjam < 1 && rows[0].byhari < 10 && rows[0].bybulan < 225;

                         // console.log(rows[0].byjam < 1 , rows[0].byhari < 10 , rows[0].bybulan < 225);

                         ///////////////////////////// checkHour ///////////////////////////
                         if( lepas_algo ){
                             /* MELEPASI ALGOTHRM */
                             ////////////////////// SEMUA LEPAS START ///////////////
                             // insert new reputation manager..
                             db.statement(
                                 //query
                                 'INSERT INTO reputation_manager (ip, time, author_id, from_tbl, from_id, reputation, type) VALUES (?, ?, ?, ?, ?, ?, ?)'
                                 ,
                                 //string
                                 [
                                    data['ip_address'],
                                    helper.datetime().replace(/ /g, ''),
                                    data['post']['id_user'],
                                    'tag_tbl',
                                    mysql.escape(data['post']['id']),
                                    '15',/* sblm ni 1 shj untuk pv, tp xnak pakai share (10), so combine */
                                    'view'
                                 ]
                                 ,
                                 false
                             );
                             ////////////////////// SEMUA LEPAS END ///////////////
                         }
                         ///////////////////////////// checkHour ///////////////////////////

                      }


         }
      }


      var query =
      `SELECT
      (SELECT COALESCE(Count(id), 0)
                    FROM   reputation_manager
                    WHERE  time > `+dariJam+`
                    AND    from_tbl = 'tag_tbl'
                    AND    from_id = ?
                    AND    ip = ?) AS byjam,

      (SELECT COALESCE(Count(id), 0)
                    FROM   reputation_manager
                    WHERE  time > `+dariHari+`
                    AND    author_id = ?
                    AND    ip = ?) AS byhari,

      (SELECT COALESCE(Count(id), 0)
                    FROM   reputation_manager
                    WHERE  time > `+dariBulan+`
                    AND    author_id = ?
                    AND    ip = ?) AS bybulan
      `;

      var string = [
         parseFloat(data.post.id),
         data['ip_address'],
         parseFloat(data.post.id_user),
         data['ip_address'],
         parseFloat(data.post.id_user),
         data['ip_address']
      ];

      db.statement(query, string, callback)

   }
}
module.exports.kira_reputation = kira_reputation