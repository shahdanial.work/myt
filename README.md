# Add my Skype

1. There's fews private configuration you need to be done
2. contact me on Skype (I've send my Skype to your email)
      
  
## 1. Install `git` in your local machine

1. install git by run this command `sudo apt-get install git-core`


## 2. Clone this project from your local computer

1. run command `sudo -s` then enter your password

2. run command `cd var/www`

3. run command `git clone https://gitlab.com/shahdanial.work/myt.git`.
it will request your gitLab login details, (firstly, enter your gitLab `username` and enter, then enter your gitLab `password`

4. run command `mv myt nodejs-project`
      

## 3. Make sure the `Node.js` version 8/++ installed in your machine
    
1. check wheter node.js version 8 or above installed in your machine or not by run this command `node -v`

2. If not showing version as of node.js as output, install node.js
    
    
## 4. Make sure the `NPM` version 6/++ installed in yout machine

1. check wheater NPM version 6 or above installed in your machine or not by run this command `NPM -v`
      
2. If not showing version as of node.js as output, install NPM
    
## 5. Install the `pm2` for you node.js

1. It make you easy while in development progress. Also learn it give you advantages since it's used at production (at server)
      
2. Done insall it? Okay, go to next step, we well use `pm2` later when need to run node.js in your machine.
    
    
## 6. Install the `mySQL` in your local machine

1. Install mySQL, search on Google `how install mySQL in Ubuntu 16.04 LTS` if you use Ubuntu 16.04 LTS
      
2. After step above, contact me on Skype, You need to setup it with me.
    
## 7. install the `NGINX` balancer in your machine

1. Install NGINX, search on Google `how install NGINX in Ubuntu 16.04 LTS` if you use Ubuntu 16.04 LTS
      
2. After step above, contact me on Skype, You need to setup it with me.
    
## 8. Setup Fake Domain for your machine

1. For Ubuntu user, run this command `cat /etc/hosts`
      
2. Copy the outputs after run command above, then send to my Skype, I'll assist you.