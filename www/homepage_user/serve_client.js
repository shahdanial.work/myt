const fs = require('fs')
var serve_client = function(req, res, current_url, data){

    res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'});
    res.write('<!DOCTYPE html>\n')
    res.write('<html class="main-blog">')
    res.write(data['head_open'])
    res.write('<title>'+ req['url'].replace('/','') +' Channel</title><meta content="'+req['url'].replace('/','')+' Profile channel di Malayatimes. Perkongisan media terkini, berita semasa, gambar, video dan kisah viral dari '+req['url'].replace('/','')+'" name="description"/><meta content="//www.malayatimes.com/'+req['url'].replace('/','')+'" property="og:url"/>')
    // res.write('<style type="text/css">'+data['css']+'</style>')
    res.write('<link rel="stylesheet" type="text/css" href="//media.malayatimes.com/css/mainpage-user.css"/>')
    res.write(fs.readFileSync('./adsense/homepage-cat-atas', 'utf8'))

    if(data['ads_keyword_gender']){
      res.write('googletag.pubads().setTargeting("gender",['+data['ads_keyword_gender']+']),')
    } else {
      res.write('googletag.pubads().setTargeting("gender",["male","female"]),')
    }
    if(data['ads_keyword_interest']){
      res.write('googletag.pubads().setTargeting("interest",['+data['ads_keyword_interest']+']),')
    }

    res.write(fs.readFileSync('./adsense/homepage-cat-bawah', 'utf8'))

    res.write('</script>')

    res.write(data['head_close'])
    res.write('<body id="main-blog">') // because it's a main website. this is main thing including the header !
    res.write(data['menu_nav'])
    res.write(data['header'])
    res.write('<div class="hfeed section" id="blog-content"><div class="widget Blog" data-version="1" id="Blog1">')
    //////////// DYNAMIC START /////////////

  var article_id = 0;

      res.write('<div class="wrapper rows">')
           /* SECTION POPULAR */
           // res.write(data['popular_box'])
           /* POPULAR START */
           if(data['popular_feed']){
               var popular = data['popular_feed'];
               res.write('<aside class="popular-articles column_1"> <h3>Popular</h3> ')
                for (let i = 0; i < popular.length; i++) {
                    var artikel = popular[i];
                    res.write('<div class="popular-article first"><div class="inner-wrapper"><a class="img" href="//'+artikel.domain+'/'+artikel['path']+'" style="background: url(//media.malayatimes.com/foto/'+artikel.first_img+') no-repeat;"><img alt="'+artikel.title.replace(/"/g,'')+' " src="//media.malayatimes.com/gambar/'+artikel.first_img+'"></a><div class="caption"><h2><a class="title" href="//'+artikel.domain+'/'+artikel['path']+'">'+artikel.title+'</a></h2>')
                       if(artikel.tags){
                           res.write('<a class="tag" href="//hub.malayatimes.com/'+artikel.tags[0].tag_type+'/'+artikel.tags[0].tag_path+'">'+artikel.tags[0].tag_title+'</a>')
                       }
                   //
                    res.write('</div></div></div> ')
                }
                if(popular.length > 1){
                   res.write(' <button class="slide_left">Back</button> <button class="slide_right">Next</button> ')
                   res.write('<div class="slidecount">')
                   for (let i = 0; i < popular.length; i++) {
                       if(i == 0){
                        res.write('<span id="count'+i+'" class="active"></span>')
                       } else {
                        res.write('<span id="count'+i+'"></span>')
                       }
                   }
                   res.write('</div>')
                }
               res.write('</aside>')
           } else {
               res.write(data['popular_box'])
           }
           /* POPULAR END */

           article_id = 10;
           column = 0;
           /* SECTION LATEST */


           if(data['latest_post']){

             var latest_post = data['latest_post'];

             if(latest_post[0]){
                  res.write('<div class="recent-articles column_2 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-11"><a class="img" href="//'+latest_post[0]['domain']+'/'+latest_post[0]['path']+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[0].first_img+') no-repeat"><img alt=" '+latest_post[0].title+' " src="//media.malayatimes.com/photo/'+latest_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[0]['domain']+'/'+latest_post[0]['path']+'">'+latest_post[0].title+'</a></h3>')

                  var masa_utc = new Date(latest_post[0]['masa']).toISOString()
                  res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                  if(latest_post[0].tags[0]){
                      res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[0].tags[0].tag_type+'/'+latest_post[0].tags[0].tag_path+'">'+latest_post[0].tags[0].tag_title+'</a>')
                  }
                  res.write('</div></article> ')
                  res.write('</div></div>')
             }

             /////////////////// START IKLAN //////////////////
             res.write('<aside class="iklan column_3 odd big"> <div class="content_group_inner_wrapper"><div class="content-item"><div class="premium" id="gpt-tgh"><script>googletag.cmd.push(function() { googletag.display("gpt-tgh"); googletag.pubads().refresh([gptAdSlots[1]]); });</script></div></div></div></aside>')
             ///////////////////  END IKLAN  //////////////////

             /* /div LATEST */

             res.write('</div><!-- end class="wrapper rows"-->') // dalamnya popular dan recent atas

             /* SECTION LATEST BAWAH */
             if(latest_post[1]){
                  res.write('<div class="wrapper rows">')

                  /* 1 */
                  res.write('<div class="recent-articles column_4 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-13"><a class="img" href="//'+latest_post[1]['domain']+'/'+latest_post[1]['path']+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[1].first_img+') no-repeat"><img alt=" '+latest_post[1].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[1].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[1]['domain']+'/'+latest_post[1]['path']+'">'+latest_post[1].title+'</a></h3>')

                  var masa_utc = new Date(latest_post[1]['masa']).toISOString()
                  res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                  if(latest_post[1].tags[0]){
                      res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[1].tags[0].tag_type+'/'+latest_post[1].tags[0].tag_path+'">'+latest_post[1].tags[0].tag_title+'</a>')
                  }
                  res.write('</div></article> ')
                  res.write('</div></div>')



                  /*2 to 4 (1 group posts box)*/
                  if(latest_post[2]){
                  res.write('<div class="recent-articles odd">')

                      for (var i = 2; i < 5; i++) {

                          ///////////////////////

                          if(latest_post[i]){
                             res.write('<article class="hentry recent-article"> <a class="img" href="//'+latest_post[i]['domain']+'/'+latest_post[i]['path']+'"> <img alt="'+latest_post[i].title.replace(/"/g,'')+'" src="//media.malayatimes.com/picture/'+latest_post[i].first_img+'"> </a> <h2 class="entry-title"><a class="title" href="//'+latest_post[i]['domain']+'/'+latest_post[i]['path']+'">'+latest_post[i].title+'</a></h2>')

                             var masa_utc = new Date(latest_post[i]['masa']).toISOString()
                             res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                             if(latest_post[i].tags[0]){
                                res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[i].tags[0].tag_type+'/'+latest_post[i].tags[0].tag_path+'">'+latest_post[i].tags[0].tag_title+'</a>')
                             }
                             res.write(' </article>')
                          }

                          ///////////////////////

                      }

                  res.write('</div><!-- end class="recent-articles column_5 odd"-->')
                  }


                  /* 5 */
                  if(latest_post[5]){
                     res.write('<div class="recent-articles column_4 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item"><a class="img" href="//'+latest_post[5]['domain']+'/'+latest_post[5]['path']+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[5].first_img+') no-repeat"><img alt=" '+latest_post[5].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[5].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[5]['domain']+'/'+latest_post[5]['path']+'">'+latest_post[5].title+'</a></h3>')

                     var masa_utc = new Date(latest_post[5]['masa']).toISOString()
                     res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                     if(latest_post[5].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[5].tags[0].tag_type+'/'+latest_post[5].tags[0].tag_path+'">'+latest_post[5].tags[0].tag_title+'</a>')
                     }
                     res.write('</div></article> ')
                     res.write('</div></div>')
                  }



                  /*6 to 8 (1 group posts box)*/
                  if(latest_post[6]){
                  res.write('<div class="recent-articles odd">')


                      var even_odd = 'odd';
                      for (let i = 6; i < 9; i++) {
                          const post_latest = latest_post[i];

                          ///////////////////////

                          if(post_latest){
                             res.write('<article class="hentry recent-article"> <a class="img" href="//'+post_latest['domain']+'/'+post_latest['path']+'"> <img alt="'+post_latest.title.replace(/"/g,'')+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> <h2 class="entry-title"><a class="title" href="//'+post_latest['domain']+'/'+post_latest['path']+'">'+post_latest.title+'</a></h2>')

                             var masa_utc = new Date(post_latest['masa']).toISOString()
                             res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                             if(post_latest.tags[0]){
                                res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_type+'/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                             }
                             res.write(' </article>')
                          }

                          ///////////////////////

                      }

                  res.write('</div><!-- end class="recent-articles column_5 odd"-->')
                  }


                  res.write('</div><!-- end class="wrapper rows"-->')
             }
             /* /div LATEST BAWAH */



             /* SECTION LATEST LAST */


             if(latest_post[9]){
                  res.write('<div class="wrapper rows">')

                  res.write('<div class="recent-articles column_7 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-16"><a class="img" href="//'+latest_post[9]['domain']+'/'+latest_post[9]['path']+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[9].first_img+') no-repeat"><img alt=" '+latest_post[9].title.replace(/"/g,'')+' " src="//media.malayatimes.com/photo/'+latest_post[9].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[9]['domain']+'/'+latest_post[9]['path']+'">'+latest_post[9].title+'</a></h3>')

                  var masa_utc = new Date(latest_post[9]['masa']).toISOString()
                  res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                  if(latest_post[9].tags){
                     if(latest_post[9].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[9].tags[0].tag_type+'/'+latest_post[9].tags[0].tag_path+'">'+latest_post[9].tags[0].tag_title+'</a>')
                     }
                  }
                  res.write('</div></article> ')
                  res.write('</div></div>')


                  //10 to 15
                  if(latest_post[10]){
                      res.write('<div class="recent-articles column_8 odd">')


                          var even_odd = 'odd';
                          var ganda = 0;
                          for (var i = 10; i < 16; i++) {
                              var post_latest = latest_post[i];
                              ganda += 1;

                              ///////////////////////
                              res.write('<article class="hentry recent-article">')
                              res.write(' <a class="img" href="//'+post_latest['domain']+'/'+post_latest['path']+'"> <img alt="'+post_latest.title.replace(/"/g,'')+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> ')
                              res.write('<h2 class="entry-title"><a class="title" href="//'+post_latest['domain']+'/'+post_latest['path']+'">'+post_latest.title+'</a></h2>')

                              var masa_utc = new Date(post_latest['masa']).toISOString()
                             res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                              if(post_latest.tags[0]){
                                  res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_type+'/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                              }
                              res.write(' </article>')

                              if(ganda == 3 && i != 15){
                                  ganda = 0;
                                  if(even_odd == 'even'){
                                      even_odd = 'odd';
                                  } else {
                                      even_odd = 'even';
                                  }
                                  res.write('</div><div class="recent-articles '+even_odd+'">')
                              }
                              ///////////////////////

                          }

                      res.write('</div><!-- end class="recent-articles column_7 odd"-->')
                      }


                      /////////////////// START IKLAN //////////////////
                      res.write('<aside class="iklan column_3 odd big"><div class="content_group_inner_wrapper"><div class="content-item"><div class="premium" id="gpt-tgh-1"><script>googletag.cmd.push(function() { googletag.display("gpt-tgh-1"); googletag.pubads().refresh([gptAdSlots[2]]); });</script></div></div></div></aside>')
                      ///////////////////  END IKLAN  //////////////////



                  res.write('</div><!-- end class="wrapper rows"-->')
             }

             /* /div LATEST LAST */


             res.write('<aside id="load-more" class="js-auto-load-content"><span class="loader"><span class="pusing"></span></span></aside>')

           }


  //////////// DYNAMIC END ///////////////
    res.write('</div></div><!--class="widget Blog" data-version="1" id="Blog1"-->')
//     res.write(`<footer id="blog-footer">
//        <p class="home-page"><a href=" //www.malayatimes.com/">Berita Semasa Malaysia</a></p>
//        <aside class="not-important">
//           <div id="login-and-search" class="search_and_social">
//              <div class="section_wrapper">
//                 <h5 class="heading"><a href="//www.malayatimes.com" title="Berita Semasa Malaysia"><img alt="Berita Semasa Malaysia" src="//media.malayatimes.com/assets/image/malayatimes.png"></a></h5>
//                 <div class="search_box">
//                    <div class="inline_block">
//                       <div id="search_box" class="es_input" contenteditable="true">
//                       </div><span id="search-button" class="cari"></span></div>
//                 </div>
//
//                 <div id="search-result">
//                       <div id="stanby" style="dislay:none;">
//                          <span class="loader">
//                             <span class="pusing"></span>
//                          </span>
//                       </div>
//                       <span id="close-search-result" onclick="clear_search()">clear</span>
//                 </div>
//
//                 <div class="or"></div>
//                 <span class="text_like">like &amp; follow us</span>
//
//                 <div id="fans"></div>
//
//                 <button id="close-login-search" class="close_section">×</button>
//              </div>
//           </div>
//              <script type="text/javascript" src="//media.malayatimes.com/js/main/single.js"></script>
//        </aside>
//     </footer>
// `)

res.write(`<footer id="blog-footer"> <p class="home-page"><a href=" //www.malayatimes.com/">Berita Semasa Malaysia</a></p><aside class="not-important"> <div id="login-and-search" class="search_and_social"> <div class="section_wrapper"> <h5 class="heading"><a href="//www.malayatimes.com" title="Berita Semasa Malaysia"><img alt="Berita Semasa Malaysia" src="//media.malayatimes.com/assets/image/malayatimes.png"></a></h5> <div class="search_box"> <div class="inline_block"> <form action="//hub.malayatimes.com/cari" id="search-form"><input id="search_box" class="es_input" name="keyword" required> <button type="submit" id="search-button" class="cari"></button></form></div></div><div class="or"></div><span class="text_like">like &amp; follow us</span> <div id="fans"></div><button id="close-login-search" class="close_section">×</button> </div></div><script type="text/javascript" src="//media.malayatimes.com/js/main/single.js"></script> </aside> </footer>`)
res.write('</body></html>')
    res.write('</body></html>')
    res.end();

} // end create server


module.exports.serve_client = serve_client;
