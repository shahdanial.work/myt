const db = require('../../db')

var check_user = function(req, res, current_url, data){

   var byUserName = function(err, db_data) {

      if(err) {
         res.writeHead(404, {
            'Content-Type': 'text/plain'
         })
         res.write('Page ini tidak wujud')
         res.end()
         throw err
      } else if(db_data) {

         var passed = false;
         if(db_data[0]){
            if(typeof db_data[0].id_user == 'number'){
               passed = true;
               const next = require('./homepage_data')
               next.homepage_data(req, res, current_url, data)
            }
         }

         if(!passed) {
            res.writeHead(404, {
               'Content-Type': 'text/plain'
            });
            res.write('Page ini tidak wujud');
            res.end();
         }

      }

   }
   // console.log(data['username']);
   var query = "SELECT id_user FROM users WHERE namapena = ?";
   var string = [data['username']];
   db.statement(query, string, byUserName);

}
module.exports.check_user = check_user;