const db = require('../../db');
const helper = require('../../helper');
const send = require('./serve_client');
/*
SET UP DATA THAT USING MySQL query.. if you have another data from db nned to use. add more STEP
*/



var by_category = function(req, res, current_url, data, id_tak_mau){

    /* STEP 2 */
    var artis_feed = function(err2,rows2){
        if(err2){
            console.log('error artis_feed sql');
            console.log(err2);
            throw err2;
        } else {
            data['artis_feed'] = rows2;
            for (let i = 0; i < data['artis_feed'].length; i++) {
                if(data['artis_feed'][i]['tag_path'] && data['artis_feed'][i]['tag_title'] && data['artis_feed'][i]['tag_type']){
                    data['artis_feed'][i]['tags'] = helper.build_tag(data['artis_feed'][i]['tag_type'], data['artis_feed'][i]['tag_path'], data['artis_feed'][i]['tag_title']);
                } else {
                    data['artis_feed'][i]['tags'] = new Array();
                }
                if(data['artis_feed'][i]['id']){
                    id_tak_mau = id_tak_mau+','+data['artis_feed'][i]['id'];
                }
            }

            /* STEP 3 */
            var sukan_feed = function(err3,rows3){
                if(err3){
                    console.log('error sukan_feed sql');
                    console.log(err3);
                } else {
                    data['sukan_feed'] = rows3;
                    for (let i = 0; i < data['sukan_feed'].length; i++) {
                        if(data['sukan_feed'][i]['tag_path'] && data['sukan_feed'][i]['tag_title'] && data['sukan_feed'][i]['tag_type']){
                            data['sukan_feed'][i]['tags'] = helper.build_tag(data['sukan_feed'][i]['tag_type'], data['sukan_feed'][i]['tag_path'], data['sukan_feed'][i]['tag_title']);
                        } else {
                            data['sukan_feed'][i]['tags'] = new Array();
                        }
                        if(data['sukan_feed'][i]['id']){
                            id_tak_mau = id_tak_mau+','+data['sukan_feed'][i]['id'];
                        }
                    }

                    /* STEP 4 */
                    var berita_feed = function(err4,rows4){
                        if(err4){
                            console.log('error berita_feed sql');
                            console.log(err4);
                        } else {
                            data['berita_feed'] = rows4;
                            for (let i = 0; i < data['berita_feed'].length; i++) {
                                if(data['berita_feed'][i]['tag_path'] && data['berita_feed'][i]['tag_title'] && data['berita_feed'][i]['tag_type']){
                                    data['berita_feed'][i]['tags'] = helper.build_tag(data['berita_feed'][i]['tag_type'], data['berita_feed'][i]['tag_path'], data['berita_feed'][i]['tag_title']);
                                } else {
                                    data['berita_feed'][i]['tags'] = new Array();
                                }
                                if(data['berita_feed'][i]['id']){
                                    id_tak_mau = id_tak_mau+','+data['berita_feed'][i]['id'];
                                }
                            }

                            /* STEP 5 */
                            var viral_feed = function(err5, rows5){
                                if(err5){
                                    console.log('error viral sql');
                                    console.log(err5);
                                } else {
                                    data['viral_feed'] = rows5;
                                    for (let i = 0; i < data['viral_feed'].length; i++) {
                                        if(data['viral_feed'][i]['tag_path'] && data['viral_feed'][i]['tag_title'] && data['viral_feed'][i]['tag_type']){
                                            data['viral_feed'][i]['tags'] = helper.build_tag(data['viral_feed'][i]['tag_type'], data['viral_feed'][i]['tag_path'], data['viral_feed'][i]['tag_title']);
                                        } else {
                                            data['viral_feed'][i]['tags'] = new Array();
                                        }
                                        if(data['viral_feed'][i]['id']){
                                            id_tak_mau = id_tak_mau+','+data['viral_feed'][i]['id'];
                                        }
                                    }
                                    /* STEP 6 */
                                    var popular_feed = function(err6, rows6){
                                        if(err6){
                                            console.log('error viral sql');
                                            console.log(err6);
                                        } else {
                                            if(rows6){
                                                data['popular_feed'] = rows6;
                                                for (let i = 0; i < data['popular_feed'].length; i++) {
                                                    if(data['popular_feed'][i]['domain']){
                                                        data['popular_feed'][i]['category'] = data['popular_feed'][i]['domain'].replace('.malayatimes.com','');
                                                    } else {
                                                        data['popular_feed'][i]['category'] = false;
                                                    }
                                                    if(data['popular_feed'][i]['id']){
                                                        id_tak_mau = id_tak_mau+','+data['popular_feed'][i]['id'];
                                                    }
                                                }
                                            }

                                            /* FINALLY */
                                            send.serve_client(req,res,current_url,data, id_tak_mau);

                                        }

                                    }
                                    // console.log('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
                                    // console.log(helper.datetime().replace(/ /g, ''))
                                    var dariBulan = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(100000000); // waktu2 1 bulan
                                    // console.log(dariBulan)
                                    // console.log('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
                                    db.statement(`
                                       (SELECT reputation_manager.from_id as id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path AS path, category_manager.domain AS domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.time > `+dariBulan+` AND reputation_manager.from_tbl = 'post_tbl' AND post_tbl.status = 'publish' AND  reputation_manager.from_id NOT IN(`+id_tak_mau+`) AND category_manager.domain != 'blog.malayatimes.com' GROUP BY reputation_manager.from_id ORDER BY score DESC) UNION DISTINCT
                                       (SELECT reputation_manager.from_id as id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path AS path, category_manager.domain AS domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.from_tbl = 'post_tbl' AND post_tbl.status = 'publish' AND  reputation_manager.from_id NOT IN(`+id_tak_mau+`) AND category_manager.domain != 'blog.malayatimes.com' GROUP BY reputation_manager.from_id ORDER BY score DESC) LIMIT 0,10`, false, popular_feed);

                                }

                            }
                            db.statement("SELECT post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path WHERE post_tbl.status = 'publish' AND category_manager.domain = 'viral.malayatimes.com' AND post_tbl.id NOT IN ("+id_tak_mau+") GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0,8", false, viral_feed);
                        }
                    }
                    db.statement("SELECT post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path WHERE post_tbl.status = 'publish' AND category_manager.domain = 'berita.malayatimes.com' AND post_tbl.id NOT IN ("+id_tak_mau+") GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0,8", false, berita_feed);
                }
            }
            db.statement("SELECT post_tbl.*, category_manager.domain as category, users.namapena, group_concat (tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path WHERE post_tbl.status = 'publish' AND category_manager.domain = 'sukan.malayatimes.com' AND post_tbl.id NOT IN ("+id_tak_mau+") GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0,8", false, sukan_feed);
        }
    }
    db.statement("SELECT post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path WHERE post_tbl.status = 'publish' AND category_manager.domain = 'artis.malayatimes.com' AND post_tbl.id NOT IN ("+id_tak_mau+") GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0,8", false, artis_feed);

}
module.exports.by_category = by_category;

/* FINALLY
send.serve_client(req,res,current_url,data);
*/