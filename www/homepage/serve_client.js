var serve_client = function(req, res, current_url, data, id_tak_mau){

    res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'});
    res.write('<!DOCTYPE html>\n')
    res.write('<html class="main-blog">')
    res.write(data['head_open'])
    res.write('<title>Berita Semasa Malaysia &#8212; Malaya Times</title><meta content="Berita semasa Malaysia terkini dan terbaru. Blog berita dan info semasa Malaysia yang terbaik terkini dan top di Malaysia." name="description"/><meta content="//www.malayatimes.com/" property="og:url"/>')
    res.write('<script src="https://media.malayatimes.com/js/es5.js"></script>')
    res.write('<style type="text/css">'+data['css']+'</style>')
    res.write(data['head_close'])
    res.write('<body id="main-blog">') // because it's a main website. this is main thing including the header !
    res.write('<input type="hidden" id="posts-id" value="'+id_tak_mau+'">')
    res.write(data['menu_nav'])
    res.write(data['header'])
    res.write('<div class="hfeed section" id="blog-content"><div class="widget Blog" data-version="1" id="Blog1">')
    //////////// DYNAMIC START /////////////

    var article_id = 0;

        res.write('<section class="wrapper authority_1">')
            /* SECTION POPULAR */
            //res.write(data['popular_box'])

            /* POPULAR START */
            if(data['popular_feed']){
                var popular = data['popular_feed'];
                res.write('<aside class="popular-articles column_1"> <h3>Popular</h3> ')
                 for (let i = 0; i < popular.length; i++) {
                     var artikel = popular[i];
                     res.write('<article class="hentry popular-article first"><div class="inner-wrapper"><a class="img" href="//'+artikel.domain+'/'+artikel.path+'" style="background: url(//media.malayatimes.com/foto/'+artikel.first_img+') no-repeat;"><img alt="'+artikel.title+' " src="//media.malayatimes.com/gambar/'+artikel.first_img+'"></a><div class="caption"><h2><a class="title" href="//'+artikel.domain+'/'+artikel.path+'">'+artikel.title+'</a></h2>')
                     res.write('<a class="tag" href="//'+artikel.domain+'">'+artikel.category+'</a>')
                     res.write('</div></div></article> ')
                 }
                 if(popular.length > 1){
                    res.write(' <button class="slide_left">Back</button> <button class="slide_right">Next</button> ')
                    res.write('<div class="slidecount">')
                    for (let i = 0; i < popular.length; i++) {
                        if(i == 0){
                         res.write('<span id="count'+i+'" class="active"></span>')
                        } else {
                         res.write('<span id="count'+i+'"></span>')
                        }
                    }
                    res.write('</div>')
                 }
                res.write('</aside>')
            } else {
                res.write(data['popular_box'])
            }
            /* POPULAR END */

            article_id = 10;
            column = 0;
            /* SECTION LATEST */
            var latest_post = data['latest_post'];

            if(latest_post[0]){
               // console.log('\n\n##########\n\n', latest_post[0] ,'\n\n##########\n\n')
                var masa_utc = new Date(latest_post[0]['masa']).toISOString()

                res.write('<div class="recent-articles column_2 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-11"><a class="img" href="//'+latest_post[0].category+'/'+latest_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[0].first_img+') no-repeat"><img alt=" '+latest_post[0].title+' " src="//media.malayatimes.com/photo/'+latest_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[0].category+'/'+latest_post[0].path+'">'+latest_post[0].title+'</a></h3>')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[0].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[0].tags[0].tag_path+'">'+latest_post[0].tags[0].tag_title+'</a>')
                }

                res.write('</div></article> </div></div>')
            }

            if(latest_post[1]){

               var masa_utc = new Date(latest_post[1]['masa']).toISOString()

                res.write('<div class="recent-articles column_3 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-12"><a class="img" href="//'+latest_post[1].category+'/'+latest_post[1].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[1].first_img+') no-repeat"><img alt=" '+latest_post[1].title+' " src="//media.malayatimes.com/photo/'+latest_post[1].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[1].category+'/'+latest_post[1].path+'">'+latest_post[1].title+'</a></h3>')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[1].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[1].tags[0].tag_path+'">'+latest_post[1].tags[0].tag_title+'</a></div></article> ')
                }
                res.write('</div></div>')
            }
            /* /SECTION LATEST */

            res.write('</section><!-- end class="wrapper authority_1"-->') // dalamnya popular dan recent atas

            /* SECTION LATEST BAWAH */
            if(latest_post[2]){

               var masa_utc = new Date(latest_post[2]['masa']).toISOString()

                res.write('<section class="wrapper authority_2">')

                res.write('<div class="recent-articles column_4 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-13"><a class="img" href="//'+latest_post[2].category+'/'+latest_post[2].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[2].first_img+') no-repeat"><img alt=" '+latest_post[2].title+' " src="//media.malayatimes.com/photo/'+latest_post[2].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[2].category+'/'+latest_post[2].path+'">'+latest_post[2].title+'</a></h3>')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[2].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[2].tags[0].tag_path+'">'+latest_post[2].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')

                /*3 to 8*/
                if(latest_post[3]){
                res.write('<div class="recent-articles column_5 odd">')


                    var even_odd = 'odd';
                    var column = 5;
                    var ganda = 0;
                    var article_id = 13;
                    for (let i = 3; i < latest_post.length; i++) {
                        const post_latest = latest_post[i];
                        article_id +=1;
                        ganda += 1;

                        ///////////////////////
                        if(i < 9){

                           var masa_utc = new Date(latest_post[i]['masa']).toISOString()

                            res.write('<article class="hentry recent-article" id="article-'+article_id+'"> <a class="img" href="//'+post_latest.category+'/'+post_latest.path+'"> <img alt="'+post_latest.title+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> <h2 class="entry-title"><a class="title" href="//'+post_latest.category+'/'+post_latest.path+'">'+post_latest.title+'</a></h2>')

                            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                            if(post_latest.tags[0]){
                                res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                            }

                            res.write(' </article>')
                        }
                        ///////////////////////

                        if(ganda == 3 && i != 8){
                            column += 1;
                            ganda = 0;
                            if(even_odd == 'even'){
                                even_odd = 'odd';
                            } else {
                                even_odd = 'even';
                            }
                            res.write('</div><div class="recent-articles column_'+column+' '+even_odd+'">')
                        }

                    }

                res.write('</div><!-- end class="recent-articles column_5 odd"-->')
                }


                // post 9
                if(latest_post[9]){

                   var masa_utc = new Date(latest_post[9]['masa']).toISOString()

                    res.write('<div class="recent-articles column_6 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-12"><a class="img" href="//'+latest_post[9].category+'/'+latest_post[9].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[9].first_img+') no-repeat"><img alt=" '+latest_post[9].title+' " src="//media.malayatimes.com/photo/'+latest_post[9].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[9].category+'/'+latest_post[9].path+'">'+latest_post[9].title+'</a></h3>')

                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(latest_post[9].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[9].tags[0].tag_path+'">'+latest_post[9].tags[0].tag_title+'</a>')
                    }
                    res.write('</div></article> </div></div>')
                }


                res.write('</section><!-- end class="wrapper authority_2"-->')
            }
            /* /SECTION LATEST BAWAH */


        /* SECTION ARTIS FEED */
        if(data['artis_feed'][0]){
        const artis_post = data['artis_feed'];
        res.write('<section class="section_wrapper artis">')

            res.write('<h2><a href="//artis.malayatimes.com" title="Gosip Artis Malaysia">Gosip artis</a></h2>')

            res.write('<div class="content latest">')

            //artis big
            if(artis_post[0]){
                article_id += 1;

                var masa_utc = new Date(artis_post[0]['masa']).toISOString()

                res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+artis_post[0].category+'/'+artis_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+artis_post[0].first_img+') no-repeat"><img alt=" '+artis_post[0].title+' " src="//media.malayatimes.com/photo/'+artis_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" class="entry-title" href="//'+artis_post[0].category+'/'+artis_post[0].path+'">'+artis_post[0].title+'</a></h3> ')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(artis_post[0]['tags'][0]){
                    res.write('<a class="tag" href="//hub.malayatimes.com/'+artis_post[0]['tags'][0].tag_path+'">'+artis_post[0]['tags'][0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')
            }

            //artis small group
            if(artis_post[1]){

                ganda = 0;

                res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                for (let i = 1; i < artis_post.length; i++) {
                    ganda +=1;
                    if(ganda < 4){
                        article_id += 1;
                        const post_artis = artis_post[i];

                        var masa_utc = new Date(post_artis['masa']).toISOString()

                        res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com/picture/'+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

                        res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                        if(post_artis['tags'][0]){
                            res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
                        }
                        res.write('</div></article>')
                    }

                }
                res.write('</div></div>')
            }

            if(artis_post[4]){
                article_id += 1;

                var masa_utc = new Date(artis_post[4]['masa']).toISOString()

                res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+artis_post[4].category+'/'+artis_post[4].path+'" style="background:url(//media.malayatimes.com/photo/'+artis_post[4].first_img+') no-repeat"><img alt=" '+artis_post[4].title+' " src="//media.malayatimes.com/photo/'+artis_post[4].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+artis_post[4].category+'/'+artis_post[4].path+'">'+artis_post[4].title+'</a></h3> ')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(artis_post[4]['tags'][0]){
                    res.write('<a class="tag" href="//hub.malayatimes.com/'+artis_post[4]['tags'][0].tag_path+'">'+artis_post[4]['tags'][0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')
            }

            if(artis_post[5]){
                ganda = 0;
                res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                for (let i = 5; i < artis_post.length; i++) {
                    ganda +=1;
                    if(ganda < 4){
                        article_id += 1;
                        const post_artis = artis_post[i];

                        var masa_utc = new Date(post_artis['masa']).toISOString()

                        res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com/picture/'+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

                        res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                        if(post_artis['tags'][0]){
                            res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
                        }
                        res.write('</div></article>')
                    }

                }
                res.write('</div></div>')
            }

            res.write('</div><!--/class="content latest"-->')

        res.write('</section>')
        }
        /* /SECTION ARTIS FEED */

        /* SECTION SUKAN FEED */
        if(data['sukan_feed'][0]){
        const sukan_post = data['sukan_feed'];
        res.write('<section class="section_wrapper sukan">')

        res.write('<h2><a href="//sukan.malayatimes.com" title="Berita sukan Malaysia">Berita sukan</a></h2>')

        res.write('<div class="content latest">')
        //sukan big
        if(sukan_post[0]){
            article_id += 1;
            res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+sukan_post[0].category+'/'+sukan_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+sukan_post[0].first_img+') no-repeat"><img alt=" '+sukan_post[0].title+' " src="//media.malayatimes.com/photo/'+sukan_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+sukan_post[0].category+'/'+sukan_post[0].path+'">'+sukan_post[0].title+'</a></h3> ')

            var masa_utc = new Date(sukan_post[0]['masa']).toISOString()
            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

            if(sukan_post[0]['tags'][0]){
                res.write('<a class="tag" href="//hub.malayatimes.com/'+sukan_post[0]['tags'][0].tag_path+'">'+sukan_post[0]['tags'][0].tag_title+'</a>')
            }
            res.write('</div></article> </div></div>')
        }

        //sukan small group
        if(sukan_post[1]){
            ganda = 0;
            res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
            for (let i = 1; i < sukan_post.length; i++) {
                ganda +=1;
                if(ganda < 4){
                    article_id += 1;
                    const post_sukan = sukan_post[i];
                    res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_sukan.category+'/'+post_sukan.path+'"><img alt=" '+post_sukan.title+' " src="//media.malayatimes.com/picture/'+post_sukan.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_sukan.category+'/'+post_sukan.path+'">'+post_sukan.title+'</a></h3>')

                    var masa_utc = new Date(sukan_post[1]['masa']).toISOString()
                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(post_sukan['tags'][0]){
                        res.write('<a class="tag" href="//hub.malayatimes.com/'+post_sukan['tags'][0].tag_path+'">'+post_sukan['tags'][0].tag_title+'</a>')
                    }
                    res.write('</div></article>')
                }

            }
            res.write('</div></div>')
        }

        if(sukan_post[4]){
            article_id += 1;
            res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+sukan_post[4].category+'/'+sukan_post[4].path+'" style="background:url(//media.malayatimes.com/photo/'+sukan_post[4].first_img+') no-repeat"><img alt=" '+sukan_post[4].title+' " src="//media.malayatimes.com/photo/'+sukan_post[4].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+sukan_post[4].category+'/'+sukan_post[4].path+'">'+sukan_post[4].title+'</a></h3> ')

            var masa_utc = new Date(sukan_post[4]['masa']).toISOString()
            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

            if(sukan_post[4]['tags'][0]){
                res.write('<a class="tag" href="//hub.malayatimes.com/'+sukan_post[4]['tags'][0].tag_path+'">'+sukan_post[4]['tags'][0].tag_title+'</a>')
            }
            res.write('</div></article> </div></div>')
        }

        if(sukan_post[5]){
            ganda = 0;
            res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
            for (let i = 5; i < sukan_post.length; i++) {
                ganda +=1;
                if(ganda < 4){
                    article_id += 1;
                    const post_sukan = sukan_post[i];
                    res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_sukan.category+'/'+post_sukan.path+'"><img alt=" '+post_sukan.title+' " src="//media.malayatimes.com/picture/'+post_sukan.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_sukan.category+'/'+post_sukan.path+'">'+post_sukan.title+'</a></h3>')

                    var masa_utc = new Date(sukan_post[5]['masa']).toISOString()
                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(post_sukan['tags'][0]){
                        res.write('<a class="tag" href="//hub.malayatimes.com/'+post_sukan['tags'][0].tag_path+'">'+post_sukan['tags'][0].tag_title+'</a>')
                    }
                    res.write('</div></article>')
                }

            }
            res.write('</div></div>')
        }
        res.write('</div><!-- /class="content latest"-->')

    res.write('</section>')
    }
    /* /SECTION SUKAN FEED */

            /* SECTION BERITA FEED */
            if(data['berita_feed'][0]){
                const berita_post = data['berita_feed'];
                res.write('<section class="section_wrapper berita">')

                res.write('<h2><a href="//berita.malayatimes.com" title="Berita Terkini Malaysia">Berita Terkini</a></h2>')

                res.write('<div class="content latest">')
                //berita big
                if(berita_post[0]){
                    article_id += 1;
                    res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+berita_post[0].category+'/'+berita_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+berita_post[0].first_img+') no-repeat"><img alt=" '+berita_post[0].title+' " src="//media.malayatimes.com/photo/'+berita_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+berita_post[0].category+'/'+berita_post[0].path+'">'+berita_post[0].title+'</a></h3> ')

                    var masa_utc = new Date(berita_post[0]['masa']).toISOString()
                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(berita_post[0]['tags'][0]){
                        res.write('<a class="tag" href="//hub.malayatimes.com/'+berita_post[0]['tags'][0].tag_path+'">'+berita_post[0]['tags'][0].tag_title+'</a>')
                    }
                    res.write('</div></article> </div></div>')
                }

                //berita small group
                if(berita_post[1]){
                    ganda = 0;
                    res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                    for (let i = 1; i < berita_post.length; i++) {
                        ganda +=1;
                        if(ganda < 4){
                            article_id += 1;
                            const post_berita = berita_post[i];
                            res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_berita.category+'/'+post_berita.path+'"><img alt=" '+post_berita.title+' " src="//media.malayatimes.com/picture/'+post_berita.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_berita.category+'/'+post_berita.path+'">'+post_berita.title+'</a></h3>')

                            var masa_utc = new Date(post_berita['masa']).toISOString()
                            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                            if(post_berita['tags'][0]){
                                res.write('<a class="tag" href="//hub.malayatimes.com/'+post_berita['tags'][0].tag_path+'">'+post_berita['tags'][0].tag_title+'</a>')
                            }
                            res.write('</div></article>')
                        }

                    }
                    res.write('</div></div>')
                }

                if(berita_post[4]){
                    article_id += 1;
                    res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+berita_post[4].category+'/'+berita_post[4].path+'" style="background:url(//media.malayatimes.com/photo/'+berita_post[4].first_img+') no-repeat"><img alt=" '+berita_post[4].title+' " src="//media.malayatimes.com/photo/'+berita_post[4].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+berita_post[4].category+'/'+berita_post[4].path+'">'+berita_post[4].title+'</a></h3> ')

                    var masa_utc = new Date(berita_post[4]['masa']).toISOString()
                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(berita_post[4]['tags'][0]){
                        res.write('<a class="tag" href="//hub.malayatimes.com/'+berita_post[4]['tags'][0].tag_path+'">'+berita_post[4]['tags'][0].tag_title+'</a>')
                    }
                    res.write('</div></article> </div></div>')
                }

                if(berita_post[5]){
                    ganda = 0;
                    res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                    for (let i = 5; i < berita_post.length; i++) {
                        ganda +=1;
                        if(ganda < 4){
                            article_id += 1;
                            const post_berita = berita_post[i];
                            res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_berita.category+'/'+post_berita.path+'"><img alt=" '+post_berita.title+' " src="//media.malayatimes.com/picture/'+post_berita.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_berita.category+'/'+post_berita.path+'">'+post_berita.title+'</a></h3>')

                            var masa_utc = new Date(post_berita['masa']).toISOString()
                            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                            if(post_berita['tags'][0]){
                                res.write('<a class="tag" href="//hub.malayatimes.com/'+post_berita['tags'][0].tag_path+'">'+post_berita['tags'][0].tag_title+'</a>')
                            }
                            res.write('</div></article>')
                        }

                    }
                    res.write('</div></div>')
                }

                res.write('</div><!--/class="content latest"-->')

            res.write('</section>')
            }
            /* /SECTION BERITA FEED */

        /* SECTION VIRAL FEED */
        if(data['viral_feed'][0]){
            const viral_post = data['viral_feed'];
            res.write('<section class="section_wrapper viral">')

            res.write('<h2><a href="//viral.malayatimes.com" title="Isu viral Malaysia">Viral Malaysia</a></h2>')

            res.write('<div class="content latest">')
            //viral big
            if(viral_post[0]){
                article_id += 1;
                res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+viral_post[0].category+'/'+viral_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+viral_post[0].first_img+') no-repeat"><img alt=" '+viral_post[0].title+' " src="//media.malayatimes.com/photo/'+viral_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+viral_post[0].category+'/'+viral_post[0].path+'">'+viral_post[0].title+'</a></h3> ')

                var masa_utc = new Date(viral_post[0]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(viral_post[0]['tags'][0]){
                    res.write('<a class="tag" href="//hub.malayatimes.com/'+viral_post[0]['tags'][0].tag_path+'">'+viral_post[0]['tags'][0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')
            }

            //viral small group
            if(viral_post[1]){
                ganda = 0;
                res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                for (let i = 1; i < viral_post.length; i++) {
                    ganda +=1;
                    if(ganda < 4){
                        article_id += 1;
                        const post_viral = viral_post[i];
                        res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_viral.category+'/'+post_viral.path+'"><img alt=" '+post_viral.title+' " src="//media.malayatimes.com/picture/'+post_viral.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_viral.category+'/'+post_viral.path+'">'+post_viral.title+'</a></h3>')

                        var masa_utc = new Date(post_viral['masa']).toISOString()
                        res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                        if(post_viral['tags'][0]){
                            res.write('<a class="tag" href="//hub.malayatimes.com/'+post_viral['tags'][0].tag_path+'">'+post_viral['tags'][0].tag_title+'</a>')
                        }
                        res.write('</div></article>')
                    }

                }
                res.write('</div></div>')
            }

            if(viral_post[4]){
                article_id += 1;
                res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+article_id+'"><a class="img" href="//'+viral_post[4].category+'/'+viral_post[4].path+'" style="background:url(//media.malayatimes.com/photo/'+viral_post[4].first_img+') no-repeat"><img alt=" '+viral_post[4].title+' " src="//media.malayatimes.com/photo/'+viral_post[4].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+viral_post[4].category+'/'+viral_post[4].path+'">'+viral_post[4].title+'</a></h3> ')

                var masa_utc = new Date(viral_post[4]['masa']).toISOString()
                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(viral_post[4]['tags'][0]){
                    res.write('<a class="tag" href="//hub.malayatimes.com/'+viral_post[4]['tags'][0].tag_path+'">'+viral_post[4]['tags'][0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')
            }

            if(viral_post[5]){
                ganda = 0;
                res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
                for (let i = 5; i < viral_post.length; i++) {
                    ganda +=1;
                    if(ganda < 4){
                        article_id += 1;
                        const post_viral = viral_post[i];
                        res.write('<article class="hentry content-item" id="article-'+article_id+'"><div class="inner-wrapper"><a class="img" href="//'+post_viral.category+'/'+post_viral.path+'"><img alt=" '+post_viral.title+' " src="//media.malayatimes.com/picture/'+post_viral.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_viral.category+'/'+post_viral.path+'">'+post_viral.title+'</a></h3>')

                        var masa_utc = new Date(post_viral['masa']).toISOString()
                        res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                        if(post_viral['tags'][0]){
                            res.write('<a class="tag" href="//hub.malayatimes.com/'+post_viral['tags'][0].tag_path+'">'+post_viral['tags'][0].tag_title+'</a>')
                        }
                        res.write('</div></article>')
                    }

                }
                res.write('</div></div>')
            }

            res.write('</div><!--/class="content latest"-->')
        res.write('</section>')
        }
        /* /SECTION VIRAL FEED */

    //////////// DYNAMIC END ///////////////
    res.write('</div></div><!--class="widget Blog" data-version="1" id="Blog1"-->')
    res.write(`<footer id="blog-footer">
       <p class="home-page"><a href=" //www.malayatimes.com/">Berita Semasa Malaysia</a></p>
       <aside class="not-important">
          <div id="login-and-search" class="search_and_social">
             <div class="section_wrapper">
                <h5 class="heading"><a href="https://www.malayatimes.com" title="Berita Semasa Malaysia"><img alt="Berita Semasa Malaysia" src="https://media.malayatimes.com/assets/image/malayatimes.png"></a></h5>
                <div class="search_box">
                   <div class="inline_block">
                      <div id="search_box" class="es_input" contenteditable="true">
                      </div><span id="search-button" class="cari"></span></div>
                </div>

                <div id="search-result">
                      <div id="stanby" style="dislay:none;">
                         <span class="loader">
                            <span class="pusing"></span>
                         </span>
                      </div>
                      <span id="close-search-result" onclick="clear_search()">clear</span>
                </div>

                <div class="or"></div>
                <span class="text_like">like &amp; follow us</span>

                <div id="fans"></div>

                <button id="close-login-search" class="close_section">×</button>
             </div>
          </div>
             <script type="text/javascript" src="https://media.malayatimes.com/js/main/home.js"></script>
       </aside>
    </footer>`)
    res.write('</body></html>')
    res.end();

} // end create server


module.exports.serve_client = serve_client;
