const db = require('../../db');
const helper = require('../../helper');
const send = require('./serve_client');
/*
SET UP DATA THAT USING MySQL query.. if you have another data from db nned to use. add more STEP
*/



var homepage_data = function(req, res, current_url, data){
    /* STEP 1 */
    var latest_post = function(err1, rows1) { //what ever category from post_tbl..
        if(err1){
            console.log('error latest_post sql');
            console.log(err1);
            throw err1;
        } else {
            data['latest_post'] = rows1;
            for (let i = 0; i < data['latest_post'].length; i++) {
                if(data['latest_post'][i]['tag_path'] && data['latest_post'][i]['tag_title'] && data['latest_post'][i]['tag_type']){
                    data['latest_post'][i]['tags'] = helper.build_tag(data['latest_post'][i]['tag_type'], data['latest_post'][i]['tag_path'], data['latest_post'][i]['tag_title']);
                } else {
                    data['latest_post'][i]['tags'] = new Array();
                }
            }
            var arr_id_latest_post = new Array()
            for (let i = 0; i < data['latest_post'].length; i++) {
                if(data['latest_post'][i].id){
                    arr_id_latest_post.push(data['latest_post'][i].id)
                }
            }
            if(arr_id_latest_post.length > 0){
                var id_tak_mau = arr_id_latest_post.join(',')
            } else {
                var id_tak_mau = '99999999999999999999999999999';
            }

            /*********************** POPULAR *************************/
            var popular_feed = function(err6, rows6){
                if(err6){
                   console.log('error viral sql');
                   console.log(err6);
                } else {
                   if(rows6){
                        data['popular_feed'] = rows6;
                        for (let i = 0; i < data['popular_feed'].length; i++) {
                           if(data['popular_feed'][i]['domain']){
                                data['popular_feed'][i]['category'] = data['popular_feed'][i]['domain'].replace('.malayatimes.com','');
                           } else {
                                data['popular_feed'][i]['category'] = false;
                           }
                           if(data['popular_feed'][i]['id']){
                                id_tak_mau = id_tak_mau+','+data['popular_feed'][i]['id'];
                           }
                        }
                   }

                   /* FINALLY */
                   send.serve_client(req,res,current_url,data, id_tak_mau);
                   // send.serve_client(req,res,current_url,data, id_tak_mau);

                }

            }
            // console.log('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
            // console.log(helper.datetime().replace(/ /g, ''))
            var dariBulan = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(100000000); // waktu2 1 bulan
            // console.log(id_tak_mau, dariBulan)
            // 20180712013030
            // console.log(dariBulan)
            // console.log('HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH')
            // db.statement(`
            //    (SELECT reputation_manager.from_id as id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path AS path, category_manager.domain AS domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.time > `+dariBulan+` AND reputation_manager.from_tbl = 'post_tbl' AND post_tbl.status = 'publish' AND  reputation_manager.from_id NOT IN(`+id_tak_mau+`) AND category_manager.domain != 'blog.malayatimes.com' GROUP BY reputation_manager.from_id ORDER BY score DESC) UNION DISTINCT
            //    (SELECT reputation_manager.from_id as id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path AS path, category_manager.domain AS domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.from_tbl = 'post_tbl' AND post_tbl.status = 'publish' AND  reputation_manager.from_id NOT IN(`+id_tak_mau+`) AND category_manager.domain != 'blog.malayatimes.com' GROUP BY reputation_manager.from_id ORDER BY score DESC) LIMIT 0,10`, false, popular_feed);
            db.statement("SELECT reputation_manager.from_id as id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path AS path, category_manager.domain AS domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.time > "+dariBulan+" AND reputation_manager.from_tbl = 'post_tbl' AND post_tbl.status = 'publish' AND  reputation_manager.from_id NOT IN("+id_tak_mau+") AND category_manager.domain != 'blog.malayatimes.com' GROUP BY reputation_manager.from_id ORDER BY score DESC LIMIT 0,10", false, popular_feed);
            /*********************** POPULAR END ************************/

        }
    }
    db.statement("SELECT post_tbl.*, category_manager.domain AS category, users.namapena, Group_concat(tag_tbl.tag_path) AS tag_path, Group_concat(tag_tbl.title) AS tag_title, Group_concat(tag_tbl.type) AS tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path AND tag_tbl.status = 'publish' WHERE post_tbl.status = 'publish' AND category_manager.domain != 'blog.malayatimes.com' GROUP BY post_tbl.masa ORDER BY post_tbl.masa DESC LIMIT 0, 9 ", false, latest_post);




} // end create server


module.exports.homepage_data = homepage_data;
