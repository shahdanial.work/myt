
const fs = require('fs')
const Entities = require('html-entities').XmlEntities;
const entities = new Entities();
const helper = require('../../helper')

var server_single_post = function(req, res, current_url, data) {
   if (data['post']) {
      var post = data['post'];
      // console.log('//////////////////////////////////////\n\n')
      // console.log(post)
      // console.log('\n\n///////////////////////////////////////')

      res.writeHead(200, {
         'Content-Type': 'text/html\; charset=utf-8'
      });
      res.write('<!DOCTYPE html>\n')
      res.write('<html class="main-blog post-page">')
      res.write(data['head_open'])

      //res.write('<meta name="theme-color" content="#333">')

      /////////// SEO meta ////////////
      var seo_title = 'Tiada title';
      if (post.meta_title) {
         seo_title = post.meta_title;
      } else if (post.title) {
         seo_title = post.title;
      }
      res.write('<title>' + entities.encode(seo_title) + '</title>')

      if (post.meta_description) res.write('<meta content="' + entities.encode(post.meta_description) + '" name="description"/>')


      if (post.google_thumb) {
         var meta_image_src = 'foto/' + post.google_thumb;
      } else if (post.viral_thumb) {
         var meta_image_src = 'foto/' + post.viral_thumb;
      } else if (post.first_img) {
         var meta_image_src = 'foto/' + post.first_img;
      } else {
         var meta_image_src = 'malaya-times.jpg';
      }
      res.write('<link href="https://media.malayatimes.com/' + meta_image_src + '" rel="image_src"/>')

      /////////// OGP.ME (Twitter, wFacebook and Google+ use it) ////////////

      res.write('<meta property="og:type" content="article">')

      res.write('<meta name="twitter:card" content="summary_large_image">')

      if (post.viral_thumb) {
         var thumbnail_viral = 'foto/' + post.viral_thumb;
      } else if (post.google_thumb) {
         var thumbnail_viral = 'foto/' + post.google_thumb;
      } else if (post.first_img) {
         var thumbnail_viral = 'foto/' + post.first_img;
      } else {
         var thumbnail_viral = 'malaya-times.jpg'
      }
      res.write('<meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/' + thumbnail_viral + '">')


      res.write('<meta name="twitter:site" content="@malayatimes">')

      if (post.viral_title) {
         var viral_title = post.viral_title
      } else {
         var viral_title = post.title;
      }
      res.write('<meta property="og:title" name="twitter:title" content="' + entities.encode(viral_title) + '">')

      if (post.viral_description) {
         var viral_description = post.viral_description;
      } else if (post.meta_description) {
         var viral_description = post.meta_description;
      } else if (post.title) {
         var viral_description = post.title + ' trending di Malayatimes. Baca lanjut ' + post.title + ' yang sedang viral kini.';
      } else {
         var viral_description = false;
      }
      if (viral_description) res.write('<meta property="og:description" name="twitter:description" content="' + entities.encode(viral_description) + '">');

      if (post.path) res.write('<meta rel="canonical" content="//'+data['domain']+'/' + post.path + '" property="og:url">');

      res.write('<meta property="fb:app_id" content="1829602560385819">')

      // res.write('<script src="//media.malayatimes.com/js/es5.js"></script>')

      res.write('<link rel="stylesheet" type="text/css" href="//media.malayatimes.com/css/singlepost.css"/>')
      res.write('<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>')

      // adsense head..
      // var adsense_head = fs.readFileSync('./adsense/before_head_single.html', 'utf8')
      // res.write(adsense_head)

      if(data.keyword) res.write(data.keyword)

      res.write(data['head_close'])
      res.write('<body id="main-blog">') // because it's a main website. this is main thing including the header !
      res.write(data['menu_nav'])
      res.write(data['header'])

      // adsense bwh header..
      if(data.showAds){
         if(data.showAds.adseseHeader) res.write(data.showAds.adseseHeader);
      }

      //////////// DYNAMIC START /////////////
      res.write('<div class="main-wrapper post-page" id="wrapper">')



      //////////////////////////// CONTENT /////////////////////
      res.write('<main id="content">')
      res.write('<article class="post hentry" id="post-' + post['id'] + '">')
      res.write('<div id="main">')//class="hfeed"

      /* TITLE */
      res.write('<h1 class="entry-title">' + post['title'] + '</h1>')

      /* META POST */
      if (post['namapena'] || post['masa'] || post['tags'][0]) {
         res.write('<p class="entry-meta headline-meta" id="headline_meta">')
         if (post['namapena']) {
            res.write('<span class="vcard author"><span class="fn" title="' + post['namapena'] + '"><a href="//www.malayatimes.com/' + post['namapena'] + '" rel="author" title="author profile">' + post['namapena'] + '</a></span></span>')
         }

         if (post['masa']) {
            //var masa_rpc = post['masa'].replace(' ', 'T');
            var masa_utc = new Date(post['masa']).toISOString()
            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')
         }

         if (post['tags'][0]) {
            // console.log(post.tags[0]);
            res.write('<a class="category" href="//hub.malayatimes.com/' + post['tags'][0]['tag_type'] +'/'+ post['tags'][0]['tag_path'] + '" rel="category tag" title="' + post['tags'][0]['tag_title'] + '">' + post['tags'][0]['tag_title'] + '</a>')
         }
         res.write('</p>')
      }

      /* SOCIAL SHARE BUTTON */
      res.write('<aside id="share" class="share_buttons social_share">')
      res.write('<p class="share-count-container"><em class="shares-count">'+helper.number_k(post.pageview)+'</em><span class="label">shares</span></p>')
      res.write('<p class="social-buttons">')

      res.write('<span class="label_button">Share this news with your friends </span>')

      var url_share = encodeURIComponent('https://' + data['domain'] + '/' + post['path']);

      res.write('<a rel="nofollow" target="_blank" id="share-facebook" class="button-share facebook " href="//www.facebook.com/dialog/share?app_id=1829602560385819&display=page&href=' + url_share + '"><img alt="Share berita ' + post['title'] + ' di Facebook " src="//2.bp.blogspot.com/-gMk-7bgjdPE/Vvg2rzB6_LI/AAAAAAAAAp0/LcIOou21K1cG-h1jBer9sWRteljCrrbow/s1600/fb-icon.png"></a>')
      res.write('<a rel="nofollow" target="_blank" id="share-google" class="button-share google " href="//plus.google.com/share?url=' + url_share + '&text=' + encodeURIComponent(post['title']) + '"/><img alt="Share berita ' + post['title'] + ' di Google+ " src="//2.bp.blogspot.com/-EF-q1k1uR00/Vvg3S95Uz1I/AAAAAAAAAp8/7_R7ptzkaYQvZvTkVcCHfFHovETJskGaQ/s1600/G%252Bicon.png"></a>')
      res.write('<a rel="nofollow" target="_blank" id="share-twitter" class="button-share tweet " href="//twitter.com/intent/tweet?text=' + encodeURIComponent(post['title']) + '&url=' + url_share + '&via=malayatimes"><img alt="Tweet berita ' + post['title'] + ' di Twitter " src="//3.bp.blogspot.com/-ECcM-77Rj_g/Vvg3kAVesZI/AAAAAAAAAqA/QmI2oby6HSoBj8pll5MGAnQdvHdCXWq5A/s1600/twitter.icon.png"></a>')
      res.write('<a id="share-whatsapp" class="button-share whatsapp"  href="//api.whatsapp.com/send?text=' + encodeURIComponent(post['title'] + ' >> ' + '//' + data['domain'] + '/' + post['path']) + '" rel="nofollow" target="_blank"><img alt="Kongsi berita ' + post['title'] + ' di melalui Whatsapp " src="//1.bp.blogspot.com/-OUhkisqZ4kE/Vvg306mhUMI/AAAAAAAAAqE/_RYLuC-sLOUBsjtFmyqYqdeXDL2AU6TUw/s1600/whatsapp-icon.png"></a>')

      res.write('</p>')
      res.write('</aside>')

      /* ENTRY CONTENT */
      res.write('<div class="entry-content" id="entry-content">')
      var content = post['content'].replace(/\"para\"/g, '\"paragraph\"')
      res.write(content)
      res.write('</div>') // end #entry content





      res.write('</div>')
      res.write('</article>') //class="post hentry" (main post)
      if (data['related_post'].length > 0) {
         var even_odd = 'even';
         res.write('<section id="related"><div class="related_wrapper"><h3>RELATED</h3>')
         res.write('<span class="button slide_left"></span>')
         for (var i = 0; i < data['related_post'].length; i++) {
            var item = data['related_post'][i];
            var related_link = '//' + item.domain + '/' + item.path;
            var related_thumb = '//media.malayatimes.com/photo/' + item.first_img;
            var left = parseFloat(305) * parseFloat(i);

            res.write('<div class="related_post big ' + even_odd + '" id="related-' + i + '" style="left:' + left + 'px;">')
            res.write('<div class="related_inner">')
            res.write('<a href="' + related_link + '" class="related-thumb"><img src="' + related_thumb + '" /></a>')
            res.write('<a href="' + related_link + '" class="related-title">' + item.title + '</a>')
            res.write('</div></div>')

            if (even_odd == 'even') {
               even_odd = 'odd';
            } else {
               even_odd = 'even';
            }
         }
         var last_left = parseFloat(left) + parseFloat(305);
         res.write('<div class="related_post big '+even_odd+' see_more_related" id="related-'+data['related_post'].length+'" style="left:'+last_left+'px;"><div class="related_inner"><a href="//'+data['domain']+'">SEE MORE</a></div></div>')

         res.write('<span class="button slide_right"></span>')
         res.write('</div></section>')
      }
      res.write('</main>')
      /////////////////////////// CONTENT END //////////////////

      ///////////////////////// SIDEBAR ////////////////////////
      res.write('<aside class="sidebar secondary" id="sidebar">')

      // adsense sidebar tengah..
      res.write('<div class="widget adsense_widget"> <h4 class="widget-heading">Advertisment</h4> <div class="widget-content">')
      res.write("<div id='gpt-kiri'><script>googletag.cmd.push(function() { googletag.display('gpt-kiri');  }) </script></div>")
      res.write('</div></div>')

      if (data['popular_feed']) {
         if (data['popular_feed'][0]) {
            res.write('<div class="widget popular_post_widget"> <h3 class="widget-heading">Trending</h3> <div class="widget-content">')

            for (let i = 0; i < data['popular_feed'].length; i++) {
               var popular_post = data['popular_feed'][i];
               res.write('<div class="popular-post"><div class="inner-wrapper"> <a class="img" href="//' + popular_post.domain + '/' + popular_post.path + '"><img alt=" ' + popular_post.title + ' " src="//media.malayatimes.com/picture/' + popular_post.first_img + '"></a> <p><a class="popular-title" href="//' + popular_post.domain + '/' + popular_post.path + '">' + popular_post.title + '</a></p> </div></div>')

            }

            res.write('</div></div>')
         }
      }
      res.write('</aside>')
      //////////////////////// END SIDEBAR /////////////////////

      /////////////////////// SIDEBAR 2 ///////////////////////
      res.write('<aside class="sidebar aside" id="sidebar_1">')
      //adsense media
      res.write('<div class="widget widget_adsense_ads">')
      res.write('<h4 class="widget-heading">Sponsor</h4>')
      res.write('<div class="widget-content">')
      // res.write('<a href="https://www.facebook.com/photo.php?fbid=2378748602152829&set=a.136723199688725"><img src="https://3.bp.blogspot.com/-ONcyFRyuyPY/W_rc9DPA45I/AAAAAAAADBE/JfHGz59djNcrv1PFGabJvFGdEYys4lrQACLcBGAs/s1600/iklan-tansri-2.jpg" /></a>')
         res.write("<div id='div-gpt-ad-1524121116261-0'><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1524121116261-0') })</script></div>")
      res.write('</div>')
      res.write('</div>')
      //visitor online..
      res.write('<div class="widget visitor_online">')
      res.write('<h4 class="widget-heading">Live Visitor</h4>')
      res.write(' <div class="widget-content">')
      res.write('<div id="visitors"><p><span id="live-visitor-count">'+helper.number_k((post.jumlahOnline + helper.tambahanOnline()))+'</span> ONLINE</p><div class="lintangan"></div></div>')
      res.write('</div>')
      res.write('</div>')
      res.write('</aside>')
      ///////////////////// END SIDEBAR 2 /////////////////////



      res.write('</div><!-- end #main-wrapper -->')
      //////////// DYNAMIC END ///////////////
      res.write( fs.readFileSync('./element/footer_single.html', 'utf8') )
      res.write('</body></html>')
      res.end();

   } // end if data post
} // end create server


module.exports.server_single_post = server_single_post;
