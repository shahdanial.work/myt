var send = require('./format_data')
const db = require('../../db')
const mysql = require('mysql')
const helper = require('../../helper')

var get_popular = function(req,res,current_url, data, id_tak_mau){

    var popular_feed = function(err1, rows1){
        if(err1){
            console.log('error popular_single sub sql \n', err1);
            throw err1;
        } else {
            if(rows1){
                data['popular_feed'] = rows1;
                for (let i = 0; i < data['popular_feed'].length; i++) {
                    if(data['popular_feed'][i]['id']){
                        id_tak_mau = id_tak_mau+', '+data['popular_feed'][i]['id'];
                    }
                }
            }
        }
        ///////// SEND TO VIEW ///////////////
        send.format_data(req,res,current_url, data);
    }
    db.statement(
      `SELECT reputation_manager.from_id, SUM(reputation_manager.reputation) AS score, post_tbl.title, post_tbl.first_img, post_tbl.path as path, category_manager.domain FROM reputation_manager LEFT JOIN post_tbl ON post_tbl.id = reputation_manager.from_id LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = reputation_manager.from_id WHERE reputation_manager.from_tbl = 'post_tbl' AND reputation_manager.from_id NOT IN (`+id_tak_mau+`) AND category_manager.domain = '`+data['domain']+`' GROUP BY reputation_manager.from_id ORDER BY score DESC
      LIMIT 0,10`, false, popular_feed);

} // end module

module.exports.get_popular = get_popular;
