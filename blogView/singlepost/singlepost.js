const db = require('../../db');
const helper = require('../../helper');
const mysql = require('mysql');

var getPostData = function(req,res,current_url, data){

    /* 1. CHECK path, if path available in MySQL post_tbl WHERE category is data['domain'] */

    var singlePostData = function(err1, rows) { //what ever category from post_tbl..
        if(err1){
            throw err1;
        } else {

            data['post'] = rows[0];
            if(data.post){
               // send to view..
               const send = require('./get_another_data')
               send.get_another_data(req,res,current_url, data)
               // up reputation
               const send_1 = require('./kira_reputation')
               send_1.kira_reputation(data)

            } else {
               res.writeHead(404, {
                  'Content-Type': 'text/plain'
               })
               res.write('Page ini tidak wujud')
               res.end()
            }


        }
    }


   var d = new Date()
   var masa_utc = d.getMinutes()

   // console.log(d.getHours())

   var myArray = [201, 203, 204, 205, 206, 207, 208, 209];
   var rand = myArray[(Math.random() * myArray.length) | 0]

   var dariJam = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(10000); // waktu2 1 jam sebelum
   // var dariHari = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(1000000); // waktu2 1 hari
   // var dariBulan = parseFloat(helper.datetime().replace(/ /g, '')) - parseFloat(100000000); // waktu2 1 bulan hari
   // console.log(helper.datetime())
   var query = `
   SELECT
    post_tbl.*,
    users.namapena,
    users.id_user,
    group_concat(tag_tbl.tag_path) as tag_path,
    group_concat(tag_tbl.title) as tag_title,
    group_concat(tag_tbl.type) as tag_type,
    group_concat(keyword_manager.mykey) as mykey,
    group_concat(keyword_manager.myvalue) as myvalue,

   (SELECT COALESCE(Count(id), 0) + `+rand+`
                  FROM   reputation_manager
                  WHERE  time > `+dariJam+`) AS jumlahOnline

    FROM post_tbl

      LEFT JOIN category_manager
      ON category_manager.from_tbl = 'post_tbl'
      AND category_manager.from_id = post_tbl.id

      LEFT JOIN users ON users.id_user = post_tbl.author_id

      LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl'
      AND tag_manager.from_id = post_tbl.id

      LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path
      AND tag_tbl.status = 'publish' AND tag_tbl.type != ''

      LEFT JOIN keyword_manager ON keyword_manager.from_tbl = 'post_tbl'
      AND keyword_manager.from_id =  post_tbl.id

   WHERE
      post_tbl.path = ?
      AND category_manager.domain = ?

   GROUP BY post_tbl.id
   ORDER BY post_tbl.id
   ASC LIMIT 0,1
   `;

   var string = [
      data['path'],
      data['domain']
   ];

   db.statement(query, string, singlePostData);

}
module.exports.getPostData = getPostData;
