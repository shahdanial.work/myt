const db = require('../../db')
const mysql = require('mysql')

var filter_api = function(req, res) {

   var url = require('url');
   var url_parts = url.parse(req.url, true);
   var data = url_parts.query;

   //callback
   var query_callback = function(err1, rows) {
     if(err1) throw err1;
     if(!rows) data['item'] = [];
     else  data['item'] = rows
     // console.log(rows);
        const next = require('./json')
        next.serve_json(req, res, data)

   }

   if(!data.limit) data.limit = 10

   // query
   var query =
     `SELECT DISTINCT keyword
      FROM searched_tbl
      WHERE (keyword like ? OR keyword like ? OR keyword like ?)

     ORDER BY id
     DESC

     LIMIT 0,?`;

   // string
   var string = [
     data['keyword']+'_%_%',
     data['keyword']+'%',
     '% '+data['keyword']+'%',
     //parseFloat(data.offset),
     parseFloat(data.limit)
  ];

   db.statement(query, string, query_callback)

}
module.exports.filter_api = filter_api