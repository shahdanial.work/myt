var serve_json = function(req, res, data){
   var obj  = {}
   obj['title'] = []
   if(data.item){
      for (var i = 0; i < data.item.length; i++) {
         var title = data.item[i]['keyword'] || false
         if(title) obj['title'].push(title)
      }
   }
   res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
   res.write(JSON.stringify(obj, null, 0))
   // res.write(obj)
   res.end()
}
module.exports.serve_json = serve_json