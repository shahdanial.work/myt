const helper = require('./../../helper')
var serve_api = function(req, res, post, data) {
   // console.log(data);

   for (var i = 0; i < post.length; i++) {

      /* masa */
      post[i]['masa'] =  new Date(post[i]['masa']).toISOString()

      /* tags */
      if (post[i]['tag_title'] && post[i]['tag_path'] && post[i]['tag_type']) {
         post[i]['tags'] = helper.build_tag(post[i]['tag_path'], post[i]['tag_title'], post[i]['tag_type'])

         /* .reduce(function(result, item) {
            var keys = Object.keys(item);
            for (var i = 0; i < keys.length; i++) {
               result[keys[i]] = item[keys[i]];
            }
            return result;
          }, {});
          */
      } else {
         post[i]['tags'] = false;
      }
      // delete post[i]['tag_title'], delete post[i]['tag_path'], delete post[i]['tag_type'];

   }


   res.writeHead(200, {
      'Content-Type': 'text/plain\; charset=utf-8'
   });
   //////////////////////// START SERVE //////////////////////




   /* kotak 1 (0)*/
   if(post[0]){
       res.write('<div class="recent-articles even big"><div class="content_group_inner_wrapper"> <article class="content-item"><a class="img" href="//'+data['category']+'/'+post[0]['path']+'" style="background:url(//media.malayatimes.com/photo/'+post[0]['first_img']+') no-repeat"><img alt=" '+post[0]['title']+' " src="//media.malayatimes.com/photo/'+post[0]['first_img']+'"></a><div class="header"> <h3><a class="title" href="//'+data['category']+'/'+post[0]['path']+'">'+post[0]['title']+'</a></h3>');
      if(post[0]['tags']){
            res.write('<a class="tag" href="//hub.malayatimes.com/'+post[0]['tags'][0]['tag_type']+'/'+post[0]['tags'][0]['tag_path']+'">'+post[0]['tags'][0]['tag_title']+'</a>');
      }
       res.write('</div></article> </div></div>');
   }




   /* kotak 2 (1,2,3)*/
   if(post[1]) res.write('<div class="recent-articles odd">');
   for (var i = 1; i < 4; i++) {
      if(post[i]){
         res.write('<article class="recent-article"> <a class="img" href="//'+ data['category'] +'/' + post[i]['path'] + '"> <img alt=" '+post[i]['title']+' " src="//media.malayatimes.com/picture/'+post[i]['first_img']+'"> </a> <h2><a class="title" href="//'+ data['category'] + '/' +post[i]['path']+'">'+post[i]['title']+'</a></h2>');
         if(post[i]['tags']){
              res.write('<a class="tag" href="//hub.malayatimes.com/'+post[i]['tags'][0]['tag_type']+'/'+post[i]['tags'][0]['tag_path']+'">'+post[i]['tags'][0]['tag_title']+'</a>');
         }
         res.write('</article>');
      }
   }
   if(post[1]) res.write('</div>');



   /* kotak 3 (4)*/
   if(post[4]){
       res.write('<div class="recent-articles even big"><div class="content_group_inner_wrapper"> <article class="content-item"><a class="img" href="//'+data['category']+'/'+post[4]['path']+'" style="background:url(//media.malayatimes.com/photo/'+post[4]['first_img']+') no-repeat"><img alt=" '+post[4]['title']+' " src="//media.malayatimes.com/photo/'+post[4]['first_img']+'"></a><div class="header"> <h3><a class="title" href="//'+data['category']+'/'+post[4]['path']+'">'+post[4]['title']+'</a></h3>');
      if(post[4]['tags']){
            res.write('<a class="tag" href="//hub.malayatimes.com/'+post[4]['tags'][0]['tag_type']+'/'+post[4]['tags'][0]['tag_path']+'">'+post[4]['tags'][0]['tag_title']+'</a>');
      }
       res.write('</div></article> </div></div>');
   }




   /* kotak 4 (5, 6, 7)*/
   if(post[5]) res.write('<div class="recent-articles odd">');
   for (var i = 5; i < 8; i++) {
      if(post[i]){
         res.write('<article class="recent-article"> <a class="img" href="//'+ data['category'] +'/' + post[i]['path'] + '"> <img alt=" '+post[i]['title']+' " src="//media.malayatimes.com/picture/'+post[i]['first_img']+'"> </a> <h2><a class="title" href="//'+ data['category'] + '/' +post[i]['path']+'">'+post[i]['title']+'</a></h2>');
         if(post[i]['tags']){
              res.write('<a class="tag" href="//hub.malayatimes.com/'+post[i]['tags'][0]['tag_type']+'/'+post[i]['tags'][0]['tag_path']+'">'+post[i]['tags'][0]['tag_title']+'</a>');
         }
         res.write('</article>');
      }
   }
   if(post[5]) res.write('</div>');



   ////////////////////////  END SERVE  //////////////////////
   res.end();

}
module.exports.serve_api = serve_api;