const helper = require('./../../helper')
const db = require('../../db');
const parseFormdata = require('parse-formdata')
const fs = require('fs')

var filter_api = function(req, res) {

  parseFormdata(req, (err, datas) => {
     if(req.method != 'POST') err = true;
      if (err) {
           // Set whatever status code you want
           res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
           res.end();
           throw err;
      }

      var data = datas.fields;
      var allowed_referer = helper.compare_domain(req.headers.referer,'*.malayatimes.com');
      // console.log(data)

      if(allowed_referer){
         ////////////////////////////////////
         var my_feed = function(errr, dbdata){
             if(errr){
                res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
                  res.end();
                console.log('error viral sql', errr);
                throw errr;
             } else {

                if(dbdata.length > 0){
                   // there's data from this query.
                   post = dbdata;
                   const send = require('./'+data['limit_post'])
                   send.serve_api(req, res, post, data)
                } else {
                   // console.log('file ./api-dir/www/box_style_'+data['box_style']+'.js tidak wujud atau dbdata is 0')
                   res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
                   res.end();
                }

             }

         }
         var string = [
            data['category'],
            parseFloat(data['offset']),
            parseFloat(data['limit_post'])
         ];
         db.statement(`SELECT post_tbl.id, post_tbl.title, post_tbl.masa, post_tbl.first_img, post_tbl.path, users.namapena, group_concat(tag_tbl.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl
         LEFT JOIN category_manager ON category_manager.from_tbl = "post_tbl" AND category_manager.from_id = post_tbl.id
         LEFT JOIN users ON users.id_user = post_tbl.author_id
         LEFT JOIN tag_manager ON tag_manager.from_tbl = "post_tbl" AND tag_manager.from_id = post_tbl.id
         LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path AND tag_tbl.status = "publish" AND tag_tbl.type != ""
         WHERE post_tbl.status = "publish" AND category_manager.domain = ?
         GROUP BY post_tbl.id
         ORDER BY post_tbl.id DESC
         LIMIT  ?, ?`,  string, my_feed);
         ////////////////////////////////////
      }

  });

}
module.exports.filter_api = filter_api;