const helper = require('./../../helper')
const db = require('../../db');
const parseFormdata = require('parse-formdata')
const fs = require('fs')

var filter_api = function(req, res) {

  parseFormdata(req, (err, datas) => {
     if(req.method != 'POST') err = true;
      if (err) {
           // Set whatever status code you want
           res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
           res.end();
           throw err;
      }

      var data = datas.fields;

      data['limit_post'] = parseFloat(data['limit_post']);
      if(isNaN(data['limit_post'])) data['limit_post'] = 7;

      ////////////////////////////////////
      var my_feed = function(errr, dbdata){
          if(errr){
             res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
               res.end();
             console.log('error viral sql', errr);
             throw errr;
          } else {

             if(dbdata.length > 0){
                // && fs.existsSync('./api-dir/www/box_style_'+data['box_style']+'.js')){

                for (let i = 0; i < dbdata.length; i++) {
                   if(dbdata[i]['tag_path'] && dbdata[i]['tag_title'] && dbdata[i]['tag_type']){
                        dbdata[i]['tags'] = helper.build_tag(dbdata[i]['tag_type'], dbdata[i]['tag_path'], dbdata[i]['tag_title']);
                   } else {
                        dbdata[i]['tags'] = new Array();
                   }
               }

                  const send = require('./serve_api.js')//require('./box_style_'+data['box_style'])
                  send.serve_api(req, res, data, dbdata)
             }
             else {
                // console.log('file ./api-dir/www/box_style_'+data['box_style']+'.js tidak wujud atau dbdata is 0')
                res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
                res.end();
             }

          }

      }

      var string = [
         data['category']+'.malayatimes.com',
         parseFloat(data['limit_post'])
      ];
      // console.log(data);
      db.statement(`SELECT post_tbl.*, category_manager.domain as category, users.namapena, group_concat(tag_manager.tag_path) as tag_path, group_concat(tag_tbl.title) as tag_title, group_concat(tag_tbl.type) as tag_type FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = 'post_tbl' AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = 'post_tbl' AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path WHERE post_tbl.status = 'publish' AND category_manager.domain = ? AND post_tbl.id NOT IN (`+data['ids']+`) GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0,?`,  string, my_feed);
      ////////////////////////////////////

  });

}
module.exports.filter_api = filter_api;