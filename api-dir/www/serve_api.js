var serve_api = function(req, res, data, dbdata){
   var myinput = data,
       mypost = dbdata;

       var setting = {
          'must_low_box': false,
          'big_thumb': '/photo/',
          'small_thumb': '/picture/'
       }
       if(myinput.speed == 'slow') setting.big_thumb = '/image/';
       if(myinput.speed == 'slowest') setting.small_thumb = '/picker/', setting.big_thumb = '/picker/';
       if(myinput.speed == 'slowest' && myinput.b_width < 612) setting.must_low_box = true;

   // console.log(mypost)
   res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});

   // if(typeof data['category'] != 'undifined' && typeof data['heading'] != 'undifined') res.write('<h2><a href="https://'+data['category']+'.malayatimes.com" title="Gosip Artis Malaysia">'+data['heading']+'</a></h2>');

   // res.write('<div class="content latest">')

   //artis big (0)
   if(mypost[0]){

      var masa_utc = new Date(mypost[0]['masa']).toISOString()

      if(setting['must_low_box']){
         res.write('<div class="recent-articles column_6 even"><article class="hentry recent-article" id="article-'+mypost[0]['id']+'" style="margin:0">')
         res.write(' <a style="background: url(//media.malayatimes.com'+setting['big_thumb']+mypost[0].first_img+') no-repeat" class="img" href="//'+mypost[0].category+'/'+mypost[0].path+'"> '+mypost[0].title+' </a>')
         res.write(' <h3 class="entry-title"><a class="title" href="//' + mypost[0].category + '/' + mypost[0].path + '">'+mypost[0].title+'</a></h3>')
         res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time> </article></div>')
      } else {

         res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+mypost[0]['id']+'"><a class="img" href="//'+mypost[0].category+'/'+mypost[0].path+'" style="background:url(//media.malayatimes.com'+setting['big_thumb']+mypost[0].first_img+') no-repeat">'+mypost[0].title+'</a><div class="header"> <h3 class="entry-title"><a class="title" class="entry-title" href="//'+mypost[0].category+'/'+mypost[0].path+'">'+mypost[0].title+'</a></h3> ')

         res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

         if(mypost[0]['tags'][0]){
              res.write('<a class="tag" href="//hub.malayatimes.com/'+mypost[0]['tags'][0].tag_path+'">'+mypost[0]['tags'][0].tag_title+'</a>')
         }
         res.write('</div></article> </div></div>')

      }
   }

   //artis small group (1-3)
   if(mypost[1]){

      res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
      for (let i = 1; i < 4; i++) {

               const post_artis = mypost[i];

               if(post_artis){

                  var masa_utc = new Date(post_artis['masa']).toISOString()

                  res.write('<article class="hentry content-item" id="article-'+post_artis['id']+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com'+setting.small_thumb+''+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

                  res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                  if(post_artis['tags'][0]){
                      res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
                  }
                  res.write('</div></article>')

               }

      }
      res.write('</div></div>')
   }

   // small group (4-6)
   //artis small group (1-3)
   if(mypost[4]){

      res.write('<div class="content_group_wrapper even small"><div class="content_group_inner_wrapper">')
      for (let i = 4; i < 7; i++) {

               const post_artis = mypost[i];

               if(post_artis){

                  var masa_utc = new Date(post_artis['masa']).toISOString()

                  res.write('<article class="hentry content-item" id="article-'+post_artis['id']+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com'+setting.small_thumb+''+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

                  res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                  if(post_artis['tags'][0]){
                      res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
                  }
                  res.write('</div></article>')

               }

      }
      res.write('</div></div>')
   }

   if(mypost[7]){

      var masa_utc = new Date(mypost[7]['masa']).toISOString()

      if(setting['must_low_box']){
         res.write('<div class="recent-articles column_6 odd"><article class="hentry recent-article" id="article-'+mypost[7]['id']+'" style="margin:0">')
         res.write(' <a style="background: url(//media.malayatimes.com'+setting['big_thumb']+mypost[7].first_img+') no-repeat" class="img" href="//'+mypost[7].category+'/'+mypost[7].path+'"> '+mypost[7].title+' </a>')
         res.write(' <h3 class="entry-title"><a class="title" href="//' + mypost[7].category + '/' + mypost[7].path + '">'+mypost[7].title+'</a></h3>')
         res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time> </article></div>')
      } else {

         res.write('<div class="content_group_wrapper odd big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+mypost[7]['id']+'"><a class="img" href="//'+mypost[7].category+'/'+mypost[7].path+'" style="background:url(//media.malayatimes.com'+setting['big_thumb']+mypost[7].first_img+') no-repeat">'+mypost[7].title+'</a><div class="header"> <h3 class="entry-title"><a class="title" class="entry-title" href="//'+mypost[7].category+'/'+mypost[7].path+'">'+mypost[7].title+'</a></h3> ')

         res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

         if(mypost[7]['tags'][0]){
              res.write('<a class="tag" href="//hub.malayatimes.com/'+mypost[7]['tags'][0].tag_path+'">'+mypost[7]['tags'][0].tag_title+'</a>')
         }
         res.write('</div></article> </div></div>')

      }

   }

   // res.write('</div><!--/class="content latest"-->')

   res.end();
}

module.exports.serve_api = serve_api;