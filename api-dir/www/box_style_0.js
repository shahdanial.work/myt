var serve_api = function(req, res, data, dbdata){
   var myinput = data,
       mypost = dbdata;

   // console.log(mypost)
   res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});

   // if(typeof data['category'] != 'undifined' && typeof data['heading'] != 'undifined') res.write('<h2><a href="https://'+data['category']+'.malayatimes.com" title="Gosip Artis Malaysia">'+data['heading']+'</a></h2>');

   // res.write('<div class="content latest">')

   //artis big
   if(mypost[0]){

      var masa_utc = new Date(mypost[0]['masa']).toISOString()

      res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+mypost[0]['id']+'"><a class="img" href="//'+mypost[0].category+'/'+mypost[0].path+'" style="background:url(//media.malayatimes.com/photo/'+mypost[0].first_img+') no-repeat"><img alt=" '+mypost[0].title+' " src="//media.malayatimes.com/photo/'+mypost[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" class="entry-title" href="//'+mypost[0].category+'/'+mypost[0].path+'">'+mypost[0].title+'</a></h3> ')

      res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

      if(mypost[0]['tags'][0]){
           res.write('<a class="tag" href="//hub.malayatimes.com/'+mypost[0]['tags'][0].tag_path+'">'+mypost[0]['tags'][0].tag_title+'</a>')
      }
      res.write('</div></article> </div></div>')
   }

   //artis small group
   if(mypost[1]){

      ganda = 0;

      res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
      for (let i = 1; i < mypost.length; i++) {
           ganda +=1;
           if(ganda < 4){
               const post_artis = mypost[i];

               var masa_utc = new Date(post_artis['masa']).toISOString()

               res.write('<article class="hentry content-item" id="article-'+post_artis['id']+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com/picture/'+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

               res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

               if(post_artis['tags'][0]){
                   res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
               }
               res.write('</div></article>')
           }

      }
      res.write('</div></div>')
   }

   if(mypost[4]){

      var masa_utc = new Date(mypost[4]['masa']).toISOString()

      res.write('<div class="content_group_wrapper even big"><div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-'+mypost[4]['id']+'"><a class="img" href="//'+mypost[4].category+'/'+mypost[4].path+'" style="background:url(//media.malayatimes.com/photo/'+mypost[4].first_img+') no-repeat"><img alt=" '+mypost[4].title+' " src="//media.malayatimes.com/photo/'+mypost[4].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+mypost[4].category+'/'+mypost[4].path+'">'+mypost[4].title+'</a></h3> ')

      res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

      if(mypost[4]['tags'][0]){
           res.write('<a class="tag" href="//hub.malayatimes.com/'+mypost[4]['tags'][0].tag_path+'">'+mypost[4]['tags'][0].tag_title+'</a>')
      }
      res.write('</div></article> </div></div>')
   }

   if(mypost[5]){
      ganda = 0;
      res.write('<div class="content_group_wrapper odd small"><div class="content_group_inner_wrapper">')
      for (let i = 5; i < mypost.length; i++) {
           ganda +=1;
           if(ganda < 4){
               const post_artis = mypost[i];

               var masa_utc = new Date(post_artis['masa']).toISOString()

               res.write('<article class="hentry content-item" id="article-'+post_artis['id']+'"><div class="inner-wrapper"><a class="img" href="//'+post_artis.category+'/'+post_artis.path+'"><img alt=" '+post_artis.title+' " src="//media.malayatimes.com/picture/'+post_artis.first_img+'"></a><h3 class="entry-title"><a class="title" href="//'+post_artis.category+'/'+post_artis.path+'">'+post_artis.title+'</a></h3>')

               res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

               if(post_artis['tags'][0]){
                   res.write('<a class="tag" href="//hub.malayatimes.com/'+post_artis['tags'][0].tag_path+'">'+post_artis['tags'][0].tag_title+'</a>')
               }
               res.write('</div></article>')
           }

      }
      res.write('</div></div>')
   }

   // res.write('</div><!--/class="content latest"-->')

   res.end();
}

module.exports.serve_api = serve_api;