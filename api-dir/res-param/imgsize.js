var url = require('url');
const {
   URL
} = require('url')
var http = require('http');
var https = require('https');

var filter_api = function(req, res) {

   if(!res['my_param']) {
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write('{"height":0,"width":0,"type":"no_param_bro"}')
      res.end();
   }

   const myURL = new URL(res['my_param']);

   var sizeOf = require('image-size');

   var imgUrl = res['my_param'];
   var options = url.parse(imgUrl);
   
   if(myURL.protocol == 'http:'){

      http.get(options, function (response, err) {

         if(err){
            res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
            res.write('{"height":0,"width":0,"type":"error"}')
            res.end();
         } else {
            var chunks = [];
            response.on('data', function (chunk) {
              chunks.push(chunk);
            }).on('end', function() {
              var buffer = Buffer.concat(chunks);
              // console.log(JSON.stringify(sizeOf(buffer)));
              res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
              res.write(JSON.stringify(sizeOf(buffer)))
              res.end();
            });
         }

      });

   } else {
      res.writeHead(200, {'Content-Type': 'text/plain\; charset=utf-8'});
      res.write('{"height":0,"width":0,"type":"error"}')
      res.end();
   }


}
module.exports.filter_api = filter_api;