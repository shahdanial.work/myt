const fs = require('fs')
var serve_client = function(req, res, current_url, data, id_tak_mau){

    res.writeHead(200, {'Content-Type': 'text/html\; charset=utf-8'});
    res.write('<!DOCTYPE html>\n')
    res.write('<html class="main-blog">')
    res.write(data['head_open'])
    res.write('<title>Berita Semasa Malaysia &#8212; Malaya Times</title><meta property="og:description" name="twitter:description" content="Berita semasa Malaysia terkini dan terbaru. Blog berita dan info semasa Malaysia yang terbaik terkini dan top di Malaysia." name="description"/><meta rel="canonical" content="//www.malayatimes.com" property="og:url"/>')
    res.write('<meta name="twitter:site" content="@malayatimes"><meta property="og:title" name="twitter:title" content="Malaya Times"><meta property="og:description" name="twitter:description" content="Berita semasa Malaysia terkini dan terbaru. Blog berita dan info semasa Malaysia yang terbaik terkini dan top di Malaysia."><meta property="og:type" content="website"><meta name="twitter:card" content="summary_large_image"><link href="https://media.malayatimes.com/malaya-times.jpg" rel="image_src"/><meta property="og:image" name="twitter:image" content="https://media.malayatimes.com/malaya-times.jpg"><meta property="fb:app_id" content="1829602560385819">')
    // res.write('<style type="text/css">'+data['css']+'</style>')
    res.write('<link rel="stylesheet" type="text/css" href="//media.malayatimes.com/css/mainpage.css"/>')

    res.write(fs.readFileSync('./adsense/homepage-cat-atas', 'utf8'))

    if(data['ads_keyword_gender']){
      res.write('googletag.pubads().setTargeting("gender",['+data['ads_keyword_gender']+']),')
    } else {
      res.write('googletag.pubads().setTargeting("gender",["male","female"]),')
    }
    if(data['ads_keyword_interest']){
      res.write('googletag.pubads().setTargeting("interest",['+data['ads_keyword_interest']+']),')
    }

    res.write(fs.readFileSync('./adsense/homepage-cat-bawah', 'utf8'))

    res.write('</script>')

    res.write(data['head_close'])
    res.write('<body id="main-blog">') // because it's a main website. this is main thing including the header !
    res.write(data['menu_nav'])
    // res.write(data['header'])
    res.write('<div class="hfeed section" id="blog-content"><div class="widget Blog" data-version="1" id="Blog1">')
    //////////// DYNAMIC START /////////////

    var article_id = 0;

        res.write('<section class="wrapper rows">')//authority_1
            /* SECTION POPULAR */
            //res.write(data['popular_box'])

            /* POPULAR START */
            if(data['popular_feed']){
                var popular = data['popular_feed'];
                res.write('<div class="popular-articles column_1"> <h3>Popular</h3> ')
                var j = 0
                 for (let i = 0; i < popular.length; i++) {
                     var artikel = popular[i];
                     j++;
                     res.write('<article class="hentry popular-article" id="article-'+j+'"><div class="inner-wrapper"><a class="img" href="//'+artikel.domain+'/'+artikel.path+'" style="background: url(//media.malayatimes.com/foto/'+artikel.first_img+') no-repeat;"><img alt="'+artikel.title+' " src="//media.malayatimes.com/gambar/'+artikel.first_img+'"></a><div class="caption"><h2><a class="title" href="//'+artikel.domain+'/'+artikel.path+'">'+artikel.title+'</a></h2>')
                     res.write('<a class="tag" href="//'+artikel.domain+'">'+artikel.category+'</a>')
                     res.write('</div></div></article> ')
                 }
                 if(popular.length > 1){
                    res.write(' <button class="slide_left">Back</button> <button class="slide_right">Next</button> ')
                    res.write('<div class="slidecount">')
                    for (let i = 0; i < popular.length; i++) {
                        if(i == 0){
                         res.write('<span id="count'+i+'" class="active"></span>')
                        } else {
                         res.write('<span id="count'+i+'"></span>')
                        }
                    }
                    res.write('</div>')
                 }
                res.write('</div>')//end popular box
            } else {
                res.write(data['popular_box'])
            }
            /* POPULAR END */


            /* SECTION LATEST */
            var latest_post = data['latest_post'];

            if(latest_post[0]){
               // console.log('\n\n##########\n\n', latest_post[0] ,'\n\n##########\n\n')
                var masa_utc = new Date(latest_post[0]['masa']).toISOString()

                res.write('<div class="recent-articles column_2 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-11"><a class="img" href="//'+latest_post[0].category+'/'+latest_post[0].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[0].first_img+') no-repeat"><img alt=" '+latest_post[0].title+' " src="//media.malayatimes.com/photo/'+latest_post[0].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[0].category+'/'+latest_post[0].path+'">'+latest_post[0].title+'</a></h3>')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[0].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[0].tags[0].tag_path+'">'+latest_post[0].tags[0].tag_title+'</a>')
                }

                res.write('</div></article> </div></div>')
            }

            /////////////////// START IKLAN //////////////////
            res.write('<aside class="iklan column_3 odd big"> <div class="content_group_inner_wrapper"><div class="content-item"><div class="premium" id="gpt-tgh"><script>googletag.cmd.push(function() { googletag.display("gpt-tgh"); googletag.pubads().refresh([gptAdSlots[1]]); });</script></div></div></div></aside>')
            // res.write('<aside class="iklan column_3 odd big"> <div class="content_group_inner_wrapper"><div class="content-item"><div class="premium" ><a href="https://www.facebook.com/DedsaanBeachRun/"><img src="https://3.bp.blogspot.com/-uygr1240LyY/W_rSsaRt97I/AAAAAAAADAs/w5huM68VnpUMu_JtKp2a65zCdsPffJQsgCLcBGAs/s1600/iklan-tansri.jpg" /></a></div></div></div></aside>')
            ///////////////////  END IKLAN  //////////////////

            /* /SECTION LATEST */

            res.write('</section><!-- end class="wrapper rows"-->') // dalamnya popular dan recent atas

            /* SECTION LATEST BAWAH */
            if(latest_post[1]){

               var masa_utc = new Date(latest_post[1]['masa']).toISOString()

                res.write('<section class="wrapper rows">')//authority_2

                res.write('<div class="recent-articles column_4 even big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-12"><a class="img" href="//'+latest_post[1].category+'/'+latest_post[1].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[1].first_img+') no-repeat"><img alt=" '+latest_post[1].title+' " src="//media.malayatimes.com/photo/'+latest_post[1].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[1].category+'/'+latest_post[1].path+'">'+latest_post[1].title+'</a></h3>')

                res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                if(latest_post[1].tags[0]){
                    res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[1].tags[0].tag_path+'">'+latest_post[1].tags[0].tag_title+'</a>')
                }
                res.write('</div></article> </div></div>')

                /*2 to 7*/
                if(latest_post[2]){
                res.write('<div class="recent-articles column_5 odd">')


                    var even_odd = 'odd';
                    var column = 5;
                    var ganda = 0;
                    var article_id = 12;
                    for (let i = 2; i < 8; i++) {
                        const post_latest = latest_post[i];
                        article_id ++;
                        ganda ++;

                        ///////////////////////

                           var masa_utc = new Date(latest_post[i]['masa']).toISOString()

                            res.write('<article class="hentry recent-article" id="article-'+article_id+'"> <a class="img" href="//'+post_latest.category+'/'+post_latest.path+'"> <img alt="'+post_latest.title+'" src="//media.malayatimes.com/picture/'+post_latest.first_img+'"> </a> <h2 class="entry-title"><a class="title" href="//'+post_latest.category+'/'+post_latest.path+'">'+post_latest.title+'</a></h2>')

                            res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                            if(post_latest.tags[0]){
                                res.write(' <a class="tag" href="//hub.malayatimes.com/'+post_latest.tags[0].tag_path+'">'+post_latest.tags[0].tag_title+'</a>')
                            }

                            res.write(' </article>')
                        ///////////////////////

                        if(ganda == 3 && i != 7){
                            column += 1;
                            ganda = 0;
                            if(even_odd == 'even'){
                                even_odd = 'odd';
                            } else {
                                even_odd = 'even';
                            }
                            res.write('</div><div class="recent-articles column_'+column+' '+even_odd+'">')
                        }

                    }

                res.write('</div><!-- end class="recent-articles column_5 odd"-->')
                }


                // post 8
                if(latest_post[8]){

                   var masa_utc = new Date(latest_post[8]['masa']).toISOString()

                    res.write('<div class="recent-articles column_7 odd big"> <div class="content_group_inner_wrapper"> <article class="hentry content-item" id="article-19"><a class="img" href="//'+latest_post[8].category+'/'+latest_post[8].path+'" style="background:url(//media.malayatimes.com/photo/'+latest_post[8].first_img+') no-repeat"><img alt=" '+latest_post[8].title+' " src="//media.malayatimes.com/photo/'+latest_post[8].first_img+'"></a><div class="header"> <h3 class="entry-title"><a class="title" href="//'+latest_post[8].category+'/'+latest_post[8].path+'">'+latest_post[8].title+'</a></h3>')

                    res.write('<time datetime="' + masa_utc + '"><abbr class="published updated" title="' + masa_utc + '"></abbr></time>')

                    if(latest_post[8].tags[0]){
                        res.write(' <a class="tag" href="//hub.malayatimes.com/'+latest_post[8].tags[0].tag_path+'">'+latest_post[8].tags[0].tag_title+'</a>')
                    }
                    res.write('</div></article> </div></div>')
                }


                res.write('</section><!-- end class="wrapper rows"-->')
            }
            /* /SECTION LATEST BAWAH */


       ////////////////// DYNAMIC BY CATEGORY DELETED //////////////////////
       res.write('<div class="section_wrapper politik scroll_xhr_content"  data-loaded="false" data-category="politik" data-heading="Politik Malaysia"></div>')
       res.write('<div class="section_wrapper artis scroll_xhr_content"  data-loaded="false" data-category="artis" data-heading="Gosip artis"></div>')
       res.write('<div class="section_wrapper berita scroll_xhr_content"  data-loaded="false" data-category="berita" data-heading="Berita terkini"></div>')
       res.write('<div class="section_wrapper sukan scroll_xhr_content"  data-loaded="false" data-category="sukan" data-heading="Berita sukan"></div>')
       res.write('<div class="section_wrapper viral scroll_xhr_content"  data-loaded="false" data-category="viral" data-heading="Viral Malaysia"></div>')
       res.write('<div class="section_wrapper kesihatan scroll_xhr_content"  data-loaded="false" data-category="kesihatan" data-heading="Info Kesihatan"></div>')

       res.write('<aside id="loading-spiner"><div class="center"><span class="loader"><span class="pusing"></span></span></div></aside>')

    //////////// DYNAMIC END ///////////////
    res.write('</div></div><!--class="widget Blog" data-version="1" id="Blog1"-->')
    res.write(`<footer id="blog-footer"> <p class="home-page"><a href=" //www.malayatimes.com/">Berita Semasa Malaysia</a></p><aside class="not-important"> <div id="login-and-search" class="search_and_social"> <div class="section_wrapper"> <h5 class="heading"><a href="//www.malayatimes.com" title="Berita Semasa Malaysia"><img alt="Berita Semasa Malaysia" src="//media.malayatimes.com/assets/image/malayatimes.png"></a></h5> <div class="search_box"> <div class="inline_block"> <form action="//hub.malayatimes.com/cari"><input id="search_box" class="es_input" name="keyword" required> <button type="submit" id="search-button" class="cari"></button></form></div></div><div id="search-result"> <div id="stanby" style="dislay:none;"> <span class="loader"> <span class="pusing"></span> </span> </div><span id="close-search-result" onclick="clear_search()">clear</span> </div><div class="or"></div><span class="text_like">like &amp; follow us</span> <div id="fans"></div><button id="close-login-search" class="close_section">×</button> </div></div><input type="hidden" id="posts-id" value="`+id_tak_mau+`"><script type="text/javascript" src="//media.malayatimes.com/js/main/home.js"></script> </aside> </footer>`)
    res.write('</body></html>')
    res.end();

} // end create server


module.exports.serve_client = serve_client;
