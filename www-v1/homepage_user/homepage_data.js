const db = require('../../db');
const helper = require('../../helper');
const next = require('./by_category');
/*
SET UP DATA THAT USING MySQL query.. if you have another data from db nned to use. add more STEP
*/



var homepage_data = function(req, res, current_url, data){
    /* STEP 1 */
    var latest_post = function(err1, rows1) { //what ever category from post_tbl..
        if(err1){
            console.log('error latest_post sql', err1);
            throw err1;
        } else {
           console.log(rows1);
            data['latest_post'] = rows1;
            for (let i = 0; i < data['latest_post'].length; i++) {
                if(data['latest_post'][i]['tag_path'] && data['latest_post'][i]['tag_title']){
                    data['latest_post'][i]['tags'] = helper.build_tag(data['latest_post'][i]['tag_path'], data['latest_post'][i]['tag_title']);
                } else {
                    data['latest_post'][i]['tags'] = new Array();
                }
            }
            var arr_id_latest_post = new Array()
            for (let i = 0; i < data['latest_post'].length; i++) {
                if(data['latest_post'][i].id){
                    arr_id_latest_post.push(data['latest_post'][i].id)
                }
            }
            if(arr_id_latest_post){
                var id_tak_mau = '0';//var id_tak_mau = arr_id_latest_post.join(',')
            } else {
                var id_tak_mau = '0';
            }

            next.by_category(req,res,current_url,data, id_tak_mau);
            // console.log(data)

        }
    }

    var query = "SELECT post_tbl.*, category_manager.domain AS domain, users.namapena, Group_concat(tag_tbl.tag_path) AS tag_path, Group_concat(tag_tbl.title) AS tag_title FROM post_tbl LEFT JOIN category_manager ON category_manager.from_tbl = ? AND category_manager.from_id = post_tbl.id LEFT JOIN users ON users.id_user = post_tbl.author_id LEFT JOIN tag_manager ON tag_manager.from_tbl = ? AND tag_manager.from_id = post_tbl.id LEFT JOIN tag_tbl ON tag_tbl.tag_path = tag_manager.tag_path AND tag_tbl.status = 'publish' WHERE post_tbl.status = ? AND users.namapena = ? GROUP BY post_tbl.id ORDER BY post_tbl.id DESC LIMIT 0, 16"


    var string = [
        'post_tbl',
        'post_tbl',
        'publish',
        req['url'].replace('/','')
    ];

    db.statement(query, string, latest_post);




} // end create server


module.exports.homepage_data = homepage_data;
