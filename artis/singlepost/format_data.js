var send = require('./serve_client')
const jsdom = require("jsdom")
const {
   JSDOM
} = jsdom;

var format_data = function(req, res, current_url, data) {

   if (data) {

      data.showAds = new Array();
      data.showAds.adsenseHeader = false;
      data.showAds.adsensePostAtas = false;
      data.showAds.adsensePostBawah = false;
      data.showAds.adsenseOverlay = false;

      if (data['post']) {

         var post = data['post'];

         var ads_post_atas = '<aside class="premium"><div id="gpt-tgh"><script>\ngoogletag.cmd.push(function() { googletag.display("gpt-tgh"); })</script></div></aside>';

         var ads_post_bawah = '<aside class="premium"><div id="gpt-tgh-1"><script>\ngoogletag.cmd.push(function() { googletag.display("gpt-tgh-1"); })</script></div></aside>';

         var dom = new JSDOM('<div id="jjjjjjj">' + post['content'] + '</div>')
         var all = dom.window.document.querySelectorAll('#jjjjjjj > *')

         var content_html = '';

         if (all) {
            if(all.length > 8){
               /* perenggan 9 or keatas (2 iklan tgh) - disable header and overlay. */

               data.showAds.adsensePostAtas = true;
               data.showAds.adsensePostAtas = true;

               var i_bawah = ( parseFloat(all.length) - ( parseFloat(all.length) / parseFloat(3) ) ).toFixed(0)
               var i_atas = (parseFloat(i_bawah) / parseFloat(2)).toFixed(0)

               console.log('iiiiiii', i_bawah, i_atas)

               for (var i = 0; i < all.length; i++) {

                  if (all[i]) {

                     if (typeof dom.window.addEventListener === 'function'){
                            (function (index) {
                               if(index.getElementsByClassName('video').length > 0){
                                  index.className = 'paragraph video_wrapper';
                               }
                               if (i == i_atas) {
                                  content_html += ads_post_atas + all[i].outerHTML;
                               } else if(i == i_bawah){
                                  content_html += ads_post_bawah + all[i].outerHTML;
                               } else {
                                  content_html += all[i].outerHTML;
                               }

                            })(all[i]);
                     }

                  }

               }

            } else if(all.length > 1) {
               /* peranggan 2 or  keatas (1 iklan tgh, 1 iklan header) - disable overlay */

               data.showAds.adseseHeader = "<aside class=\"ikln_header\"><div id='bwh-header'><script>\ngoogletag.cmd.push(function() { googletag.display('bwh-header'); })</script></div></aside>";

               data.showAds.adsensePostAtas = true;

               var i_middle = parseInt(all.length / 2)
               i_middle = parseFloat(i_middle) - 1;//sbb loop nnt start dari 0

               for (var i = 0; i < all.length; i++) {

                  if (all[i]) {

                     if (typeof dom.window.addEventListener === 'function'){
                            (function (index) {
                               if(index.getElementsByClassName('video').length > 0){
                                  index.className = 'paragraph video_wrapper';
                               }
                               if (i == i_middle) {
                                  content_html += all[i].outerHTML + ads_post_atas;
                               } else {
                                  content_html += all[i].outerHTML;
                               }

                            })(all[i]);
                     }

                  }

               }

            } else {
               /* perenggan 1 shj? video? inforaphic? (1 iklan header, 1 iklan overlay) - disable tgh. */

               data.showAds.adseseHeader = "<aside class=\"ikln_header\"><div id='bwh-header'><script>\ngoogletag.cmd.push(function() { googletag.display('bwh-header'); }); if(window.googletag && googletag.pubadsReady) { console.log(googletag.pubads()) }</script></div></aside>";

            }

         }

         /* OVERIDE post['content'] */
         post['content'] = content_html;
         send.server_single_post(req,res,current_url, data);

      }
   }


} // end module..

module.exports.format_data = format_data;


// var iklan = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\n<ins class="adsbygoogle"\nstyle="display:block"\ndata-ad-client="ca-pub-2103503985775870"\ndata-ad-slot="1378446614"\ndata-ad-format="link"></ins><script>\n(adsbygoogle = window.adsbygoogle || []).push({});\n</script>';
//
// /* Google Adsense Table */
// var tbl_ad = '<aside class="google_adsense"><table class="links_ad aside"><tbody><tr><td class="first_box">'
// tbl_ad += iklan;
// tbl_ad += '</td><td>';
// tbl_ad += iklan;
// tbl_ad += '</td></tr></tbody></table></aside>';
